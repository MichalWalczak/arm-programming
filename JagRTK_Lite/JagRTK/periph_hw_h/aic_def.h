/********************************************************************
  File: aic_def.h   (notacja JagNOT)
  Desc: aic

				Przedstawiony kod opisany jest w rozdziale 9.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*********************************************************************/
#ifndef _AIC_DEF_H_INCLUDED_
#define _AIC_DEF_H_INCLUDED_
#include "../periph_hw_h/reg_typedef.h"

//----------------------------------------------------------------------------------
typedef struct S_AIC {
	REG	 SOURCE_MODE_R[32]; 		// R/W,	0x0,     +0x0,	SMR
	REG	 SOURCE_VECTOR_R[32]; 	// R/W,	0x0,  	+0x80,	SVR
	REG	 IRQ_VECTOR_R; 					// RO,	0x0,  	+0x100,	IVR		
	REG	 FIQ_VECTOR_R; 					// RO,	0x0,  	+0x104,	FVR	
	REG	 INT_STATUS_NR_R; 			// RO,	0x0,  	+0x108, ISR	
	REG	 INT_PENDING_R; 				// RO,	0x0*,  	+0x10C, IPR
	REG	 INT_MASK_R; 						// RO,	0x0,  	+0x110, IMR	
	REG	 CORE_INT_STATUS_R; 		// RO,	0x0,  	+0x114, CISR 
	REG	 Reserved0[2]; 					// 
	REG	 INT_ENABLE_R; 					// WO, 		-, 		+0x120, IECR
	REG	 INT_DISABLE_R; 				// WO, 		-, 		+0x124, IDCR 
	REG	 INT_CLEAR_CMD_R; 			// WO, 		-, 		+0x128, ICCR 
	REG	 INT_SET_CMD_R; 				// WO, 		-, 		+0x12C, ISCR 
	REG	 EOI_R; 								// WO, 		-, 		+0x130, EOICR 
	REG	 SPURIOUS_VECTOR_R; 		// R/W,	0x0,  	+0x134, SPU 
	REG	 DEBUG_CONTROL_R; 			// R/W,	0x0,  	+0x138, DCR 
	REG	 Reserved1[1]; 					// 
	REG	 FAST_ENABLE_R; 				// WO, 		-, 		+0x140, FFER 
	REG	 FAST_DISABLE_R; 				// WO, 		-,		+0x144, FFDR 
	REG	 FAST_STATUS_MASK_R; 		// RO,	0x0,  	+0x148, FFSR 
} S_AIC, *PS_AIC;

#define 	AIC   ((PS_AIC) 0xFFFFF000) 		

#define SYS_PERIPH_ID   		1 
#define SYS_PERIPH_ID_MASK  (0x1 << SYS_PERIPH_ID)


// -------- SOURCE_MODE_R -----------------
#define AIC_PRIORITY       ((unsigned int) 0x7 <<  0) 
#define 	AIC_PRIORITY_LOWEST         ((unsigned int) 0x0) 
#define 	AIC_PRIORITY_0							((unsigned int) 0x0)
#define 	AIC_PRIORITY_1							((unsigned int) 0x1)
#define 	AIC_PRIORITY_2							((unsigned int) 0x2)
#define 	AIC_PRIORITY_3							((unsigned int) 0x3)
#define 	AIC_PRIORITY_4							((unsigned int) 0x4)
#define 	AIC_PRIORITY_5							((unsigned int) 0x5)
#define 	AIC_PRIORITY_6							((unsigned int) 0x6)
#define 	AIC_PRIORITY_7							((unsigned int) 0x7)
#define 	AIC_PRIORITY_HIGHEST        ((unsigned int) 0x7) 

#define AIC_SRC_TYPE     ((unsigned int) 0x3 <<  5) 
#define 	AIC_LEVEL_TRIGGERED   		 ((unsigned int) 0x0 <<  5) 
#define 	AIC_EDGE_TRIGGERED    		 ((unsigned int) 0x1 <<  5) 
#define 	AIC_EXTERNAL_LOW_LEVEL_TRIGGERED   	 ((unsigned int) 0x0 <<  5) 
#define 	AIC_EXTERNAL_NEGATIVE_EDGE_TRIGGERED ((unsigned int) 0x1 <<  5) 
#define 	AIC_EXTERNAL_HIGH_LEVEL_TRIGGERED    ((unsigned int) 0x2 <<  5) 
#define 	AIC_EXTERNAL_POSITIVE_EDGE_TRIGGERED ((unsigned int) 0x3 <<  5) 

// -------- CORE_INT_STATUS_R  -------- 
#define AIC_NFIQ        ((unsigned int) 0x1 <<  0) 
#define AIC_NIRQ        ((unsigned int) 0x1 <<  1) 
// -------- DEBUG_CONTROL_R -------- 
#define AIC_DCR_PROT    ((unsigned int) 0x1 <<  0) 
#define AIC_DCR_GMSK    ((unsigned int) 0x1 <<  1) 



#endif // of _AIC_DEF_H_INCLUDED_
