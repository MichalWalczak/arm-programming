/*****************************************************************************
  File: pwm_tmr.c
  Desc: Sterownik (driver) timera PWM 
        Timer PWM jest traktowany jak timer ogolnego przeznaczenia.

				Przedstawiony kod opisany jest w rozdziale 14.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
				0.1G - GNU / 2007.08.31
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*******************************************************************************/
#include "proj_config.h"
#include "../periph_hw_h/pmc.h"
#include "../periph_hw_h/pio.h"
#include "../periph_hw_h/board_pins.h"
#include "../periph_hw_h/aic.h"
#include "../periph_hw_h/led_sw.h"
#include "../periph_hw_h/delay.h"
#include "../periph_hw_h/dbgu_debug.h"
#include "../periph_h/pwm_tmr.h"

#define LEDWIZ	0	// 1-wizualizacja modulu, 0-wylaczona
//-------------------------------------------------------------------------------


int PWMTMR_cnt;
static int licznik;

//-------------------------------------------------------------------------------
void TMRPWM_irq_handler( void ) DEFINE_AS_IRQ_HANDLER
{  
 int req;
	req = PWM->INT_STATUS_R;		

	if( req & PWM_CHAN_ID3 ){  	
		PWMTMR_cnt++;							
		licznik++;
		// ... 											// kod merytoryczny stosownie do potrzeb aplikacji

		if(licznik >= 10 ){ 		// kod testowy, co dziesiec okresow
			licznik=0;	LEDWIZ_ToggleOne( PIO_PA1, LEDWIZ );	// wizualizacja
		}
	}
 	AIC_EOI;
}
//-----------------------------------------------------------------
void TMRPWM_Open( int channel, int time_ms )
{ 
	PMC_OpenPeriphClock( PWM_PERIPH_ID_MASK );
	PWM->INT_DISABLE_R = 0x01 << channel;
	PWM->DISABLE_R = 0x1 << channel;				
	PWM->CHANNEL[channel].MODE_R =  PWM_CHAN_PRESC_MCK_1024
																| PWM_POLARITY_LOW
																| PWM_LEFT_ALINGMENT
																| PWM_UPDATE_PERIOD;
	PWM->CHANNEL[channel].PERIOD_R = TMRPWM_CalcPeriodR_PrescMCK1024( time_ms );
	PWM->CHANNEL[channel].DUTY_R = 1;  			
	
	AIC_Configure_IRQ( PWM_PERIPH_ID, (AIC_LEVEL_TRIGGERED | AIC_PRIORITY_5), 
										 TMRPWM_irq_handler ); 	
	PWM->INT_ENABLE_R = (0x01 << channel);
	PWM->ENABLE_R = (0x1 << channel);				
	DBGU_Trace("Starting PWMTMR channel=%i.\n\r", channel );
}
//-----------------------------------------------------------------
int TMRPWM_CalcPeriodR_PrescMCK1024( int time_ms )
{ 
 int period;
	period = (((fMCK_Hz*10)/1024)*time_ms)/1000;
	if( (period %10) >=5 ) period = (period/10)+1; 
	else 									 period = (period/10); 
	return(period);
}

