#ifndef LCD_H
#define LCD_H

#include "library\AT91SAM7S64.h"
#include "library\lib_AT91SAM7S64.h"
#include "delay.h"
#include <stdio.h>


/*************************************************************************
				DEFINE PINS
**************************************************************************/

#define		LCD_E_PIN			AT91C_PIO_PA0	
#define		LCD_RS_PIN			AT91C_PIO_PA1
#define		LCD_DATA_D0_PIN		AT91C_PIO_PA2
#define		LCD_DATA_D1_PIN		AT91C_PIO_PA3
#define		LCD_DATA_D2_PIN		AT91C_PIO_PA4
#define		LCD_DATA_D3_PIN		AT91C_PIO_PA5
#define		LCD_CMD				0
#define		LCD_DATA			1

/*************************************************************************
*************************************************************************
				FUNCTIONS
*************************************************************************
**************************************************************************/
void LCD_Open();
void LCD_SendNible(unsigned int data, unsigned int cmd, char Low_High);
void LCD_WriteCmd(unsigned int cmd);
void LCD_WriteByte(unsigned int data);
void LCD_ClrScr();
void LCD_Home();
void LCD_WriteString(char * s);
void LCD_GotoRC(unsigned char row, unsigned char col );

//funkcje zapisuj� liczby tak by przecinek by� w miejscu

void LCD_WriteInt(int value, int lm, int null_zero);
// value - liczba, lm - liczba p�l przeznaczonych na liczbe, null_zero - je�li '0' to z przodu wpisywane 
// s� zera, je�li inna cyfra to wpisywane s� miejsca puste

void LCD_WriteFloat(float l,int lm_i,int lm_f);
// podobnie, z tym �e lm_i to liczba p�l przeznaczonych przed przecinkiem, a lm_f za przecinkiem

//************************************************************************
//************************************************************************
//************************************************************************




/*************************************************************************
*************************************************************************
				DEFINITIONS
*************************************************************************
**************************************************************************/

void LCD_SendNible(unsigned int data, unsigned int cmd, char Low_High)
{
	AT91F_PIO_ClearOutput(AT91C_BASE_PIOA, LCD_E_PIN);
	
	if (cmd == LCD_CMD)	AT91F_PIO_ClearOutput(AT91C_BASE_PIOA, LCD_RS_PIN);
	else					AT91F_PIO_SetOutput(AT91C_BASE_PIOA, LCD_RS_PIN);
	
	if(Low_High == 'H')	data = data >> 4;
	
	AT91F_PIO_ClearOutput(AT91C_BASE_PIOA, LCD_DATA_D0_PIN | LCD_DATA_D1_PIN |
											LCD_DATA_D2_PIN | LCD_DATA_D3_PIN );

	if(data & 0x01)		AT91F_PIO_SetOutput(AT91C_BASE_PIOA, LCD_DATA_D0_PIN);
	if(data & 0x02)		AT91F_PIO_SetOutput(AT91C_BASE_PIOA, LCD_DATA_D1_PIN);
	if(data & 0x04)		AT91F_PIO_SetOutput(AT91C_BASE_PIOA, LCD_DATA_D2_PIN);
	if(data & 0x08)		AT91F_PIO_SetOutput(AT91C_BASE_PIOA, LCD_DATA_D3_PIN);
	
	AT91F_PIO_SetOutput(AT91C_BASE_PIOA, LCD_E_PIN);
	delay_us(1);
	AT91F_PIO_ClearOutput(AT91C_BASE_PIOA, LCD_E_PIN);

}
//----------------------------------------------------------------------------------

void LCD_WriteCmd(unsigned int cmd)
{
	LCD_SendNible(cmd, LCD_CMD, 'H');
	LCD_SendNible(cmd, LCD_CMD, 'L');
	delay_us(40);
}
//----------------------------------------------------------------------------------

void LCD_WriteByte(unsigned int data)
{
	LCD_SendNible(data, LCD_DATA, 'H');
	LCD_SendNible(data, LCD_DATA, 'L');
	delay_us(40);
}

//**********************************************************************************

#define 		LCD_KONFIG_PARAM_R		0X20
#define			LCD_MATRYCA_10X5		(0X1 << 2)
#define			LCD_MATRYCA_5X7			(0X0 << 2)
#define			LCD_2LINIE				(0X1 << 3)
#define			LCD_1LINIA				(0X0 << 3)
#define			LCD_8BIT				(0X1 << 4)
#define			LCD_4BIT				(0X0 << 4)

#define			LCD_KONFIG_RUCHKIER_R	0X01
#define			LCD_RUCH_DO_PRAWEJ		(0X1 << 2)
#define			LCD_RUCH_DO_LEWEJ		(0X0 << 2)
#define			LCD_RUCH_OBRAZU			(0X1 << 3)
#define			LCD_RUCH_KURSORA		(0X0 << 3)

#define			LCD_KONFIG_KURSOR_R		0X8
#define			LCD_KURSOR_BLOKOWY		(0X1 << 0)
#define			LCD_KURSOR_PODKRESLENIE	(0X0 << 0)
#define			LCD_KURSOR_OFF			(0X1 << 1)
#define			LCD_KURSOR_ON			(0X0 << 1)
#define			LCD_DISP_ON				(0X1 << 2)
#define			LCD_DISP_OFF			(0X0 << 2)

#define			LCD_KONFIG_INKR_WST_R	0X4
#define			LCD_WSTAWIANIE			(0X1 << 0)
#define			LCD_NADPISYWANIE		(0X0 << 0)
#define			LCD_INKREMENTACJA		(0X1 << 1)
#define			LCD_DEKREMENTACJA		(0X0 << 1)

#define			LCD_CLRSCR_R			0X01 
#define			LCD_HOME_R				0X02
#define			LCD_SET_DATA_PTR_R		0X80


void LCD_Open()
{
	AT91F_PIO_CfgOutput(AT91C_BASE_PIOA, (LCD_E_PIN | LCD_RS_PIN | LCD_DATA_D0_PIN | LCD_DATA_D1_PIN
						| LCD_DATA_D2_PIN | LCD_DATA_D3_PIN));
						
	delay_ms(16);
	
	LCD_SendNible((LCD_KONFIG_PARAM_R | LCD_8BIT), LCD_CMD, 'H');
	delay_ms(4);
	LCD_SendNible((LCD_KONFIG_PARAM_R | LCD_8BIT), LCD_CMD, 'H');
	delay_us(120);
	LCD_SendNible((LCD_KONFIG_PARAM_R | LCD_8BIT), LCD_CMD, 'H');
	delay_us(120);
	
	//inicjalizacja trybu 4 bit
	
	LCD_SendNible((LCD_KONFIG_PARAM_R | LCD_4BIT), LCD_CMD, 'H');
	delay_ms(3);
	
	//konfiguracja
	
	LCD_WriteCmd((LCD_KONFIG_PARAM_R | LCD_4BIT |LCD_2LINIE | LCD_MATRYCA_5X7));
	LCD_WriteCmd((LCD_KONFIG_KURSOR_R | LCD_DISP_ON |LCD_KURSOR_ON | LCD_KURSOR_BLOKOWY));
	LCD_WriteCmd((LCD_KONFIG_INKR_WST_R | LCD_INKREMENTACJA | LCD_NADPISYWANIE ));
	LCD_WriteCmd((LCD_KONFIG_RUCHKIER_R | LCD_RUCH_KURSORA));
	delay_ms(2);
	LCD_ClrScr();
	LCD_Home();
	
}

//---------------------------------------------------------------------------------------

void LCD_ClrScr()
{
	LCD_WriteCmd(LCD_CLRSCR_R);
	delay_ms(2);	
}

//--------------------------------------------------------------------------------------

void LCD_Home()
{
	LCD_WriteCmd(LCD_HOME_R);
	delay_ms(2);
}

//--------------------------------------------------------------------------------------

void LCD_WriteString(char * s)
{
	while(*s)
		LCD_WriteByte(*s++);
}

//--------------------------------------------------------------------------------------


/*void LCD_WriteFloat(float l,int lm_i,int lm_f)
{

		int dzielnik = 1;	
	int i = (int) l;
	int f; 
	int lm_f1 = lm_f;
			
	while(lm_f1--)	
		dzielnik *= 10;		

	f = (int)((l - (float)i ) * dzielnik + 0.5 );

	if (f < 0)
		f = (int)-f + 1.5;
	
	LCD_WriteInt(i,lm_i, 1) ;	
	LCD_WriteByte('.');		
	LCD_WriteInt((int)f,lm_f, 0) ;	
}*/
//--------------------------------------------------------------------------------------
void LCD_WriteInt(int value, int lm, int null_zero)
{
	int dzielnik = 1;
	int i = 0;
	int lm1 = lm;
	char buf[10];
	int flaga = 0;
	
	while((lm1--)-1)
	{
		dzielnik = dzielnik * 10;
	}
	
	if( value < 0)
	{
		
		flaga = 1;
		value = -value;
	}

	while(dzielnik >=1)
	{
		if((int) value/dzielnik == 0)
		{	
				if(null_zero == 0)
					buf[i++] = '0';
				else
					buf[i++] = ' ';
		}
		else
		{
				buf[i++] = (char)((value/dzielnik) + 48);

				if (flaga == 1)
				{
					buf[i-2] = '-';
					flaga = 0;
				}
				null_zero = 0;
		}
		
		value = value % dzielnik;
		dzielnik = dzielnik/10;
	}	
	
	if(buf[i-1] == ' ')
		buf[i-1] = '0';

	buf[i] = 0;

	LCD_WriteString(buf);
}

//--------------------------------------------------------------------------------------
void LCD_GotoRC(unsigned char row, unsigned char col )
{
	unsigned char adr=0;
	switch(row)
	{
		case 0: adr = (LCD_SET_DATA_PTR_R + 0) + col;			break;
		case 1: adr = (LCD_SET_DATA_PTR_R + 0x40) + col;		break;
	}
	
	LCD_WriteCmd(adr);
	delay_us(40);
}

#endif //LCD_H
