//*----------------------------------------------------------------------------
//*         ATMEL Microcontroller Software Support  -  ROUSSET  -
//*----------------------------------------------------------------------------
//* The software is delivered "AS IS" without warranty or condition of any
//* kind, either express, implied or statutory. This includes without
//* limitation any warranty or condition with respect to merchantability or
//* fitness for any particular purpose, or against the infringements of
//* intellectual property rights of others.
//*----------------------------------------------------------------------------
//* File Name           : main.c
//* Object              : main application written in C
//* Creation            : JPP   16/Jun/2004
//* modified by Michał Walczak
//*----------------------------------------------------------------------------


#include "proj_h.h"
#include "proj_config.h"


unsigned volatile int LED0_0, interupt_conter=0;

void main_init(){ //peripheral devices itialization
	PMC_OpenPeriphClock(PIOA_PERIPH_ID_MASK);
	LED_Open(PIO_PA0 | PIO_PA1 | PIO_PA2 );
	SW_Open(PIO_PA15 | PIO_PA16 | PIO_PA17 | PIO_PA18);
	LCD_Open();
}

void TimerCounterIrq(void) DEFINE_AS_IRQ_HANDLER // Interrupt from TC0
{
	int status_r = TC0->INT_STATUS_R;
	LED_ToggleOne(LED0_0);
	AIC_EOI;
}
void PIO_PA15Irq(void) DEFINE_AS_IRQ_HANDLER // Interrupt from SW1
{
	int dummy = PIO->INT_STATUS_R;
	LED_Clear(LED0 | LED1 | LED2| LED3);// claear all leds
	interupt_conter++;
	interupt_conter=interupt_conter%4 ; // max value
	LED0_0 = (unsigned int)1<<(interupt_conter) ;//change led number for TC0 irq

	AIC_EOI;
}
void pmcTimerInit(){ // Timer Initialization
	int mode, action;

	PMC_OpenPeriphClock( TC0_PERIPH_ID_MASK );
	TC0->CONTROL_R = TC_CLK_DISABLE;
	TC0->INT_DISABLE_R = 0xFFFFFFFF;
	TC0->INT_ENABLE_R = TC_COMPARE_RC | TC_COMPARE_RB;

		 mode =
					( TC_MCK1024
					| TC_CLOCK_NOT_INVERTED
					| TC_CLOCK_GATED_BY_NONE

					| TC_NOT_STOP_ON_RC_COMPARE
					| TC_NOT_DISABLE_COUNTER_CLOCK_ON_RC_COMPARE

					| TC_EXTERNAL_EVT_EDGE_NONE
					| TC_EXTERNAL_EVT_SEL_XC0
					| TC_EXTERNAL_EVT_TRIGGER_NOT_ENABLE_RESET_PV

					| TC_MODE_UP_WITHOUT_AUTO_RESET_ON_RC_COMPARE
					| TC_MODE_WAVE_PWM
					);
		 action =
					( TC_ON_RA_COMPARE_TIOA_NONE
					| TC_ON_RC_COMPARE_TIOA_NONE
					| TC_ON_EXTERNAL_EVENT_TIOA_NONE
					| TC_ON_SOFT_TRIGGER_TIOA_NONE

					| TC_ON_RB_COMPARE_TIOB_NONE
					| TC_ON_RC_COMPARE_TIOB_NONE
					| TC_ON_EXTERNAL_EVENT_TIOB_NONE
					| TC_ON_SOFT_TRIGGER_TIOB_NONE
					);
		TC0->MODE_R = mode | action;
		TC0->RB = 46800;//(fMCK_Hz / 1024);

		TC0->CONTROL_R = TC_CLK_ENABLE | TC_SOFTWARE_TRIGGER;

	//-----------------------------------------------------------------------
		//Enable TC0 interrupt
		AIC_Configure_IRQ(	TC0_PERIPH_ID,
							(AIC_EXTERNAL_POSITIVE_EDGE_TRIGGERED | AIC_PRIORITY_2),
							TimerCounterIrq );
}

int main(void)
{//* Begin
	int dummy=0;
	char str[10];
	main_init();
	LED0_0 = (unsigned int)1<<0; //set to first diode
	LED_Clear(LED0 | LED1 | LED2| LED3);
	DBGUPOL_Open(115200);// allow serial bus transmision
	
	LCD_ClrScr();
	LCD_WriteString("DIOD CHANGE");

	pmcTimerInit();

	dummy = PIO->INT_STATUS_R;

	AIC_Configure_IRQ(PIOA_PERIPH_ID,
					 (AIC_EXTERNAL_POSITIVE_EDGE_TRIGGERED | AIC_PRIORITY_1),
					  PIO_PA15Irq );
					  
	PIO_InterruptEnable(PIO_PA15);

	while(1){ // main loop
		DBGUPOL_WriteString("I'M ALIVE\n");// serial transmision 
		sprintf(str, "%d\n", interupt_conter);
		DBGUPOL_WriteString(str);
		DelayMS	(2000);
	}
}//* End
