/********************************************************************
  File: pit.c
  Desc: Driver (sterownik) timera PIT 

				Przedstawiony kod opisany jest w rozdziale 12.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*********************************************************************/
#include "proj_config.h"

#include "../periph_hw_h/pio.h"
#include "../periph_hw_h/pmc.h"
#include "../periph_hw_h/aic.h"
#include "../periph_hw_h/led_sw.h"
#include "../periph_hw_h/board_pins.h"
#include "../periph_hw_h/dbgu_debug.h"
#include "../periph_hw_h/sys_intrpt_handler.h"
#include "../periph_hw_h/pit.h"
//#include "../periph_ui_h/kb_pit.h"

#include <string.h>
#include <stdio.h>
//----------------------------------------------------------------------------------


//----------------------------------------------------------------------------------

#define LEDWIZ  0  //1-wizualizacja modulu, 0-brak wizualizacji

//---------------------------------------------------------------------------------

unsigned int it =0;
int irq_flaga ;
unsigned int PIT_SysTime;						
unsigned int PIT_Configured_Time_us;			
//----------------------------------------------------------------------------------
void PIT_irq_kernel( unsigned int status_r )
{
	
 int val, dt;
 		//status_r = status_r;					
		val = PIT->VALUE_R;						
		dt = (val & PIT_COUNTER_field) >> PIT_COUNTER_bsf;	 
		PIT_SysTime += dt;					
		
		
		Global.it_value = Global.it_counter;
		Global.it_counter = 0;
		LED_ToggleOne( PIO_PA2);
		
		//...													
		//KB_CheckState();							// skanowanie klawiatury
}

//----------------------------------------------------------------------------------
void PIT_Open( int czas_us )
{	
 unsigned int val, dummy;
 
	PIT->MODE_R = ~(PIT_ENABLE | PIT_INT_ENABLE);				
	while( PIT->VALUE_R & PIT_CURRENT_VALUE_field ){}; 	
	
	PIT_SysTime	= 0;
 	PIT_Configured_Time_us = czas_us;		

	dummy = PIT->VALUE_R;		
	AIC_Configure_IRQ(	SYS_PERIPH_ID, 				
										 (AIC_EXTERNAL_HIGH_LEVEL_TRIGGERED | AIC_PRIORITY_LOWEST),
											SYS_INTRPT_irq_handler ); 	

	val = PIT_Calc_VALUE( czas_us );
	PIT->MODE_R = val | PIT_ENABLE | PIT_INT_ENABLE;

	DBGU_FlushWrite();
	PIT_FormatInfo( DBGU_TxBuf );
	DBGU_AsyncWriteString( DBGU_TxBuf );
}

//----------------------------------------------------------------------------------
unsigned int PIT_Calc_VALUE( unsigned int czas_us )
{ 
	return(  ((czas_us * ((fMCK_Hz/16)/1000) ) / 1000)   );
}
//----------------------------------------------------------------------------------
unsigned int PIT_GetDeltaTime( void )
{	
	unsigned int val =  PIT->IMAGE_R;
	return( val >> (PIT_COUNTER_bsf-4) );	
}
//----------------------------------------------------------------------------------
void PIT_FormatInfo( char *p )
{
	sprintf(p, "Starting system timer:\n\r"
						 " PIT_Configured_Time_us=%i[us],\n\r"
						 " PIT_SysTime=%i[ts]\n\r", 							
								PIT_Configured_Time_us, 
								PIT_SysTime );
}									 

