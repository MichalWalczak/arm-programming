/********************************************************************
  File: tc_def.h   (notacja JagNOT)
  Desc: Timery TC

				Przedstawiony kod opisany jest w rozdziale 15.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*********************************************************************/
#ifndef _TC_DEF_H_INCLUDED_
#define _TC_DEF_H_INCLUDED_
#include "../periph_hw_h/reg_typedef.h"

//----------------------------------------------------------------------
typedef struct  _TC {
	REG	 	CONTROL_R;				// WO, 			-, 		+0x00,	CCR	
	REG	 	MODE_R; 				// R/W, 		0x0, 	+0x04,	MR
	REG	 	Reserved0[2]; 		
	REG	 	COUNTER_VALUE; 			// RO,			0x0,	+0x10,	CV
	REG	 	RA; 					// R/W-RO		0x0,	+0x14,	RA
	REG	 	RB; 					// R/W-RO,	0x0,	+0x18,	RB
	REG	 	RC; 					// R/W,			0x0,	+0x1C,	RC
	REG	 	INT_STATUS_R; 			// RO,			0x0,	+0x20,	SR	
	REG	 	INT_ENABLE_R; 			// WO, 			-,		+0x24,	IER
	REG	 	INT_DISABLE_R; 			// WO, 			-,		+0x28,	IDR		
	REG	 	INT_MASK_R;				// RO, 			0x0,	+0x2C,	IMR
} _TC, *PS_TC;

#define 	TC0   ((PS_TC) 0xFFFA0000) 		
#define 	TC1   ((PS_TC) 0xFFFA0040) 		
#define  	TC2   ((PS_TC) 0xFFFA0080) 		

#define TC0_PERIPH_ID 			12   				
#define TC0_PERIPH_ID_MASK (0x1<<TC0_PERIPH_ID) 	
#define TC1_PERIPH_ID 			13				 	
#define TC1_PERIPH_ID_MASK (0x1<<TC1_PERIPH_ID)
#define TC2_PERIPH_ID				14					
#define TC2_PERIPH_ID_MASK (0x1<<TC2_PERIPH_ID)
 

//-------------------------------------------------------------------
// ----- CONTROL_R -------------
		#define TC_CLK_ENABLE       0x1
		#define TC_CLK_DISABLE      0x2
		#define TC_SOFTWARE_TRIGGER 0x4		

//--------------------------------------------------------------------------
// Bity rejestrow: _INT_ENABLE, INT_DISABLE, INT_MASK, INT_STATUS
		#define TC_COUNTER_OVERFLOW   0x1   
		#define TC_LOAD_OVERRUN       0x2   
		#define TC_COMPARE_RA         0x4   
		#define TC_COMPARE_RB         0x8   
		#define TC_COMPARE_RC        0x10   
		#define TC_LOAD_RA           0x20   
		#define TC_LOAD_RB           0x40   
		#define TC_EXTERNAL_TRIGGER  0x80   
	// bity tylko w rejestrze INT_STATUS_R
		#define TC_CLOCK_STATUS   0x10000   
		#define TC_MIRROR_TIOA    0x20000   
		#define TC_MIRROR_TIOB    0x40000   


//-------------------------------------------------------------------
// WAVE/PWM: rejestr MODE_R : w trybie WAVE/PWM
	// clock source [2..0]
		#define TC_MCK2    		0x0
		#define TC_MCK8    		0x1
		#define TC_MCK32   		0x2
		#define TC_MCK128  		0x3
		#define TC_MCK1024 		0x4
		#define TC_XC0       	0x5
		#define TC_XC1       	0x6
		#define TC_XC2       	0x7
	//pole clock inwerted,	CLKI [.3]
		#define TC_CLOCK_INVERTED  		 		((unsigned int) 0x1 << 3) 
		#define TC_CLOCK_NOT_INVERTED  		((unsigned int) 0x0 << 3)
	//pole gated clock, BURST [5..4]
		#define TC_CLOCK_GATED_BY_NONE 		((unsigned int) 0x0 <<  4)
		#define TC_CLOCK_GATED_BY_XC0	 		((unsigned int) 0x1 <<  4)
		#define TC_CLOCK_GATED_BY_XC1	 		((unsigned int) 0x2 <<  4)
		#define TC_CLOCK_GATED_BY_XC2	 		((unsigned int) 0x3 <<  4)

	// pole stop on RC_COMPARE, CPCSTOP [.6]	 
		#define TC_STOP_ON_RC_COMPARE			((unsigned int) 0x1 <<  6)
		#define TC_NOT_STOP_ON_RC_COMPARE	((unsigned int) 0x0 <<  6)																	 
	// pole disable counter clock on RC_COMPARE (chyba one-shot), CPCDIS [.7]
	  #define TC_DISABLE_COUNTER_CLOCK_ON_RC_COMPARE      ((unsigned int) 0x1 <<  7)
		#define TC_NOT_DISABLE_COUNTER_CLOCK_ON_RC_COMPARE  ((unsigned int) 0x0 <<  7)             

	// pole External event edge selection, EEVTEDG [9..8]
		#define TC_EXTERNAL_EVT_EDGE_NONE		 ((unsigned int) 0x0 <<  8)
		#define TC_EXTERNAL_EVT_EDGE_RISING	 ((unsigned int) 0x1 <<  8)
		#define TC_EXTERNAL_EVT_EDGE_FALLING ((unsigned int) 0x2 <<  8)
		#define TC_EXTERNAL_EVT_EDGE_EACH		 ((unsigned int) 0x3 <<  8)           	
	// pole External event (source) selection, EEVT [11..10]
		#define TC_EXTERNAL_EVT_SEL_TIOB		((unsigned int) 0x0 <<  10) 
		#define TC_EXTERNAL_EVT_SEL_XC0			((unsigned int) 0x1 <<  10)
		#define TC_EXTERNAL_EVT_SEL_XC1			((unsigned int) 0x2 <<  10)
		#define TC_EXTERNAL_EVT_SEL_XC2			((unsigned int) 0x3 <<  10)	
	// pole External trigger event enable, ENETRG [.12]
		#define TC_EXTERNAL_EVT_TRIGGER_ENABLE_RESET_PV			((unsigned int) 0x1 << 12)	
		#define TC_EXTERNAL_EVT_TRIGGER_NOT_ENABLE_RESET_PV ((unsigned int) 0x0 << 12)
	
	// pole , WAVSEL [14..13]
		#define TC_MODE_UP_WITHOUT_AUTO_RESET_ON_RC_COMPARE			((unsigned int) 0x0 << 13)
		#define TC_MODE_UPDOWN_WITHOUT_AUTO_RESET_ON_RC_COMPARE ((unsigned int) 0x1 << 13)
		#define TC_MODE_UP_WITH_AUTO_RESET_ON_RC_COMPARE				((unsigned int) 0x2 << 13)
		#define TC_MODE_UPDOWN_WITH_AUTO_RESET_ON_RC_COMPARE		((unsigned int) 0x3 << 13)
	// pole, glowny tryb pracy, WAVE [.15]
		#define TC_MODE_WAVE_PWM			((unsigned int) 0x1 << 15) 
		#define TC_MODE_CAPTURE				((unsigned int) 0x0 << 15) 
									 
	// definicja zachowania wyprowadzenia TIOA
	// pola dla TIOA: TIOA on_RA_Compare, ACPA
		#define TC_ON_RA_COMPARE_TIOA_NONE	 		((unsigned int) 0x0 << 16)
		#define TC_ON_RA_COMPARE_TIOA_SET		 		((unsigned int) 0x1 << 16)
		#define TC_ON_RA_COMPARE_TIOA_CLEAR	 		((unsigned int) 0x2 << 16)
		#define TC_ON_RA_COMPARE_TIOA_TOGGLE 		((unsigned int) 0x3 << 16)
	// TIOA on_RC_Compare, ACPC
	 	#define TC_ON_RC_COMPARE_TIOA_NONE	 		((unsigned int) 0x0 << 18)
		#define TC_ON_RC_COMPARE_TIOA_SET		 		((unsigned int) 0x1 << 18)
		#define TC_ON_RC_COMPARE_TIOA_CLEAR	 		((unsigned int) 0x2 << 18)
		#define TC_ON_RC_COMPARE_TIOA_TOGGLE 		((unsigned int) 0x3 << 18)
	// TIOA on External Event, AAEVT
		#define TC_ON_EXTERNAL_EVENT_TIOA_NONE	 ((unsigned int) 0x0 << 20)
		#define TC_ON_EXTERNAL_EVENT_TIOA_SET		 ((unsigned int) 0x1 << 20)
		#define TC_ON_EXTERNAL_EVENT_TIOA_CLEAR	 ((unsigned int) 0x2 << 20)
		#define TC_ON_EXTERNAL_EVENT_TIOA_TOGGLE ((unsigned int) 0x3 << 20)
	// TIOA on Software Trigger, ASWTRG
		#define TC_ON_SOFT_TRIGGER_TIOA_NONE	 	((unsigned int) 0x0 << 22)
		#define TC_ON_SOFT_TRIGGER_TIOA_SET		 	((unsigned int) 0x1 << 22)
		#define TC_ON_SOFT_TRIGGER_TIOA_CLEAR	 	((unsigned int) 0x2 << 22)
		#define TC_ON_SOFT_TRIGGER_TIOA_TOGGLE 	((unsigned int) 0x3 << 22)

	// definicja zachowania wyprowadzenia TIOB
 	// pola dla TIOB: TIOB on_RB_Compare, BCPB
		#define TC_ON_RB_COMPARE_TIOB_NONE	 		((unsigned int) 0x0 << 24)
		#define TC_ON_RB_COMPARE_TIOB_SET		 		((unsigned int) 0x1 << 24)
		#define TC_ON_RB_COMPARE_TIOB_CLEAR	 		((unsigned int) 0x2 << 24)
		#define TC_ON_RB_COMPARE_TIOB_TOGGLE 		((unsigned int) 0x3 << 24)
	// TIOb on_RC_Compare, BCPC
	 	#define TC_ON_RC_COMPARE_TIOB_NONE	 		((unsigned int) 0x0 << 26)
		#define TC_ON_RC_COMPARE_TIOB_SET		 		((unsigned int) 0x1 << 26)
		#define TC_ON_RC_COMPARE_TIOB_CLEAR	 		((unsigned int) 0x2 << 26)
		#define TC_ON_RC_COMPARE_TIOB_TOGGLE 		((unsigned int) 0x3 << 26)
	// TIOB on External Event, BAEVT
		#define TC_ON_EXTERNAL_EVENT_TIOB_NONE	 ((unsigned int) 0x0 << 28)
		#define TC_ON_EXTERNAL_EVENT_TIOB_SET		 ((unsigned int) 0x1 << 28)
		#define TC_ON_EXTERNAL_EVENT_TIOB_CLEAR	 ((unsigned int) 0x2 << 28)
		#define TC_ON_EXTERNAL_EVENT_TIOB_TOGGLE ((unsigned int) 0x3 << 28)
	// TIOB on Software Trigger, BSWTRG
		#define TC_ON_SOFT_TRIGGER_TIOB_NONE	 	((unsigned int) 0x0 << 30)
		#define TC_ON_SOFT_TRIGGER_TIOB_SET		 	((unsigned int) 0x1 << 30)
		#define TC_ON_SOFT_TRIGGER_TIOB_CLEAR	 	((unsigned int) 0x2 << 30)
		#define TC_ON_SOFT_TRIGGER_TIOB_TOGGLE 	((unsigned int) 0x3 << 30)



#endif // of_TC_DEF_H_INCLUDED_
