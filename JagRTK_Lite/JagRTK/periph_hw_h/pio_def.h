/********************************************************************
  File: pio_def.h   (notacja JagNOT)
  Desc: pio

				Przedstawiony kod opisany jest w rozdziale 4 i 7.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*********************************************************************/
#ifndef _PIO_DEF_H_INCLUDED_
#define _PIO_DEF_H_INCLUDED_
#include "../periph_hw_h/reg_typedef.h"

//--------------------------------------------------------------------
typedef struct S_PIO {								
	REG	 PIO_ENABLE_R; 									// PER
	REG	 PIO_DISABLE_R; 								// PDR
	REG	 PIO_STATUS_R; 									// PSR
	REG	 rezerw0[1]; 										// 
	REG	 OUTPUT_ENABLE_R; 							// OER
	REG	 OUTPUT_DISABLE_R; 							// ODR
	REG	 OUTPUT_STATUS_R; 							// OSR
	REG	 rezerw1[1]; 										// 
	REG	 INPUT_FILTER_ENABLE_R; 				// 
	REG	 INPUT_FILTER_DISABLE_R; 				// 
	REG	 INPUT_FILTER_STATUS_R; 				// 
	REG	 rezerw2[1]; 										// 
	REG	 SET_OUTPUT_DATA_R; 						// SODR
	REG	 CLEAR_OUTPUT_DATA_R; 					// CODR
	REG	 OUTPUT_DATA_STATUS_R; 					// ODSR
	REG	 PIN_DATA_STATUS_R; 						// PDSR
	
	REG	 INT_ENABLE_R; 									// 
	REG	 INT_DISABLE_R; 								// 
	REG	 INT_MASK_R; 										//
	REG	 INT_STATUS_R; 									//

	REG	 MULTI_DRIVE_ENABLE_R; 					// MDER
	REG	 MULTI_DRIVE_DISABLE_R; 				// MDDR
	REG	 MULTI_DRIVE_STATUS_R; 					// MDSR
	REG	 rezerw3[1]; 										// 
	REG	 PULLUP_DISABLE_R; 							// PPUDR
	REG	 PULLUP_ENABLE_R; 							// PPUER
	REG	 PULLUP_STATUS_R; 							// PPUSR
	REG	 rezerw4[1]; 										// 
	REG	 PERIPH_A_SELECT_R; 						// ASR
	REG	 PERIPH_B_SELECT_R; 						// BSR
	REG	 PERIPH_AB_STATUS_R; 						// ABSR
	REG	 rezerw5[9]; 										// 
	REG	 OUTPUT_WRITE_MASK_ENABLE_R; 		// OWER 
	REG	 OUTPUT_WRITE_MASK_DISABLE_R; 	// OWDR
	REG	 OUTPUT_WRITE_MASK_STATUS_R; 		// OWSR
} S_PIO, *PS_PIO;

#define PIO   ((PS_PIO) 0xFFFFF400) 

#define PIOA_PERIPH_ID  		((unsigned int)  2) 
#define PIOA_PERIPH_ID_MASK ((unsigned int) 0x1 << PIOA_PERIPH_ID)


#define IRQ0_PERIPH_ID  		((unsigned int) 30) 
#define IRQ0_PERIPH_ID_MASK ((unsigned int) 0x1 << IRQ0_PERIPH_ID)

#define IRQ1_PERIPH_ID  		((unsigned int) 31) 
#define IRQ1_PERIPH_ID_MASK ((unsigned int) 0x1 << IRQ1_PERIPH_ID)

#define FIQ_PERIPH_ID				((unsigned int) 0)
#define FIQ_PERIPH_ID_MASK	((unsigned int) 0x1 << FIQ_PERIPH_ID)

//------------------------------------------------------------
#define  PIN_ASSIGNED_TO_PERIPH_A	0x0  
#define  PIN_ASSIGNED_TO_PERIPH_B	0x1	 

//---------------------------------------------------------------------------
//procesor SAM7X 
#define PIOB	((PS_PIO) 0xFFFFF600) 
#define PIOB_PERIPH_ID  	((unsigned int)  3) 
#define PIOB_PERIPH_ID_MASK  ((unsigned int) 0x1 << PIOB_PERIPH_ID)
//---------------------------------------------------------------------------



#endif // _PIO_DEF_H_INCLUDED_
