/********************************************************************
  File: dbgu_debug.c
  Desc: Driver (sterownik) portu DBGU przeznaczony do 
        debuggowania/monitorowania/komunikacji z uzytkownikiem.
				Obsluga DMA/INT

				Przedstawiony kod opisany jest w rozdziale 11.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************************************************/
#include <stdio.h>
#include "../periph_hw_h/bit_names.h"
#include "../periph_hw_h/reg_typedef.h"
#include "../periph_hw_h/pio.h"
#include "../periph_hw_h/board_pins.h"
#include "../periph_hw_h/led_sw.h"
#include "../periph_hw_h/pmc.h"
#include "../periph_hw_h/aic.h"
#include "../periph_hw_h/pdc.h"
#include "../periph_hw_h/delay.h"

#include "proj_config.h"
#include <string.h>

#include "../periph_hw_h/dbgu_debug.h"
#include "../periph_hw_h/sys_intrpt_handler.h"


//-----------------------------------------------------------------------------------

#define LEDWIZ	0		// 1-wizualizacja modulu, 0-wylaczona

//----------------------------------------------------------------------------------
#define 		DBGU_TX_SOFT_BUF_LEN  	100				
char 				DBGU_TxBuf[DBGU_TX_SOFT_BUF_LEN];	


int  DBGU_BaudRate;
unsigned char DBGU_TraceFlag=1;  
//----------------------------------------------------------------------------------
int  DBGU_irq_counter;
void DBGU_irq_kernel( int status_and_mask, int status_r )
{	
 char znak;

	DBGU_irq_counter++;														
	LEDWIZ_ToggleOne( PIO_PA1, LEDWIZ );					

		if( DBGU_RX_RDY & status_and_mask ){
			znak = DBGU->RECEIVER_HOLDING_R;
			if( (DBGU_OVERRUN_ERR|DBGU_FRAME_ERR|DBGU_PARIRY_ERR) & status_r ){				
				DBGU->CONTROL_R = DBGU_RESET_ERROR_BITS;
				return;
			}
			//DBGU_Execute_Command( znak );
		}	
}

//----------------------------------------------------------------------------------
void DBGU_Open( int baudrate )
{
	DBGU->INT_DISABLE_R  = 0xFFFFFFFF;		
	PDC_DisableAndResetWrite( PDC_DBGU );
	PDC_DisableAndResetRead( PDC_DBGU );

  DBGU->CONTROL_R = DBGU_RESET_TX | DBGU_TX_DISABLE
								  | DBGU_RESET_RX | DBGU_RX_DISABLE;

	DBGU_ReConfigure( baudrate, (DBGU_PARITY_NONE|DBGU_MODE_NORMAL) );

	AIC_Configure_IRQ( SYS_PERIPH_ID, 				
										 (AIC_LEVEL_TRIGGERED | AIC_PRIORITY_LOWEST),
										 SYS_INTRPT_irq_handler ); 	

	DBGU->CONTROL_R = DBGU_TX_ENABLE | DBGU_RX_ENABLE;

	#if (PROCESSOR_TYPE == PROCESSOR_SAM7S)
		PIO_OpenPeriph_A( DRXD_A9 | DTXD_A10 ); 
	#elif (PROCESSOR_TYPE == PROCESSOR_SAM7X)
		PIO_OpenPeriph_A( DRXD_AA27 | DTXD_AA28 ); 
	#else
		#error "nieznany typ procesora"
	#endif

	DBGU_BaudRate=baudrate;
	DBGU_TraceFlag=1;  
	
	DBGU->INT_ENABLE_R  = DBGU_RX_RDY;		

	//---------------------------- intro --------------------
	DBGU_Trace("\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r", NULL);
	DBGU_Info( baudrate );
}
//----------------------------------------------------------------------------------
void DBGU_ReConfigure( unsigned int baud_rate, 						
											 unsigned int mode_and_parity )			
{
	unsigned int baud_value = ((fMCK_Hz*10)/(baud_rate * 16));
	if ((baud_value % 10) >= 5)		baud_value = (baud_value / 10) + 1;
	else													baud_value /= 10;
	DBGU->BAUD_RATE_GEN_R = baud_value;
	DBGU->MODE_R = mode_and_parity;   
}


//-----------------------------------------------------------------------------------
void DBGU_AsyncWrite( char *p, int ile )
{
	while( PDC_DBGU->TX_NEXT_CNT_R ){};			
	PDC_DisableWrite( PDC_DBGU ); 					
		if( PDC_DBGU->TX_CNT_R ){							
			PDC_SetNextTx( p, ile, PDC_DBGU );
		}else{
	 		PDC_SetTx( p, ile, PDC_DBGU );
		}
	PDC_EnableWrite( PDC_DBGU );								
}
//-----------------------------------------------------------------------------------
void DBGU_FlushWrite( void )
{
	while( PDC_DBGU->TX_CNT_R || PDC_DBGU->TX_NEXT_CNT_R ){}; 
}
//-----------------------------------------------------------------------------------
int DBGU_IsWriteCompleted( void )						 
{
	return( !( PDC_DBGU->TX_CNT_R || PDC_DBGU->TX_NEXT_CNT_R ));
}
//----------------------------------------------------------------------------------
void DBGU_AsyncWriteString( char *p )
{
 	DBGU_AsyncWrite( p, strlen(p) );	 
}
//----------------------------------------------------------------------------------
void DBGU_AsyncWriteHex( char *p, int len )
{	
 int i, ta_paczka; char *pbuf;
 int ile_jeszcze = len;
 	
	while( ile_jeszcze > 0 ){			
		DBGU_FlushWrite();					
		pbuf=DBGU_TxBuf;			 	
		ta_paczka = (ile_jeszcze<(DBGU_TX_SOFT_BUF_LEN/2)) ? ile_jeszcze:(DBGU_TX_SOFT_BUF_LEN/2);
		for(i=0;i<ta_paczka;i++){
			sprintf( pbuf, "%02X", p[i] );
			pbuf += 2; 
		}				
		DBGU_AsyncWrite( DBGU_TxBuf, ta_paczka*2 );	 
		ile_jeszcze -= ta_paczka;
	}
}


//-----------------------------------------------------------------------------------
void DBGU_Info( unsigned int baudrate )
{
	DBGU_AsyncWriteString("\n\r");
	DBGU_AsyncWriteString("                                 \n\r");	DBGU_FlushWrite();	
	DBGU_AsyncWriteString(" JagRTK(periph) on 32-bit RISC. STARTING.\n\r" ); DBGU_FlushWrite();

	sprintf    (DBGU_TxBuf, " DBGU_BAUD=%i (viaPDC).\n\r", baudrate);
	DBGU_AsyncWriteString( DBGU_TxBuf );	 		DBGU_FlushWrite();

	DBGU_AsyncWriteString("\n\r");
	DBGU_FlushWrite(); 
}



