/***********************************************************************
  File: board_pins.h
  Desc: Warunkowe przylaczenia header'a z definicjami wyprowadzen (pinow PIOA/B)

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*************************************************************************/
#ifndef _BOARD_PINS_H_INCLUDED_
#define _BOARD_PINS_H_INCLUDED_
#include "proj_config.h"

#if 	(PROCESSOR_TYPE == PROCESSOR_SAM7S)
 	#include "JagRTK/periph_hw_h/pioa7s_def.h" 
#elif (PROCESSOR_TYPE == PROCESSOR_SAM7X)
 	#include "periph_hw_h/pioab7x_def.h"
#elif
  #error "Nieznany typ procesora w pliku board_pins.h"
#endif


#if 	(KIT_TYPE == SAM7S_KIT_SPARROW_1)
	#include "JagRTK/periph_hw_h/board_7s_sparrow_1.h" 
#elif (KIT_TYPE == SAM7S_KIT_PROPOX)
 	#include "periph_hw_h/board_7s_propox.h" 
#elif (KIT_TYPE == SAM7X_KIT_ATMEL)
 	#include "periph_hw_h/board_7x_atmel.h"
#elif (KIT_TYPE == SAM7X_KIT_OLIMEX)
	#include "periph_hw_h/board_7x_olimex.h"
#elif 	
  #error "Nieznany typ kita w pliku board_pins.h"
#endif



#endif //_BOARD_PINS_H_INCLUDED_
