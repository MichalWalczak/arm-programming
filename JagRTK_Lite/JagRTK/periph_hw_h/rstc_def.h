/********************************************************************
  File: rstc_def.h   (notacja JagNOT)
  Desc: defincja struktury rejestrow i stalych symbolicznych kontrolera RSTC

				Przedstawiony kod opisany jest w rozdziale 5.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*********************************************************************/
#ifndef _RSTC_DEF_H_INCLUDED_
#define _RSTC_DEF_H_INCLUDED_
#include "../periph_hw_h/reg_typedef.h"


void	RSTC_EnableUserReset( void );
void	RSTC_ResetPeripherial( void );

//-----------------------------------------------------------------------------
typedef struct _RSTC {
	REG	CONTROL_R; 			// WO, 	-,  		+0x0,  CR
	REG	STATUS_R; 			// RO, 	0x0,		+0x4,  SR
	REG	MODE_R; 				// R/W, 0x0,		+0x8,  MR
} _RSTC, *PS_RSTC;

#define RSTC   ((PS_RSTC) 0xFFFFFD00) 


// -------- CONTROL_R -------- 
#define RSTC_PROCESSOR_RESET  	((unsigned int) 0x1 <<  0) 
#define RSTC_ICE_RESET     			((unsigned int) 0x1 <<  1) 
#define RSTC_PERIPH_RESET   		((unsigned int) 0x1 <<  2) 
#define RSTC_EXTERNAL_RESET   	((unsigned int) 0x1 <<  3) 
#define RSTC_KEY_bsf     				24
#define RSTC_KEY_field     			((unsigned int) 0xFF << RSTC_KEY_bsf) 
// -------- MODE_R -------- 
#define RSTC_USER_RESET_EN     	((unsigned int) 0x1 <<  0) 
#define RSTC_USER_RESET_INT_EN	((unsigned int) 0x1 <<  4) 
#define RSTC_EXT_RESET_LENGTH_bsf		8
#define RSTC_EXT_RESET_LENGTH_field ((unsigned int) 0xF << 8) 
#define RSTC_BOD_INT_ENABLE     ((unsigned int) 0x1 << 16) 
#define RSTC_KEY_PASSWORD       ((unsigned int) 0xFF << 24)
// -------- STATUS_R -------- 
#define RSTC_USER_RESET_STATUS  ((unsigned int) 0x1 <<  0) 
#define RSTC_BOD_STATUS     		((unsigned int) 0x1 <<  1) 
#define RSTC_RESET_TYPE     		((unsigned int) 0x7 <<  8) 
#define 	RSTC_RST_TYPE_POWER_UP     ((unsigned int) 0x0 <<  8) 
#define 	RSTC_RST_TYPE_WATCHDOG     ((unsigned int) 0x2 <<  8) 
#define 	RSTC_RST_TYPE_SOFTWARE     ((unsigned int) 0x3 <<  8) 
#define 	RSTC_RST_TYPE_USER         ((unsigned int) 0x4 <<  8) 
#define 	RSTC_RST_TYPE_BROWNOUT     ((unsigned int) 0x5 <<  8) 
#define RSTC_NRST_PIN_STATE_LEVEL    ((unsigned int) 0x1 << 16) 
#define RSTC_SOFTW_RESET_COMMAND_IN_PROGRESS  ((unsigned int) 0x1 << 17) 


#endif // of _RSTC_DEF_H_INCLUDED_
