/***********************************************************************
	File: Periph_Startup_C_JagRTK_Lite_GNU.c
  Desc: Inicjalizacja kluczowych urzadzen peryferyjnych
				z poziomu jezyka C

    By: JAG (Jacek Augustyn)  
   Ver: 0.1G / 2007.08.31
	 Web: www.sparrow-rt.com

   * Copyright (C) 2006-2007 by JAG Jacek Augustyn. All rights reserved.
   *
   * Redistribution and use in source and binary forms, with or without
   * modification, are permitted provided that the following conditions
   * are met:
   *
   * 1. Redistributions of source code must retain the above copyright
   *    notice, this list of conditions and the following disclaimer.
   * 2. Redistributions in binary form must reproduce the above copyright
   *    notice, this list of conditions and the following disclaimer in the
   *    documentation and/or other materials provided with the distribution.
   * 3. Neither the name of the copyright holders nor the names of
   *    contributors may be used to endorse or promote products derived
   *    from this software without specific prior written permission.
   *
   * THIS SOFTWARE IS PROVIDED BY JAG AND CONTRIBUTORS
   * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
   * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL EGNITE
   * SOFTWARE GMBH OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
   * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
   * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
   * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
   * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
   * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
   * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
   * SUCH DAMAGE.

************************************************************************/
#include "proj_config.h"
#include "../periph_hw_h/reg_typedef.h"
#include "../periph_hw_h/rstc_def.h"
#include "../periph_hw_h/pmc_def.h"
#include "../periph_hw_h/wdt_def.h"
#include "../startup_h/startup_C.h"
#include "../periph_hw_h/pit_def.h"
#include "../periph_hw_h/rtt_def.h" 
#include "../periph_hw_h/dbgu_def.h"


//---------------------------------------------------------------------------
#define REG_FLASH_MODE_R					(*((volatile unsigned int*) 0xFFFFFF60))
#define REG_FLASH_REMAP_CONTROL_R	(*((volatile unsigned int*) 0xFFFFFF00))

#define MC_WAIT_STATE_1					(0x1 << 8)
#define MC_REMAP_CMD_BIT				0x1
#define MC_MCK_PER_1_5_US_bsf 	16
//---------------------------------------------------------------------------
void Periph_Startup_SAMBA_FLASH( void );
void Periph_Startup_JTAG_RAM( void );
void Periph_Startup_JTAG_FLASH( void );

//---------------------------------------------------------------------------
void Periph_Startup_7s_addl_c( int MemoryType )
{
 	if( MemoryType == JTAG_RAM_MEMORY_TYPE )						Periph_Startup_JTAG_RAM();
	else if ( MemoryType == SAMBA_FLASH_MEMORY_TYPE )		Periph_Startup_SAMBA_FLASH();
	else if ( MemoryType == JTAG_FLASH_MEMORY_TYPE )		Periph_Startup_JTAG_FLASH();
	else																								Periph_Startup_SAMBA_FLASH();
}
//--------------------------------------------------------------------------
void Periph_Startup_JTAG_RAM( void )
{
	AIC_Startup();

	if( !MC_IsRAMRemapped() ){ 	
 		MC_REMAP();		
	}	
	WDT_Startup();
	MC_Startup();
	#if (MCK == 47923200 )
		PMC_Startup_48MHz();
	#else
		#error "Konfigurowana czestotliwosc MCK jest rozna od 47923200Hz (plik Periph_Startup_C)"
	#endif
}
//--------------------------------------------------------------------------
void Periph_Startup_SAMBA_FLASH( void )	 
{
	RSTC_Startup();		
	WDT_Startup();
	MC_Startup();
	#if (MCK == 47923200 )
		PMC_Startup_48MHz();
	#else
		#error "Konfigurowana czestotliwosc MCK jest rozna od 47923200Hz (plik Periph_Startup_C)"
	#endif
}
//--------------------------------------------------------------------------
void Periph_Startup_JTAG_FLASH( void )	 
{
	//...
} 



//--------------------------------------------------------------------------
void WDT_Startup( void )
{
 	WDT->MODE_R = WDT_DISABLE
							| WDT_IDLE_HALT
							| WDT_DEBUG_HALT;
}
//--------------------------------------------------------------------------
void MC_Startup( void )
{ 
	REG_FLASH_MODE_R = MC_WAIT_STATE_1
									 | 83 << MC_MCK_PER_1_5_US_bsf;	
}
//--------------------------------------------------------------------------
int 	MC_IsRAMRemapped( void )
{
 volatile unsigned int stan;
 volatile unsigned int *p;
 	p=(unsigned int*)0x0;		
	stan = *p;							
	*p=0x5A5A5A5A;					
	if( *p != 0x5A5A5A5A ){ return(0); }	
	*p=0xA5A5A5A5;					
	if( *p != 0xA5A5A5A5 ){ return(0); }	
	*p = stan;							
 	return( 1 );						
}
//--------------------------------------------------------------------------
void MC_REMAP( void )
{
	REG_FLASH_REMAP_CONTROL_R = MC_REMAP_CMD_BIT;
}
//--------------------------------------------------------------------------
void PMC_Startup_48MHz( void )
{
	PMC->MAIN_OSC_R = PMC_MAIN_OSC_ENABLE
									| (0x6 << PMC_OSC_COUNT_bsf);
	while( !(PMC_MAIN_OSC_STATUS & PMC->INT_STATUS_R) ){};	
	
	PMC->PLL_R =  (5 << PMC_DIV_bsf)						
						 | (25 << PMC_MUL_bsf)		
						 |  (6 << PMC_PLL_COUNT_bsf) 			
						 | PMC_OUT_PLL_80_160
						 | PMC_USB_DIV_2;
	while( !(PMC_LOCK & PMC->INT_STATUS_R )){};	
	
	PMC->MCK_R = PMC_SEL_PLLCLK
						 | PMC_PRESC_2;
}

//--------------------------------------------------------------------------
void RSTC_Startup( void )
{
 	RSTC->MODE_R = RSTC_USER_RESET_EN 
							 | 0x4 << RSTC_EXT_RESET_LENGTH_bsf	 	
							 | 0xA5 << RSTC_KEY_bsf;
}


#define REG_AIC_INT_DISABLE_R  (*((volatile unsigned int*)   0xFFFFF124))
#define REG_AIC_EOI_R				   (*((volatile unsigned int*)   0xFFFFF130))
//---------------------------------------------------------------------------------
void AIC_Startup( void )
{
 volatile int i;
	REG_AIC_INT_DISABLE_R = 0xFFFFFFFF;		
	for(i=0;i<8;i++){
	 	REG_AIC_EOI_R = 0x0; 
	}
	
	PIT->MODE_R = ~(PIT_ENABLE | PIT_INT_ENABLE);	  
	RTT->MODE_R &= ~((unsigned int)RTT_ALARM_INT_ENABLE);
	DBGU->INT_DISABLE_R  = 0xFFFFFFFF;		
}

