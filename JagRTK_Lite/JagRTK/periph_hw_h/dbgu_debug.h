/********************************************************************
  File: dbgu_debug.h
  Desc: Driver (sterownik) portu DBGU przeznaczony do 
        debuggowania/monitorowania/komunikacji z uzytkownikiem.
				Obsluga DMA/INT

				Przedstawiony kod opisany jest w rozdziale 11.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*********************************************************************/
#ifndef _DBGU_DEBUG_H_INCLUDED_
#define _DBGU_DEBUG_H_INCLUDED_
#include "../periph_hw_h/reg_typedef.h"
#include "../periph_hw_h/pio_def.h"
#include "../periph_hw_h/pdc.h"
#include "../periph_hw_h/dbgu_def.h"

#include "../periph_hw_h/sys_intrpt_handler.h"
#include <stdio.h>
#include <string.h>
///#include "periph_hw_h/dbgu_def.h"

//--------------------------------------------------------------------
extern char DBGU_TxBuf[];			
extern unsigned char DBGU_TraceFlag;
extern int DBGU_irq_counter;

//--------------------------------------------------------------------
#define DBGU_Trace(txt,zmn)  														\
	if( DBGU_TraceFlag ){ 																\
		DBGU_FlushWrite(); 																	\
		sprintf( DBGU_TxBuf, txt, zmn );  									\
		DBGU_AsyncWrite( DBGU_TxBuf, strlen(DBGU_TxBuf ) ); \
		}	
	

//------------------------------------------------------------------
void DBGU_AsyncWrite( char *p, int len );
void DBGU_AsyncWriteString( char *p );				
void DBGU_AsyncWriteHex( char *p, int len );	
int  DBGU_IsWriteCompleted( void );
void DBGU_FlushWrite( void );
//------------------------------------------------------------------
void DBGU_irq_kernel( int status_and_mask, int status_r );
void DBGU_Execute_Command( unsigned char znak );
void DBGU_Command_Info( void );

//------------------------------------------------------------------
void DBGU_Open( int baud );
void DBGU_ReConfigure( unsigned int baud_rate, 						
											 unsigned int mode_and_parity );		
void DBGU_Info( unsigned int baud );

#endif // _DBGU_DEBUG_H_INCLUDED_
