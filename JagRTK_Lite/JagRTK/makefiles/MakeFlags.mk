#***********************************************************************************
# File: MakeFlags
# Desc: makeflags
#   By: JAG (Jacek Augustyn)  
#  Ver: 0.2 / 2007.08.27
#  Web: www.sparrow-rt.com
#
#   * Copyright (C) 2006-2007 by JAG Jacek Augustyn. All rights reserved.
#   *
#   * Redistribution and use in source and binary forms, with or without
#   * modification, are permitted provided that the following conditions
#   * are met:
#   *
#   * 1. Redistributions of source code must retain the above copyright
#   *    notice, this list of conditions and the following disclaimer.
#   * 2. Redistributions in binary form must reproduce the above copyright
#   *    notice, this list of conditions and the following disclaimer in the
#   *    documentation and/or other materials provided with the distribution.
#   * 3. Neither the name of the copyright holders nor the names of
#   *    contributors may be used to endorse or promote products derived
#   *    from this software without specific prior written permission.
#   *
#   * THIS SOFTWARE IS PROVIDED BY JAG AND CONTRIBUTORS
#   * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#   * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL EGNITE
#   * SOFTWARE GMBH OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#   * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#   * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
#   * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
#   * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#   * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
#   * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
#   * SUCH DAMAGE.
#***********************************************************************************

#-- kompilator: -----------------------------------------------------------------------------
C_FLAGS      += -mcpu=arm7tdmi 
C_FLAGS      += -Wall -Wcast-align -Wimplicit -Wpointer-arith -Wswitch
C_FLAGS      += -Wredundant-decls -Wreturn-type -Wshadow -Wunused
ifeq ($(LST_C_ASM),YES)
  C_FLAGS    += -Wa,-adhlns=$(subst $(suffix $<),.lst,$<)
endif	
C_FLAGS      += $(patsubst %,-I%,$(EXTRAINCDIRS))

C_ONLY_FLAGS += -Wnested-externs
C_ONLY_FLAGS += -std=gnu99

# GENDEPFLAGS = -Wp,-M,-MP,-MT,$(*F).o,-MF,.dep/$(@F).d
GENDEPFLAGS   = -MD -MP -MF .dep/$(@F).d

ALL_C_FLAGS   = $(THUMB_IW) -I. $(C_FLAGS) $(GENDEPFLAGS)


#-- asembler: --------------------------------------------------------------------------------
ASM_FLAGS     = -mcpu=arm7tdmi $(A_DEFS) -Wa,-adhlns=$(<:.S=.lst),-g$(DEBUG)
ALL_ASM_FLAGS = $(THUMB_IW) -I. -x assembler-with-cpp $(ASM_FLAGS)


#-- linker: ----------------------------------------------------------------------------------
LD_FLAGS      = -nostartfiles -Wl,-Map=objs/$(TARGET).map,--cref
LD_FLAGS     += -mthumb-interwork -mthumb

MATH_LIB      = -lm
#-lc -lgcc -lnewlib-lpc -lm
#-lc -lm -lc -lgcc
#NEWLIBLPC    = -lnewlib-lpc

#CPLUSPLUS_LIB = -lstdc++


LD_FLAGS += -lc -lm -lc -lgcc
LD_FLAGS += $(NEWLIBLPC) $(MATH_LIB) $(CPLUSPLUS_LIB)	$(JAG_LIB)
LD_FLAGS += -Tsys/LD_$(RUN_MODE).ld
