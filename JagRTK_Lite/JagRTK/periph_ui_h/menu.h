/********************************************************************
  File: menu.h
  Desc: Sterownik (driver) MNU wielopoziomowego, hierarchicznego, nieblokujacego
				menu. 
				Wykorzystuje m.in. sterownik LCD

				Przedstawiony kod opisany jest w rozdziale 8.

				Szczegolowy opis programu, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*********************************************************************/
#ifndef _MENU_H_INCLUDED_
#define _MENU_H_INCLUDED_

#define MNU_MAX_NEST				5		
#define MNU_LINES						2
#define MNU_COLUMNS					16


#define PODMENU 							0
#define FUNKCJA_JEDNORAZOWA		1				
#define FUNKCJA_OKRESOWA 			2	
#define FUNKCJA_CIAGLA 				2

#define MNU_NOT_FIRST_FLAG 		0		
#define MNU_FIRST_FLAG 				1		


#define MNU_REDRAW						1
#define MNU_REDRAW_IF_CHANGE	0
#define MNU_FIRST_FC_CALL			1
#define MNU_NOT_FIRST_FC_CALL 0

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
typedef struct pozycja{
	unsigned char typ;			
	char *napis;
	void *adr_podmenu_lub_funkcji;
} pozycja, *ppozycja;

//---------------------------------------------------------------------------
typedef struct menu{
	int 			ilosc;
	pozycja **ppozycje;
} menu, *pmenu;

extern menu Menu0;			

//---------------------------------------------------------------------------
void MNU_Open( pmenu mnu_start );
void MNU_Execute( int znak );
int  MNU_Disp( signed char nr_poz, int redraw );
void MNU_WriteLine( char *p, int max );


#endif // _MENU_H_INCLUDED_
