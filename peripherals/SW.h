#ifndef SW_H
#define SW_H

#include "library\AT91SAM7S64.h"
#include "library\lib_AT91SAM7S64.h"

void SW_open(unsigned int keyb_mask)
{
	AT91F_PIO_CfgPullup(AT91C_BASE_PIOA,keyb_mask);
}

//--------------------------------------

unsigned int SW_isPressed(unsigned int keyb_mask)
{
	return ~(AT91F_PIO_GetInput(AT91C_BASE_PIOA) && keyb_mask);
}

#endif //SW_H

