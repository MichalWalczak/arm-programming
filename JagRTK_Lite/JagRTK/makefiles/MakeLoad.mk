#***********************************************************************************
# File: MakeLoad.mk
# Desc: makeload
#   By: JAG (Jacek Augustyn)  
#  Ver: 0.2 / 2007.08.27
#  Web: www.sparrow-rt.com
#
#   * Copyright (C) 2006-2007 by JAG Jacek Augustyn. All rights reserved.
#   *
#   * Redistribution and use in source and binary forms, with or without
#   * modification, are permitted provided that the following conditions
#   * are met:
#   *
#   * 1. Redistributions of source code must retain the above copyright
#   *    notice, this list of conditions and the following disclaimer.
#   * 2. Redistributions in binary form must reproduce the above copyright
#   *    notice, this list of conditions and the following disclaimer in the
#   *    documentation and/or other materials provided with the distribution.
#   * 3. Neither the name of the copyright holders nor the names of
#   *    contributors may be used to endorse or promote products derived
#   *    from this software without specific prior written permission.
#   *
#   * THIS SOFTWARE IS PROVIDED BY JAG AND CONTRIBUTORS
#   * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#   * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL EGNITE
#   * SOFTWARE GMBH OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#   * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#   * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
#   * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
#   * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#   * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
#   * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
#   * SUCH DAMAGE.
#***********************************************************************************


#---------------------------------------------------------------------------------------------
OPENOCD_SCRIPT_DIR = 'c:\Program Files\openocd-2007re131_JagScripts\'
#OPENOCD_DIR    		 = 'c:\Program Files\openocd-2007re131\bin\'
#OPENOCD        		 = $(OPENOCD_DIR)openocd-ftd2xx.exe
#OCD = openocd-ftd2xx


OPEN_OCD_PROGRAMATOR = jeszcze_nie_ustawiony_programator_
ifeq ($(PROGRAMATOR),OPEN_OCD_USB)
 	OPEN_OCD_PROGRAMATOR = 7s_gnu_uocd_
endif
ifeq ($(PROGRAMATOR),OPEN_OCD_WIGGLER)
 	OPEN_OCD_PROGRAMATOR = 7s_gnu_wig_
endif


ram:    build
	@echo
	@echo "Biezaca konfiguracja w make=" $(RUN_MODE) ". Ma byc ustawiona na JTAG_RAM !"
	@echo
	@cmd /c 'echo  load_binary objs/$(TARGET_SUBNAME)_JTAG_RAM.bin 0x200000  > c:\temp\zaladuj_RAM.ocd'
	$(OPENOCD_SCRIPT_DIR)$(OPEN_OCD_PROGRAMATOR)RAM.bat

flash:  build
	@echo
	@echo "Biezaca konfiguracja w make=" $(RUN_MODE) ". Ma byc ustawiona na SAMBA_FLASH !"
	@echo
	@cmd /c 'echo  flash write 0 objs/$(TARGET_SUBNAME)_SAMBA_FLASH.bin 0x0  > c:\temp\zaladuj_FLASH.ocd'
	$(OPENOCD_SCRIPT_DIR)$(OPEN_OCD_PROGRAMATOR)FLASH.bat
  
