/********************************************************************
  File: aic.h
  Desc: Sterownik (driver) kontrolera przerwan AIC

				Przedstawiony kod opisany jest w rozdziale 4 i 7.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)
   Ver: 0.1 / 2007.07.25
        0.2
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*********************************************************************/
#ifndef _AIC_H_INCLUDED_
#define _AIC_H_INCLUDED_
#include "../periph_hw_h/reg_typedef.h"
#include "../periph_hw_h/aic_def.h"


//--- wybor kompilatora i sposobu obslugi zagniezdzania procedur obslugi przerwan ---------
#if defined 	VIC_DIRECT_CALLS_IRQ_HANDLERS_GNU
	#include "../periph_hw_h/aic_nesting_gnu.h"
#elif defined VIC_DIRECT_CALLS_IRQ_HANDLERS_KEIL
	#include "../periph_hw_h/aic_nesting_keil.h"
#elif defined VIC_STARTUP_NEST_CALLS_IRQ_HANDLERS
	#include "../periph_hw_h/aic_nesting_in_startup.h"
#else
	#error "Niezdefiniowany/nieoprogramowany sposob obslugi przerwan (CALLS_IRQ_TYPE)."
#endif 



unsigned int AIC_Configure_IRQ (
						unsigned int priph_id,     						
						unsigned int priorytet_i_wyzwalanie,
						void (*adres_proc_obslugi) (void) ); 				

unsigned int AIC_Configure_FIQ (
						unsigned int priph_id,     				
						unsigned int typ_wyzwalania,				
						void (*adres_proc_obslugi)(void) ); 

inline void AIC_Enable2( unsigned int irq_id_mask );
inline void AIC_Disable2( unsigned int irq_id_mask );
inline void AIC_Enable( unsigned int irq_id_mask );
inline void AIC_Disable( unsigned int irq_id_mask );

inline void AIC_DisableAll( void );

void AIC_ConfigureDefaultSpuriousHandler( void );
void AIC_ConfigureSpurious( void (*adres_proc_obslugi)(void) );



#endif // of _AIC_H_INCLUDED_
