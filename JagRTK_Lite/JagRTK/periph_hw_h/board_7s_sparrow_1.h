/********************************************************************
  File: board_7s_sparrow_1.h
  Desc: Definicje wyprowadzen przyporzadkowanych w kicie 7s_sparrow_1
	      uwaga: zalozono takie polaczenia dodatkowych LED i LCD

				Szczegolowy opis programu, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************************************************/
#ifndef _BOARD_7S_SPARROW_1_H_INCLUDED_
#define _BOARD_7S_SPARROW_1_H_INCLUDED_
#if ( KIT_TYPE != SAM7S_KIT_SPARROW_1 )
	#error "Te definicje wyprowadzen przeznaczone sa dla kita SAM7S_SPARROW_1"
#endif

//--------------------------------------------------------------------------------------
// LED, LED2 i LED3 sa dolaczone zewnetrznie do wyprowadzen goldpin
#define 	LED0  			PIO_PA0
#define 	LED1  			PIO_PA1
#define 	LED2  			PIO_PA2		// dolaczana na zewnatrz
#define 	LED3  			PIO_PA3		// dolaczana na zewnatrz

//--------------------------------------------------------------------------------------
// switch												// funkcje alternatywne
#define 	SW0_ENTER 	PIO_PA10	// SSCTFsync		TIOA1
#define 	SW1_DOWN  	PIO_PA11  // SSCTclk			TIOB1
#define 	SW2_UP   	PIO_PA12	// SSCRclk			FIQ		  AD2
#define 	SW3_ESC   	PIO_PA26	// SSCRFsync		IRQ0		AD3


//--------------------------------------------------------------------------------------
// LCD, jest dolaczony zewnetrznie do wyprowadzen goldpin
#define LCD_E_PIN			PIO_PA4
#define LCD_RS_PIN			PIO_PA5
#define LCD_DATA_D4_PIN		PIO_PA6		
#define LCD_DATA_D5_PIN		PIO_PA7		
#define LCD_DATA_D6_PIN		PIO_PA8		
#define LCD_DATA_D7_PIN		PIO_PA9	
	
#define LCD_D4_D7_PINS (LCD_DATA_D4_PIN|LCD_DATA_D5_PIN|LCD_DATA_D6_PIN|LCD_DATA_D7_PIN)  //4kolejne !!!
//--- dla innej wersji funkcji drivera
#define LCD_SHIFT_LOW_NIBBLE	23	
#define LCD_SHIFT_HIGH_NIBBLE	19	

// Linie i kolumny wyswietlacza
//#if 0
	#define LCD_MAX_LINE_NR			2
	#define LCD_MAX_LINE_SIZE		16
//#else
// Linie i kolumny wyswietlacza
	//#define LCD_MAX_LINE_NR			4
	//#define LCD_MAX_LINE_SIZE		20
//#endif



#endif // _BOARD_7S_SPARROW_1_H_INCLUDED_
