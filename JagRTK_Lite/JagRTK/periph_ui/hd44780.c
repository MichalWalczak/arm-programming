/************************************************************
  File: hd44780.c
  Desc:	Sterownik (driver) wyswietlacza LCD typu hd44780

				Przedstawiony kod opisany jest w rozdziale 6 i 7.

				Szczegolowy opis programu, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
	 	 	 	0.1G - GNU / 2007.08.31
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************************************/
#include "../startup_h/startup_C.h"		
#include "../periph_hw_h/delay.h"
#include "../periph_hw_h/pio.h"
#include "../periph_hw_h/board_pins.h"
//#include "../periph_hw_h/dbgu_debug.h"
#include "../periph_ui_h/hd44780.h"

//----------------------------------------------------------------------------
#if (TYP_PROJEKTU==JTAG_RAM) 
	#define LCD_DELAY_WYPISYWANIE_ZNAKU  1 
#else
	#define LCD_DELAY_WYPISYWANIE_ZNAKU  15 
#endif

//-----------------------------------------------------------------------------
unsigned char LCD_LineNr, LCD_ColNr;
char LCD_TraceBuf[64];


//----------------------------------------------------------------------------		  
void LCD_WriteString(char *p)
{
	while(*p){								
		LCD_WriteByte( *p++ );	
	}
}
//---------------------------------------------------------------------------
void LCD_WriteStringUpToN( int n, char *p )
{
 int i;
 	for(i=0;i<n;i++){	
		if( *p==0x0 ) break;
	 	LCD_WriteByte( *p );
		p++;
	}
}
//---------------------------------------------------------------------------
void LCD_WriteN(char *p, unsigned char ile )
{
	while(ile--){						
		LCD_WriteByte(*p++);	
	}
}

//----------------------------------------------------------------------------
void LCD_WriteByte( unsigned char data_lcd )
{
	 LCD_SendNibble(data_lcd, LCD_DATA, 'H');
	 LCD_SendNibble(data_lcd, LCD_DATA, 'L');
	 LCD_Delay40us();
}
//----------------------------------------------------------------------------
void LCD_WriteCmd( unsigned char cmd_lcd )
{
	LCD_SendNibble(cmd_lcd, LCD_CMD, 'H');
	LCD_SendNibble(cmd_lcd, LCD_CMD, 'L');
	LCD_Delay40us();
}
//---------------------------------------------------------------------------
void LCD_WriteDirectN(char *pt, unsigned char ile )
{	
	while(ile--){						
		 LCD_SendNibble(*pt, LCD_DATA, 'H');
		 LCD_SendNibble(*pt, LCD_DATA, 'L');
		 pt++;
		 LCD_Delay40us();
	}
}
//----------------------------------------------------------------------------
void LCD_WriteStringRC(char row, char col, char *p)
{	
	LCD_GotoRC(row, col);
	while(*p){
		LCD_WriteByte( *p ); 	
		p++;
	}
}
//----------------------------------------------------------------------------
void LCD_GotoRC(char row, char col )
{
	if( row > (LCD_MAX_LINE_NR-1))  row = (LCD_MAX_LINE_NR-1);
	if( col > (LCD_MAX_LINE_SIZE-1))col = (LCD_MAX_LINE_SIZE-1);
	LCD_LineNr=row;
	LCD_ColNr =col;	
	switch(row){
		case 0:	col += (LCD_SET_DATA_PTR_R + 0 );		  break; 
		case 1:	col += (LCD_SET_DATA_PTR_R + 0x40 );	break; 
		case 2:	col += (LCD_SET_DATA_PTR_R + 0x14 );	break; 
		case 3:	col += (LCD_SET_DATA_PTR_R + 0x54 );	break; 
	}	
	LCD_WriteCmd( col );
	LCD_Delay2ms();  
}


//----------------------------------------------------------------------------
void LCD_Open(void)
{
//char wrb[]=" JagRTK  by JAG ";
int i;
		LCD_LineNr=0;
		LCD_ColNr=0;
		GPIO_OpenOutput( LCD_E_PIN | LCD_RS_PIN | LCD_D4_D7_PINS );
/*		DBGU_Trace("Wymagane podlaczenie HD44780:\n\r", NULL );
		DBGU_Trace(" maska D4_D7_PINS: 0x=%08X\n\r", (LCD_DATA_D4_PIN|LCD_DATA_D5_PIN|LCD_DATA_D6_PIN|LCD_DATA_D7_PIN)  );
		DBGU_Trace(" maska LCD_RS_PIN: 0x=%08X\n\r", LCD_RS_PIN );
		DBGU_Trace(" maska LCD_E_PIN:  0x=%08X\n\r", LCD_E_PIN );*/
		
		for(i=0;i<8;i++){	LCD_Delay2ms(); }  
   	LCD_SendNibble((LCD_KONFIG_PARAM_R | LCD_8BIT), LCD_CMD, 'H'); LCD_Delay2ms(); LCD_Delay2ms(); 
   	LCD_SendNibble((LCD_KONFIG_PARAM_R | LCD_8BIT), LCD_CMD, 'H'); LCD_Delay40us();  LCD_Delay40us();  LCD_Delay40us();
   	LCD_SendNibble((LCD_KONFIG_PARAM_R | LCD_8BIT), LCD_CMD, 'H'); LCD_Delay40us();  LCD_Delay40us();  LCD_Delay40us();
								
		LCD_SendNibble((LCD_KONFIG_PARAM_R | LCD_4BIT),LCD_CMD, 'H'); LCD_Delay40us();  LCD_Delay2ms(); 

  	LCD_WriteCmd((LCD_KONFIG_PARAM_R | LCD_4BIT | LCD_2LINIE | LCD_MATRYCA_5x7)); 
		LCD_WriteCmd((LCD_KONFIG_KURSOR_R | LCD_DISP_ON /*| LCD_KURSOR_OFF  LCD_KURSOR_BLOKOWY */));
   	LCD_WriteCmd((LCD_KONFIG_INKR_WST_R | LCD_INKREMENTACJA | LCD_NADPISYWANIE )); 
		LCD_WriteCmd((LCD_KONFIG_RUCHKIER_R | LCD_RUCH_KURSORA )); LCD_Delay2ms(); 
   	
//		DBGU_Trace(" printing banner...\n\r", NULL );
		LCD_ClrScr();
		LCD_Home();

  	/*for(i=0; i<16; i++){	
			LCD_WriteByte( wrb[i] );	LCD_DelayN2ms(LCD_DELAY_WYPISYWANIE_ZNAKU);  
  	} 
  	LCD_WriteStringRC(1, 0, " on 32-bit RISC");  	
		LCD_DelayN2ms(5*LCD_DELAY_WYPISYWANIE_ZNAKU);*/
}

//----------------------------------------------------------------------------
void LCD_SendNibble(unsigned int data_lcd, unsigned int cmd, char ktory )
{
  GPIO_ClearOutput( LCD_E_PIN );  
	if( cmd == LCD_CMD ) GPIO_ClearOutput( LCD_RS_PIN ); 
	else							 	 GPIO_SetOutput  ( LCD_RS_PIN ); 

	if(ktory=='H'){				
		data_lcd = data_lcd >> 4;
	}
	GPIO_ClearOutput( LCD_DATA_D4_PIN|LCD_DATA_D5_PIN|LCD_DATA_D6_PIN|LCD_DATA_D7_PIN );  
	if( data_lcd & 0x01 )	GPIO_SetOutput( LCD_DATA_D4_PIN );
	if( data_lcd & 0x02 )	GPIO_SetOutput( LCD_DATA_D5_PIN );
	if( data_lcd & 0x04 )	GPIO_SetOutput( LCD_DATA_D6_PIN );
	if( data_lcd & 0x08 )	GPIO_SetOutput( LCD_DATA_D7_PIN );

  GPIO_SetOutput(LCD_E_PIN);
	LCD_Delay1us();
  GPIO_ClearOutput(LCD_E_PIN);
}


//----------------------------------------------------------------------------
void LCD_ClrScr(void)
{
  LCD_WriteCmd( LCD_CLRSCR_R );
	LCD_Delay2ms(); 
}
//----------------------------------------------------------------------------
void LCD_Home(void)
{
  LCD_WriteCmd( LCD_HOME_R );
	LCD_Delay2ms(); 
}
//----------------------------------------------------------------------------
void LCD_Delay1us( void )
{
 int iter;
 volatile unsigned int i;
	if( RunTimeMemoryType == JTAG_RAM_MEMORY_TYPE )	iter=17;  // zalezy od opcji kompilatora
	else iter=9;		// zalezy od opcji kompilatora
	
	for(i=0;i<iter;i++){};	 
}
//----------------------------------------------------------------------------
void LCD_Delay40us(void)
{ 
  unsigned int i;		
  for(i = 0; i<40; i++) LCD_Delay1us();	 
}	
//----------------------------------------------------------------------------	
void LCD_Delay2ms(void) 
{ 
  unsigned int i;		
  for(i = 0; i<50; i++) LCD_Delay40us();	 
}		
//----------------------------------------------------------------------------
void LCD_DelayN2ms( unsigned int ile )
{
 int i;
 	for(i=0;i<ile;i++) LCD_Delay2ms();
}
//---------------------------------------------------------------------------
void LCD_WriteInt(int value, int lm, int null_zero)
{
	int dzielnik = 1;
	int i = 0;
	int lm1 = lm;
	char buf[10];
	int flaga = 0;
	
	while((lm1--)-1)
	{
		dzielnik = dzielnik * 10;
	}
	
	if( value < 0)
	{
		
		flaga = 1;
		value = -value;
	}

	while(dzielnik >=1)
	{
		if((int) value/dzielnik == 0)
		{	
				if(null_zero == 0)
					buf[i++] = '0';
				else
					buf[i++] = ' ';
		}
		else
		{
				buf[i++] = (char)((value/dzielnik) + 48);

				if (flaga == 1)
				{
					buf[i-2] = '-';
					flaga = 0;
				}
				null_zero = 0;
		}
		
		value = value % dzielnik;
		dzielnik = dzielnik/10;
	}	
	
	if(buf[i-1] == ' ')
		buf[i-1] = '0';

	buf[i] = 0;

	LCD_WriteString(buf);
}
