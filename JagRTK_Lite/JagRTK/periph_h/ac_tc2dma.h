/********************************************************************
  File: ac_tc2dma.h
  Desc: Sterownik (driver) ADTC2DMA przetwornika ADC 
	      Przetwornik wyzwalany z timera TC2, sterownik uzywa DMA oraz 
				przerwania (ENDRX).
				Wynik przetwarzania kierowany jest do sterownika timera PWM.

				Przedstawiony kod opisany jest w rozdziale 15.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*********************************************************************/
#ifndef _AC_TC2DMA_H_INCLUDED_
#define _AC_TC2DMA_H_INCLUDED_
#include "../periph_h/ac_def.h"

//-----------------------------------------------------------------------------------
void ADTC2DMA_Open( int adclock_hz, int chans_mask );
int  ADTC2DMA_IsCompleted( void );
void ADTC2DMA_ClearCounter( void );
int  ADTC2DMA_Mask2Num( int mask );
unsigned int ADTC2DMA_Calc_ModeR( int adclock_hz );

typedef struct _ADTC2DMA_state{
	int 			NrOpened;				
	volatile int Counter;	
	short int Buf[8];		
} _ADTC2DMA_state;
extern struct _ADTC2DMA_state ADTC2DMA_state;

#endif // _AC_TC2DMA_H_INCLUDED_
