/********************************************************************
  File: timer_rtt.c
  Desc: Driver (sterownik) timera RTT

				Przedstawiony kod opisany jest w rozdziale 13.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*********************************************************************/
#include "proj_config.h"
#include "../periph_hw_h/pio.h"
#include "../periph_hw_h/pmc.h"
#include "../periph_hw_h/aic.h"
#include "../periph_hw_h/led_sw.h"
#include "../periph_hw_h/board_pins.h"
#include "../periph_hw_h/dbgu_debug.h"
#include "../periph_hw_h/sys_intrpt_handler.h"
#include "../periph_hw_h/rtt.h"
//-----------------------------------------------------------------------------

#define LEDWIZ  0  //1-wizualizacja modulu, 0-brak wizualizacji

static struct{
	int Configured_Time_ms;
	int Prescaler;
} RTT_state;
//----------------------------------------------------------------------------------
void RTT_irq_kernel( int status_r )
{
	if( status_r & RTT_ALARM_STATUS ){			
		RTT->MODE_R &= ~((unsigned int)RTT_ALARM_INT_ENABLE);
		RTT->ALARM_R = 0xFFFFFFFF;						

		//...																	// kod uzytkowy stosownie do potrzeb aplikacji
		DBGU_Trace(" RTT_INTERRUPT - ALARM ", NULL );
		LEDWIZ_ToggleOne( PIO_PA1, LEDWIZ );	// wizualizacja
	}
}
//----------------------------------------------------------------------------------
void RTT_SetAlarm( unsigned int sek )
{
 int t=0;
	if(1){
		t=RTT_ReadTime();
		RTT->ALARM_R = t + ((1000*sek)/RTT_state.Configured_Time_ms); 
		RTT->MODE_R = RTT_state.Prescaler | RTT_ALARM_INT_ENABLE;  
	}else{ 
		DBGU_Trace("wpis=0x%X ", RTT_state.Prescaler  | RTT_ALARM_INT_ENABLE | RTT_RESTART );
		RTT->ALARM_R = (1000*sek)/RTT_state.Configured_Time_ms; //
		RTT->MODE_R = RTT_state.Prescaler  | RTT_ALARM_INT_ENABLE | RTT_RESTART;  // uruchom alarm
	}	
}
//----------------------------------------------------------------------------------
void RTT_Open( unsigned int time_ms )
{	
 int val;
		RTT->MODE_R = (RTT_INCREMENT_INT_DISABLE | RTT_ALARM_INT_DISABLE); 
		while( (RTT->MODE_R & (RTT_INCREMENT_INT_DISABLE | RTT_ALARM_INT_DISABLE)) ){};		

		val = RTT->STATUS_R;	
		while( (RTT->MODE_R & (RTT_ALARM_STATUS | RTT_INCREMENT_STATUS)) ){};		
		RTT->ALARM_R = 0xFFFFFFFF;		

		AIC_Configure_IRQ( SYS_PERIPH_ID, (AIC_LEVEL_TRIGGERED | AIC_PRIORITY_LOWEST), 
										 	 SYS_INTRPT_irq_handler );	
		RTT_state.Configured_Time_ms = time_ms;			
		RTT_state.Prescaler = ((time_ms*0x8000) /1000); 	

		RTT->MODE_R = RTT_state.Prescaler | RTT_RESTART;		
		DBGU_Trace("Starting RTT timer:\n\r Configured_Time_ms=%i[ms].\n\r", time_ms);
}

//----------------------------------------------------------------------------------
unsigned int RTT_ReadTime( void )
{
 unsigned int val;
	do{
		val = RTT->VALUE_R;
	}while( val != RTT->VALUE_R );
	return( val );
}


