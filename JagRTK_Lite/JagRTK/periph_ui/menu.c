/***********************************************************************
  File: menu.c
  Desc: Sterownik (driver) MNU wielopoziomowego, hierarchicznego, nieblokujacego
				menu. 
				Wykorzystuje m.in. sterownik LCD

				Przedstawiony kod opisany jest w rozdziale 8.

				Szczegolowy opis programu, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************************************************/
#include "../periph_hw_h/pmc.h"
#include "../periph_hw_h/dbgu_debug.h"
#include "../periph_hw_h/led_sw.h"
#include "../periph_hw_h/delay.h"
#include "../periph_ui_h/kb_keycodes_def.h"
#include "../periph_ui_h/hd44780.h"
#include "../periph_ui_h/menu.h"

//--------------------------------------------------------------
#define MNU_NOREDR  0
#define MNU_REDR  	1

static struct{	
	menu 				*pmnu;
	signed char  poz;					
	signed char	 nest;
	menu 				*stack_pmnu[MNU_MAX_NEST];
	char 			 	 stack_poz[MNU_MAX_NEST];	
	int (*pfc)(int znak, int first_flag);		
} MNU_state;								// state

//-------------------------------------------------------- 
void MNU_Open( menu *mnu_start )
{
  MNU_state.pmnu = mnu_start;			
  MNU_state.poz = 0;							
	MNU_state.nest = 0;							
	MNU_state.pfc=NULL;				  		
	MNU_Disp( 0, MNU_REDRAW );
	DBGU_Trace("MNU1 on HD44780 uruchomiono.\n\r", NULL);
}

//-------------------------------------------------------- 
int MNU_Disp( signed char nr_poz, int redraw )
{
 int j, nr;
 char buf[4];
 	nr = (signed)nr_poz;																
  if( nr < 0 ){ nr=0; }
	if( nr != MNU_state.poz || redraw ){  		
		if( nr >= MNU_state.pmnu->ilosc ){			
			nr = (MNU_state.pmnu->ilosc - 1);	
		}
		MNU_state.poz = nr;											
		
		LCD_GotoRC( 0, 0 );
		sprintf(buf,"%i>", MNU_state.nest );
		LCD_WriteString( buf );
		MNU_WriteLine( MNU_state.pmnu->ppozycje[nr]->napis, MNU_COLUMNS-2 );
		for(j=1;j<MNU_LINES;j++){								
			LCD_GotoRC( j, 0 );
			if( nr+j < MNU_state.pmnu->ilosc ){ 	
				LCD_WriteString( "  " );
				MNU_WriteLine( MNU_state.pmnu->ppozycje[nr+j]->napis, MNU_COLUMNS-2 );
			}else{
				MNU_WriteLine("",MNU_COLUMNS);			
			}
		}
	}
	LCD_GotoRC( 0, 0 );												
	return(nr);																
}

//--------------------------------------------------------------------------------------
void MNU_Execute( int key )
{
 int ret;
 void (*pfj)(void);					
 signed char poz;

	if(MNU_state.pfc!=NULL){	
		ret = (*MNU_state.pfc)(key, MNU_NOT_FIRST_FC_CALL);	
		if( ret != 0 ){	
			MNU_state.pfc=NULL;		
			MNU_Disp( MNU_state.poz, MNU_REDRAW );	
		}
		return; 								
	}

 	poz = MNU_state.poz;					
	switch( key ){
	case KEY_DOWN:
		poz++; MNU_Disp( poz, MNU_REDRAW_IF_CHANGE );
		break;
	case KEY_UP:
		poz--; MNU_Disp( poz, MNU_REDRAW_IF_CHANGE );
		break;
	case KEY_ENTER:
		switch( MNU_state.pmnu->ppozycje[poz]->typ ){
		case PODMENU:		
			if( MNU_state.nest < MNU_MAX_NEST ){
				if( MNU_state.pmnu->ppozycje[poz]->adr_podmenu_lub_funkcji != NULL ){ 
			  	MNU_state.stack_pmnu[MNU_state.nest] = MNU_state.pmnu; 	
  				MNU_state.stack_poz[MNU_state.nest] = MNU_state.poz;	
					MNU_state.nest++;														
					MNU_state.pmnu = MNU_state.pmnu->ppozycje[MNU_state.poz]->adr_podmenu_lub_funkcji;	
					MNU_state.poz = 0;										
					MNU_Disp( 0, MNU_REDRAW );						
			 	}else{ LCD_GotoRC(1,0); 	LCD_WriteString("mnu(): prtNULL");	}
			}else{ LCD_GotoRC(1,0);	LCD_WriteString("MAX zagniezdz.");	}
			break;
		case FUNKCJA_JEDNORAZOWA:
			if( MNU_state.pmnu->ppozycje[poz]->adr_podmenu_lub_funkcji != NULL ){ 
					pfj = MNU_state.pmnu->ppozycje[poz]->adr_podmenu_lub_funkcji;
					(*pfj)();							
			}else{ LCD_GotoRC(1,0); 	LCD_WriteString("mnu(): prtNULL");	}
			break;
		case FUNKCJA_OKRESOWA:
			if( MNU_state.pmnu->ppozycje[poz]->adr_podmenu_lub_funkcji != NULL ){  
					MNU_state.pfc = MNU_state.pmnu->ppozycje[poz]->adr_podmenu_lub_funkcji;	
					(*MNU_state.pfc)( key, MNU_FIRST_FC_CALL );		
			}else{ LCD_GotoRC(1,0); 	LCD_WriteString("mnu(): prtNULL");	}
			break;
		default:
			LCD_GotoRC(1,0); 	LCD_WriteString("nieopr. typ pozycji");
			break;
		}
		break;
	case KEY_ESC:
			if( MNU_state.nest > 0 ){										
				MNU_state.nest--;
			 	MNU_state.pmnu = MNU_state.stack_pmnu[MNU_state.nest]; 	
  			MNU_state.poz = MNU_state.stack_poz[MNU_state.nest];	
			}		
			MNU_Disp( MNU_state.poz, MNU_REDRAW );	
		break;
	case KEY_NO_PRESSED:	
		break;
	default:
		LCD_GotoRC(1,0); 	LCD_WriteString("nieopr. kod klawisza");
		break;
	}
}
//---------------------------------------------------------------------------
void MNU_WriteLine( char *p, int max )
{
 char buf[MNU_COLUMNS+1];	
 int i;
 	for(i=0;i<max;i++){	
		if( *p==0x0 ) break;
		buf[i]=*p;
		p++;
	}
	for( ;i<max;i++){	buf[i]=' ';	}
	buf[i]=0x0;
	LCD_WriteString(buf);
}

