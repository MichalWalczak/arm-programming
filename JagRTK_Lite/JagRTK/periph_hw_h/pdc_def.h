/********************************************************************
  File: pdc_def.h   (notacja JagNOT)
  Desc: pdc 

				Przedstawiony kod opisany jest w rozdziale 10.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*********************************************************************/
#ifndef _PDC_DEF_H_INCLUDED_
#define _PDC_DEF_H_INCLUDED_
#include "../periph_hw_h/reg_typedef.h"


//-------------------------------------------------------------------------
typedef struct _S_PDC{		// dostep, wartosc po resecie, adres wzgledny, nazwa [AT]
	REG	 RX_PTR_R; 					// R/W, 		0x0, 						+0x00,			RPR 	 
	REG	 RX_CNT_R; 					// R/W, 		0x0, 						+0x04, 			RCR 											
	REG	 TX_PTR_R; 					// R/W, 		0x0, 						+0x08, 			TPR 											
	REG	 TX_CNT_R; 					// R/W, 		0x0, 						+0x0C, 			TCR 										
	REG	 RX_NEXT_PTR_R; 		// R/W, 		0x0, 						+0x10, 			RNPR	 
	REG	 RX_NEXT_CNT_R; 		// R/W, 		0x0, 						+0x14, 			RNCR 										
	REG	 TX_NEXT_PTR_R; 		// R/W, 		0x0, 						+0x18, 			TNPR 										
	REG	 TX_NEXT_CNT_R; 		// R/W, 		0x0, 						+0x1C, 			TNCR 										
	REG	 CONTROL_R; 				// WO, 				-, 						+0x20, 			PTCR							
	REG	 STATUS_R; 					// RO, 			0x0, 						+0x24, 			PTSR 						
}	S_PDC, *PS_PDC;

// ---- CONTROL_R ------------------------------
#define PDC_RX_ENABLE  	BIT_0
#define PDC_RX_DISABLE 	BIT_1
#define PDC_TX_ENABLE  	BIT_8
#define PDC_TX_DISABLE 	BIT_9
// ---- STATUS_R ------------------------------
#define PDC_RX_ENABLED  	BIT_0
#define PDC_TX_ENABLED  	BIT_8


#endif // _PDC_DEF_H_INCLUDED_
