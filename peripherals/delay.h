#ifndef DELAY_H
#define DELAY_H

#include "library\AT91SAM7S64.h"
#include "library\lib_AT91SAM7S64.h"

#define TYP_PROJEKTU 	3
#define JTAG_RAM		1
#define SAMBA_FLASH		3

void delay_us(unsigned int delay)
{
	volatile unsigned int i;
	#if (TYP_PROJEKTU == JTAG_RAM)
			for(i = 0; i < 16 * delay; i++);
	#elif (TYP_PROJEKTU == SAMBA_FLASH)
			for(i = 0; i< 11 * delay; i++);
	#endif
}

//----------------------------------------

void delay_ms(unsigned int delay)
{
	volatile unsigned int i;
	
	for(i = 0; i < delay; i++) 
		delay_us(1000);	
}

//----------------------------------------

void delay_s(unsigned int delay)
{
	volatile unsigned int i;
	
	for(i = 0; i < delay; i++) 
		delay_ms(1000);	
}

#endif //delay_h
