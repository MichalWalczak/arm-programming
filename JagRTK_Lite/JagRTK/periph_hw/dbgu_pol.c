/********************************************************************
  File: dbgu.c
  Desc: Driver (sterownik) portu DBGU obslugiwanego 
        w trybie odpytywania (pooling)

				Przedstawiony kod opisany jest w rozdziale 11.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*********************************************************************/
#include <stdio.h>
#include "proj_config.h"
#include "../periph_hw_h/bit_names.h"
#include "../periph_hw_h/pio.h"
#include "../periph_hw_h/board_pins.h"
#include "../periph_hw_h/pmc.h"
#include "../periph_hw_h/aic.h"
#include "../periph_hw_h/pdc.h"
#include "../periph_hw_h/dbgu_pooling.h"

//-------------------------------------------------------------------------------
void DBGUPOL_WriteChar( char ch )
{
	while( !(DBGU->INT_STATUS_R & DBGU_TX_RDY ) ){ };
	DBGU->TRANSMIT_HOLDING_R = ch;
}
//----------------------------------------------------------------------------------
void DBGUPOL_WriteString( char *p )
{
		while( *p ){
			DBGUPOL_WriteChar( *p );
		 	p++;
		}
}
//-------------------------------------------------------------------------------
char DBGUPOL_ReadChar( void )
{
	while( !(DBGU->INT_STATUS_R & DBGU_RX_RDY ) ){ }; 	
	return( (char)DBGU->RECEIVER_HOLDING_R );
}
//-------------------------------------------------------------------------------
int DBGUPOL_IsCharAvailable( void )
{
	return( (DBGU->INT_STATUS_R & DBGU_RX_RDY) );
}
//---------------------------------------------------------------------------
void DBGUPOL_Open( unsigned int baudrate )
{
	PMC_OpenPeriphClock( SYS_PERIPH_ID_MASK );
	DBGU->INT_DISABLE_R  = 0xFFFFFFFF;		
	PDC_DisableAndResetWrite( PDC_DBGU );
	PDC_DisableAndResetRead( PDC_DBGU );
	DBGU->CONTROL_R = DBGU_RESET_RX | DBGU_RX_DISABLE
									| DBGU_RESET_TX | DBGU_TX_DISABLE;
  DBGU_ReConfigure( baudrate, (DBGU_PARITY_NONE|DBGU_MODE_NORMAL)); 	
	DBGU->CONTROL_R = DBGU_RX_ENABLE | DBGU_TX_ENABLE;  
	#if (PROCESSOR_TYPE == PROCESSOR_SAM7S)
		PIO_OpenPeriph_A( DRXD_A9 | DTXD_A10 ); 
	#elif (PROCESSOR_TYPE == PROCESSOR_SAM7X)
		PIO_OpenPeriph_A( DRXD_AA27 | DTXD_AA28 ); 
	#else
		#error "nieznany typ procesora"
	#endif
}
//-------------------------------------------------------------------------------
void DBGU_ReConfigure( unsigned int baudrate, unsigned int mode_and_parity )
{	
	unsigned int baud_value = ((fMCK_Hz*10)/(baudrate * 16));
	if ((baud_value % 10) >= 5)  baud_value = (baud_value/10)+1;
	else	baud_value /= 10;
	DBGU->BAUD_RATE_GEN_R = baud_value;	
	DBGU->MODE_R = mode_and_parity;	
}
