/********************************************************************
  File: ac_tc2dma.c
  Desc: Sterownik (driver) ADTC2DMA przetwornika ADC 
	      Przetwornik wyzwalany z timera TC2, sterownik uzywa DMA oraz 
				przerwania (ENDRX).
				Wynik przetwarzania kierowany jest do sterownika timera PWM.

				Przedstawiony kod opisany jest w rozdziale 15.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
	 			0.1G - GNU / 2007.08.31
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*********************************************************************/
#include <stdio.h>
#include "proj_config.h"
#include "../periph_hw_h/pio.h"
#include "../periph_hw_h/pmc.h"
#include "../periph_hw_h/aic.h"
#include "../periph_hw_h/pdc.h"
#include "../periph_hw_h/delay.h"
#include "../periph_hw_h/led_sw.h"
#include "../periph_hw_h/dbgu_debug.h"
#include "../periph_h/ac_tc2dma.h"
#include "../periph_h/pwm.h"

//--------------------------------------------------------------------------------
#define LEDWIZ  1  //1-wizualizacja modulu, 0-bez wizualizacji

void ADTC2DMA_irq_handler( void ) DECLARE_AS_IRQ_HANDLER;

//--------------------------------------------------------------------------------
struct _ADTC2DMA_state ADTC2DMA_state;

//-----------------------------------------------------------------------------------
void ADTC2DMA_irq_handler( void ) DEFINE_AS_IRQ_HANDLER
{
 int status_r;
 int tmp;
 INT_NEST_ENABLE;
  status_r = ADC->INT_STATUS_R;					
	if( status_r & ADC_ENDRX ){
		ADTC2DMA_state.Counter++;
		PDC_SetRx( (char*)ADTC2DMA_state.Buf, ADTC2DMA_state.NrOpened, PDC_ADC );  

		//Przetwarzaj()											// kod uzytkowy stosownie do potrzeb aplikacji
		tmp = ADTC2DMA_state.Buf[1] >> 2;		
		PWM_Set(1, tmp );										

		//DBGU_Trace("AD[ch4]=%i ", ADDMA_state.Buf[0] );
		//DBGU_Trace("AD[ch5]=%i \n\r", ADDMA_state.Buf[1] );
	}

 INT_NEST_DISABLE;
 AIC_EOI;
}
//-----------------------------------------------------------------------------------
void ADTC2DMA_Open( int adclock_hz, int chan_mask )
{
 	DBGU_Trace("Starting ADConverter ADTC2DMA:\n\r", NULL );
	PMC_OpenPeriphClock( ADC_PERIPH_ID_MASK );
	ADC->INT_DISABLE_R = 0xFFFFFFFF;				
	ADC->CONTROL_R = ADC_SOFT_RESET;				
	
	ADTC2DMA_state.NrOpened = ADTC2DMA_Mask2Num(chan_mask);	
	PDC_DisableRead( PDC_ADC );	
	PDC_SetRx( (char*)ADTC2DMA_state.Buf, ADTC2DMA_state.NrOpened, PDC_ADC );
	PDC_EnableRead( PDC_ADC );
	
	AIC_Configure_IRQ( ADC_PERIPH_ID, (AIC_LEVEL_TRIGGERED | AIC_PRIORITY_4), 
										 ADTC2DMA_irq_handler ); 	
	ADC->CHANNEL_ENABLE_R = chan_mask;	

	ADTC2DMA_state.Counter = 0;
	ADC->MODE_R = ADC_HW_TRG_ENABLE
							| ADC_TRG_TIOA2
							| ADC_RESOLUTION_10BIT
							| ADC_POWER_SLEEP_MODE
							| ADTC2DMA_Calc_ModeR( adclock_hz );
	ADC->INT_ENABLE_R = ADC_ENDRX;
	DBGU_Trace("ADTC2DMA Opened. \n\r", NULL );
}
//-----------------------------------------------------------------------------------
int ADTC2DMA_IsCompleted( void )
{	
	return( ADTC2DMA_state.Counter );
}
//-----------------------------------------------------------------------------------
void ADTC2DMA_ClearCounter( void )
{	
	ADTC2DMA_state.Counter = 0;
}
//---------------------------------------------------------------------
int ADTC2DMA_Mask2Num( int mask )
{	
	int i;
	int licznik=0;
	for( i=0; i<8; i++){
		if ( mask & 0x1 )  licznik++;
		mask >>=1;
	}
	return( licznik );
}

//-----------------------------------------------------------------------------------
unsigned int ADTC2DMA_Calc_ModeR( int adclock_hz )
{
 int prescaler, adclock_recp; 
 int startup_cfg, startup_time_recp; 
 int sh_req, sh_inv, sh_cfg, sh_time_recp;
	DBGU_Trace(" fMCK_Hz: %i[Hz].\n\r", fMCK_Hz );

	prescaler = (fMCK_Hz/(2*adclock_hz))-1;		
	if(prescaler>63) prescaler=63;						
	adclock_recp = fMCK_Hz/(2*(prescaler+1));	
	DBGU_Trace(" ADCLK Reqiured: %i[Hz].", adclock_hz );
	DBGU_Trace(" ADCLK Configured: %i[Hz].", adclock_recp );
	DBGU_Trace(" prescaler=%i.\n\r", prescaler );

	startup_cfg	= 10*adclock_recp / (8*50000);								
	if(startup_cfg%10>=1) startup_cfg=((startup_cfg/10)+1)-1;	
	else									startup_cfg=(startup_cfg/10)-1;
	startup_time_recp = (1000000*(8*(startup_cfg+1)))/adclock_recp; 
	DBGU_Trace(" StartupTime Reqiured: %i[us].", 20 );
	DBGU_Trace(" StartupTime Configured: %i[us].", startup_time_recp );
	DBGU_Trace(" startup_cfg=%i.\n\r", startup_cfg );

	sh_req = 10;
	sh_inv = ((10*1000000/sh_req))/10;
	sh_cfg = (100*adclock_recp/sh_inv);						
	if(sh_cfg%100>=1) sh_cfg=(sh_cfg/100+1)-1;		
	else 							sh_cfg=(sh_cfg/100)-1;
	sh_time_recp = (10000000*(sh_cfg+1))/adclock_recp;
	DBGU_Trace(" S&HTime Reqiured: %i[us].", sh_req );
	DBGU_Trace(" S&HTime Configured: %i/10[us].", sh_time_recp );
	DBGU_Trace(" S&H_cfg=%i.\n\r", sh_cfg );

	return( prescaler << ADC_PRESCALER_bsf
				|	startup_cfg << ADC_STARTUP_TIME_CFG_bsf
				| sh_cfg << ADC_SH_TIME_CFG_bsf
				);
}

