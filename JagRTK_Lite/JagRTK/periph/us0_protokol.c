/********************************************************************
  File: us_protokol.c
  Desc: Sterownik (driver) U0PKT portu USART
        Uzywa DMA oraz przerwan, przeznaczony jest do komunikacji 
				pakietowej.

				Przedstawiony kod opisany jest w rozdziale 16.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
	 			0.1G - GNU / 2007.08.31
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*********************************************************************/
#include <stdio.h>
#include "../periph_hw_h/bit_names.h"
#include "../periph_hw_h/reg_typedef.h"
#include "../periph_hw_h/pio.h"
#include "../periph_hw_h/board_pins.h"
#include "../periph_hw_h/pmc.h"
#include "../periph_hw_h/aic.h"
#include "../periph_hw_h/led_sw.h"
#include "../periph_hw_h/dbgu_debug.h"
#include "../periph_hw_h/pdc.h"

#include "../periph_h/us_def.h"
#include "../periph_h/us0_protokol.h"
#include <string.h>

//-------------------------------------------------------------------------------
#define LEDWIZ		1		// 1-wizualizacja modulu, 0-brak wizualizacji


char odpowiedz[5];
//--------------------------------------------------------------------------
void U0PKT_Execute( char *p )
{
 S_PKT *pkt;
 unsigned char ktora, stan;
	pkt = (S_PKT*)p;
	if( pkt->Len != 0 ){					
		DBGU_Trace("\n\rOdebrano ramke #%i", pkt->UserData[0] );
	}

	if( pkt->UserData[0] == ROZKAZ_TEST ){
		strcpy( odpowiedz, "ECHO" );
		U0PKT_AsyncWrite( odpowiedz, 4 );
	}
	else if( pkt->UserData[0] == ROZKAZ_ZAPAL_LED ){
	 	LED_Set( PIO_PA1 );
		odpowiedz[0] = POTWIERDZENIE_ZAPAL_LED;
		U0PKT_AsyncWrite( odpowiedz, 1 );
	}
	else if( pkt->UserData[0] == ROZKAZ_ZGAS_LED ){
	 	LED_Clear( PIO_PA1 );
		odpowiedz[0] = POTWIERDZENIE_ZGAS_LED;
		U0PKT_AsyncWrite( odpowiedz, 1 );
	}
	else if( pkt->UserData[0] == ROZKAZ_USTAW_STAN_NTEJ ){
		ktora = pkt->UserData[1];
		stan  = pkt->UserData[2];
		LED_SetState( 0x01 << ktora, stan );
		odpowiedz[0] = POTWIERDZENIE_USTAW_STAN_NTEJ;
		U0PKT_AsyncWrite( odpowiedz, 1 );
	}
	else if( pkt->UserData[0] == ROZKAZ_POBIERZ_STAN_NTEJ ){
		ktora = pkt->UserData[1];
		stan = LED_GetState( 0x01 << ktora );
		stan = stan >> ktora;
		DBGU_Trace(" stan=%i ", stan );
		odpowiedz[0] = POTWIERDZENIE_POBIERZ_STAN_NTEJ;
		odpowiedz[1] = stan;
		U0PKT_AsyncWrite( odpowiedz, 2 );
	}
	else{
		odpowiedz[0] = NIEZNANY_ROZKAZ;
		DBGU_Trace(" rozkaz_nieznany ", NULL );
		U0PKT_AsyncWrite( odpowiedz, 1 );
	}
	if( pkt->Len == 0x00 ){
		DBGU_Trace(" rozkaz_pusty ", NULL );
		U0PKT_AsyncWrite( odpowiedz, 0 );		
	}
}

//-------------------------------------------------------------------------------


S_U0PKT_state 	U0PKT_state;		

//--------------------------------------------------------------------------
void U0PKT_AsyncWrite( char *p, unsigned char len )
{
	U0PKT_FlushWrite();
	PDC_DisableWrite( PDC_US0 );	
	 U0PKT_state.TxCompleted = 0;	
	 U0PKT_state.TxHdr[1] = len;									
	 PDC_SetTx( (char*)U0PKT_state.TxHdr, 2, PDC_US0 );	
	 PDC_SetNextTx( p, len, PDC_US0 );						
	PDC_EnableWrite( PDC_US0 );
	US0->INT_ENABLE_R = US_TXBUFS_EMPTY;	
}

//--------------------------------------------------------------------------
void U0PKT_AsyncRead( char *p, unsigned char len_max )	
{
	U0PKT_FlushRead();
	 U0PKT_state.RxBuf = p;										 		
	 U0PKT_state.RxUDLen = len_max-2;							
	 U0PKT_state.RxCompleted = 0;
	 PDC_SetRx( p, 2, PDC_US0 );		 			 				
	 PDC_SetNextRx( p+2, len_max-2, PDC_US0 );		
	PDC_EnableRead( PDC_US0 );
	
	US0->CONTROL_R = US_START_TIMEOUT;		
	US0->INT_ENABLE_R = US_ENDRX | US_RXBUFS_FULL | US_RX_TIMEOUT;
}


//------------------------------------------------------------------------------
void U0PKT_irq_handler( void ) DEFINE_AS_IRQ_HANDLER
{
 int status_r;
 int status_and_mask;
 int pktlen;
 int ilejuz;
	status_r = US0->INT_STATUS_R;		
  status_and_mask = status_r & US0->INT_MASK_R;	
	DBGU_Trace("\n\rINT:status_end_mask=0x%X ", status_and_mask );		

	if( US_ENDRX == status_and_mask ){	 
		US0->INT_DISABLE_R = US_ENDRX;		
		DBGU_Trace("ENDRX: ", NULL);		

		if( U0PKT_state.RxBuf[0] != 'J' ){						
			DBGU_Trace("zly naglowek -reinicjalizacja obu buforow ", NULL);		
			U0PKT_ReinitRx();
		}else{			
			pktlen= (int)(*(U0PKT_state.RxBuf+1));
			DBGU_Trace("pole_ilosc=%i ", pktlen);		
			if( pktlen == 0 ){ 
				PDC_DisableRead( PDC_US0 );			
				PDC_US0->RX_CNT_R = 0;				  
				U0PKT_state.RxCompleted = 1;
				US0->INT_DISABLE_R = US_RXBUFS_FULL | US_ENDRX | US_RX_TIMEOUT;	
				DBGU_Trace("ilosc zero ", NULL);
			}else{	
				PDC_DisableRead( PDC_US0 );		
				ilejuz = (U0PKT_state.RxUDLen-PDC_US0->RX_CNT_R);	
				DBGU_Trace("ilejuz=%i ", ilejuz);	
				if( pktlen == ilejuz ){ 
					US0->INT_DISABLE_R = US_RXBUFS_FULL | US_ENDRX | US_RX_TIMEOUT;	
					U0PKT_state.RxCompleted = 1;
				}else{  
					PDC_US0->RX_CNT_R = pktlen-ilejuz; 	
					PDC_EnableRead( PDC_US0 );		 			
					DBGU_Trace("jeszcze do odebrania=%i ", PDC_US0->RX_CNT_R);	
				}
			} 
		}	
	}
	if( US_RXBUFS_FULL & status_and_mask ){						
		US0->INT_DISABLE_R = US_RXBUFS_FULL | US_ENDRX | US_RX_TIMEOUT;	
		U0PKT_state.RxCompleted = 1;
		DBGU_Trace("RXBUFS_FULL: ", NULL);		
 	}
 	if( status_and_mask & US_RX_TIMEOUT ){
	 	DBGU_Trace("RXTIMEOUT: RX_CNT_R=%i - reinicjalizacja obu buforow ",PDC_US0->RX_CNT_R);
		US0->INT_DISABLE_R = US_RX_TIMEOUT;
		U0PKT_state.RxTimeout = 1;
		U0PKT_ReinitRx();
	}

	if( US_TXBUFS_EMPTY & status_and_mask ){
		US0->INT_DISABLE_R = US_TXBUFS_EMPTY | US_ENDTX;	
		DBGU_Trace("TXBUFS_EMPTY: ", NULL ); 
		U0PKT_state.TxCompleted = 1;
 	}

	AIC->EOI_R=0;	
}
//--------------------------------------------------------------------------
void U0PKT_ReinitRx( void )
{
	PDC_DisableRead( PDC_US0 );
	 PDC_SetRx( U0PKT_state.RxBuf, 2, PDC_US0 );		 			 				
	 PDC_SetNextRx( U0PKT_state.RxBuf+2, U0PKT_state.RxUDLen, PDC_US0 );		
	PDC_EnableRead( PDC_US0 );
		
	US0->CONTROL_R = US_START_TIMEOUT;		
	US0->INT_ENABLE_R = US_ENDRX | US_RXBUFS_FULL | US_RX_TIMEOUT;
}

//--------------------------------------------------------------------------
void U0PKT_Open(unsigned long baudrate, unsigned short timeout_in_bits )
{
	PMC_OpenPeriphClock( US0_PERIPH_ID_MASK );
	US0->INT_DISABLE_R = 0xFFFFFFFF;
  US0->CONTROL_R = US_RESET_RECEIVER | US_RESET_TRANSMITER
								 | US_RX_DISABLE | US_TX_DISABLE;
	PDC_DisableAndResetWrite( PDC_US0 );
	PDC_DisableAndResetRead( PDC_US0 );
	
	US0->MODE_R = US_CLOCK_MCK 
							| US_MODE_NORMAL | US_ASYNC_MODE | US_OVERSAMPLING16
							| US_8_BITS | US_PARITY_NONE | US_STOP_1_BIT | US_LSB_FIRST; 
  US0->BAUD_RATE_R = US_CalcPrescaler( baudrate );
  US0->TRANSMITER_TIMEGUARD_R = 0;
	US0->RECEIVER_TIMEOUT_R = timeout_in_bits;
	
	U0PKT_state.TxHdr[0]='J';				
	U0PKT_state.RxCompleted=1;				
	U0PKT_state.RxTimeout=0;				
	U0PKT_state.TxCompleted=1;				

	AIC_Configure_IRQ( US0_PERIPH_ID, (AIC_LEVEL_TRIGGERED | AIC_PRIORITY_3),
										 U0PKT_irq_handler );

  US0->CONTROL_R = US_TX_ENABLE | US_RX_ENABLE;
	PDC_EnableWrite( PDC_US0 );

	#if (PROCESSOR_TYPE == PROCESSOR_SAM7S)
		PIO_OpenPeriph_A( RXD0_A5|TXD0_A6 );  
	#elif (PROCESSOR_TYPE == PROCESSOR_SAM7X)	
		PIO_OpenPeriph_A( RXD0_AA0|TXD0_AA0 ); 
	#else
		#error "Nieznany typ procesora"
	#endif

	DBGU_Trace("Port USART0 (U0PKT) Opened, %i[bd].\n\r", (int)baudrate );
}



//--------------------------------------------------------------------------
int U0PKT_IsWriteCompleted( void )
{
	if( PDC_US0->TX_CNT_R || PDC_US0->TX_NEXT_CNT_R ) return(0); 
	else return(1);		
}
//--------------------------------------------------------------------------
void U0PKT_FlushWrite( void )
{
 	while( !U0PKT_IsWriteCompleted() ){ };
}


//--------------------------------------------------------------------------
int U0PKT_IsReadCompleted( void )
{
	if( U0PKT_state.RxCompleted )	return(1);
	else return(0);
}
//--------------------------------------------------------------------------
void U0PKT_FlushRead( void )
{
 	while( !U0PKT_IsReadCompleted() ){ };
}



