/********************************************************************
  File: us0_protokol.h
  Desc: Sterownik (driver) U0PKT portu USART
        Uzywa DMA oraz przerwan, przeznaczony jest do komunikacji 
				pakietowej.

				Przedstawiony kod opisany jest w rozdziale 16.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*********************************************************************/
#ifndef _US0_PROTOKOL_H_INCLUDED_
#define _US0_PROTOKOL_H_INCLUDED_

#include "../periph_h/us_def.h"

//--------------------------------------------------------------------
#define ROZKAZ_TEST						 			1
#define ROZKAZ_ZAPAL_LED						2 
#define ROZKAZ_ZGAS_LED 						3
#define ROZKAZ_USTAW_STAN_NTEJ 			4
#define ROZKAZ_POBIERZ_STAN_NTEJ 		5

#define NIEZNANY_ROZKAZ									0
#define POTWIERDZENIE_TEST 							1
#define POTWIERDZENIE_ZAPAL_LED					2 
#define POTWIERDZENIE_ZGAS_LED 					3
#define POTWIERDZENIE_USTAW_STAN_NTEJ 	4
#define POTWIERDZENIE_POBIERZ_STAN_NTEJ 5


#define PKT_HEADER_LEN				2
#define PKT_MAX_USER_DATA			3
#define PKT_MAX_BYTES					PKT_HEADER_LEN+PKT_MAX_USER_DATA

typedef struct _S_PKT {
	unsigned char		Signature;
	unsigned char 	Len;
	unsigned char   UserData[ PKT_MAX_USER_DATA ];	
} S_PKT, *PS_PKT;

typedef struct S_U0PKT_state {
	char					*RxBuf;
	unsigned char  RxUDLen;
	unsigned char  TxHdr[2];
	volatile signed char		 RxCompleted;
	volatile signed char		 RxTimeout;
	volatile signed char		 TxCompleted;
} S_U0PKT_state, *PS_U0PKT_state;
extern S_U0PKT_state U0PKT_state;

//-----------------------------------------------------------------------
extern int U0PKT_FrameLen;  	

void U0PKT_Open(unsigned long baudrate, unsigned short timeout_in_bits );
void U0PKT_ReinitRx( void );

void U0PKT_AsyncRead( char *p, unsigned char ile );
int  U0PKT_IsReadCompleted( void );
void U0PKT_FlushRead( void );

void U0PKT_AsyncWrite( char *p, unsigned char ile );
int  U0PKT_IsWriteCompleted( void );
void U0PKT_FlushWrite( void );

void U0PKT_Execute( char *p );

int US_CalcPrescaler( unsigned int baud_rate );	


#endif //_US0_PROTOKOL_H_INCLUDED_
