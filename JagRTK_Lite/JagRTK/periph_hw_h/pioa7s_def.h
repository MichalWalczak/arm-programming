/********************************************************************************
  File: pioa7s_def.c   (notacja JagNOT)
  Desc: Wyprowadzenia PIOA w procesorze SAM7S

				Przedstawiony kod opisany jest w rozdziale 4 i 7.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*********************************************************************************/
#ifndef _PIOA_7S_DEF_H_INCLUDED_
#define _PIOA_7S_DEF_H_INCLUDED_


// -------------------------------------------------------------------------------------
#define PIO_PA0    ((unsigned int) 1 <<  0) 
#define 	PWM0_A0     ((unsigned int) PIO_PA0) 
#define 	TIOA0_B0    ((unsigned int) PIO_PA0) 

#define PIO_PA1    ((unsigned int) 1 <<  1) 
#define 	PWM1_A1     ((unsigned int) PIO_PA1)
#define 	TIOB0_B1    ((unsigned int) PIO_PA1) 

#define PIO_PA2    ((unsigned int) 1 <<  2) 
#define 	PWM2_A2     ((unsigned int) PIO_PA2) 
#define 	SCK0_B2     ((unsigned int) PIO_PA2) 

#define PIO_PA3    ((unsigned int) 1 <<  3) 
#define 	TWD_A3      ((unsigned int) PIO_PA3) 
#define 	NPCS3_B3    ((unsigned int) PIO_PA3) 

#define PIO_PA4    ((unsigned int) 1 <<  4) 
#define 	TWCLK_A4    ((unsigned int) PIO_PA4) 
#define 	TCLK0_B4    ((unsigned int) PIO_PA4) 

#define PIO_PA5    ((unsigned int) 1 <<  5) 
#define 	RXD0_A5     ((unsigned int) PIO_PA5) 
#define 	NPCS3_B5    ((unsigned int) PIO_PA5) 

#define PIO_PA6    ((unsigned int) 1 <<  6) 
#define 	TXD0_A6     ((unsigned int) PIO_PA6) 
#define 	PCK0_B6     ((unsigned int) PIO_PA6) 

#define PIO_PA7    ((unsigned int) 1 <<  7) 
#define 	RTS0_A7     ((unsigned int) PIO_PA7) 
#define 	PWM3_B7     ((unsigned int) PIO_PA7) 

#define PIO_PA8    ((unsigned int) 1 <<  8) 
#define 	CTS0_A8     ((unsigned int) PIO_PA8) 
#define 	ADTRG_B8    ((unsigned int) PIO_PA8) 

#define PIO_PA9    ((unsigned int) 1 <<  9) 
#define 	DRXD_A9     ((unsigned int) PIO_PA9) 
#define 	NPCS1_B9    ((unsigned int) PIO_PA9) 

#define PIO_PA10   ((unsigned int) 1 << 10) 
#define 	DTXD_A10    ((unsigned int) PIO_PA10) 
#define 	NPCS2_B10   ((unsigned int) PIO_PA10) 

#define PIO_PA11   ((unsigned int) 1 << 11) 
#define 	NPCS0_A11   ((unsigned int) PIO_PA11) 
#define 	PWM0_B11    ((unsigned int) PIO_PA11) 

#define PIO_PA12   ((unsigned int) 1 << 12) 		
#define 	MISO_A12    ((unsigned int) PIO_PA12) 
#define 	PWM1_B12    ((unsigned int) PIO_PA12) 

#define PIO_PA13   ((unsigned int) 1 << 13) 
#define 	MOSI_A13     ((unsigned int) PIO_PA13)
#define 	PWM2_B13     ((unsigned int) PIO_PA13)

#define PIO_PA14   ((unsigned int) 1 << 14) 
#define 	SPCK_A14     ((unsigned int) PIO_PA14)
#define 	PWM3_B14     ((unsigned int) PIO_PA14)

#define PIO_PA15   ((unsigned int) 1 << 15) 
#define 	TF_A15       ((unsigned int) PIO_PA15) 
#define 	TIOA1_B15    ((unsigned int) PIO_PA15) 




#define PIO_PA16   	((unsigned int) 1 << 16) 
#define 	TK_A16       ((unsigned int) PIO_PA16)
#define 	TIOB1_B16    ((unsigned int) PIO_PA16)

#define PIO_PA17    ((unsigned int) 1 << 17) 
#define 	TD_A17       ((unsigned int) PIO_PA17)
#define 	PCK1_B17     ((unsigned int) PIO_PA17)

#define PIO_PA18    ((unsigned int) 1 << 18) 
#define 	RD_A18       ((unsigned int) PIO_PA18)
#define 	PCK2_B18     ((unsigned int) PIO_PA18)

#define PIO_PA19    ((unsigned int) 1 << 19) 
#define 	RK_A19       ((unsigned int) PIO_PA19)
#define 	FIQ_B19			 ((unsigned int) PIO_PA19)

#define PIO_PA20    ((unsigned int) 1 << 20) 
#define 	RF_A20       ((unsigned int) PIO_PA20)
#define 	IRQ0_B20		 ((unsigned int) PIO_PA20)

#define PIO_PA21    ((unsigned int) 1 << 21) 
#define 	RXD1_A21     ((unsigned int) PIO_PA21) 
#define 	PCK1_B21     ((unsigned int) PIO_PA21) 

#define PIO_PA22    ((unsigned int) 1 << 22) 
#define 	TXD1_A22     ((unsigned int) PIO_PA22) 
#define 	NPCS3_B22    ((unsigned int) PIO_PA22) 

#define PIO_PA23    ((unsigned int) 1 << 23) 
#define 	SCK1_A23     ((unsigned int) PIO_PA23) 
#define 	PWM0_B23     ((unsigned int) PIO_PA23) 

#define PIO_PA24    ((unsigned int) 1 << 24) 
#define 	RTS1_A24     ((unsigned int) PIO_PA24) 
#define 	PWM1_B24     ((unsigned int) PIO_PA24) 

#define PIO_PA25    ((unsigned int) 1 << 25) 
#define 	CTS1_A25     ((unsigned int) PIO_PA25) 
#define 	PWM2_B25     ((unsigned int) PIO_PA25) 

#define PIO_PA26    ((unsigned int) 1 << 26) 
#define 	DCD1_A26     ((unsigned int) PIO_PA26) 
#define 	TIOA2_B26    ((unsigned int) PIO_PA26) 

#define PIO_PA27    ((unsigned int) 1 << 27) 
#define 	DTR1_A27     ((unsigned int) PIO_PA27) 
#define 	TIOB2_B27    ((unsigned int) PIO_PA27) 

#define PIO_PA28    ((unsigned int) 1 << 28) 
#define 	DSR1_A28     ((unsigned int) PIO_PA28) 
#define 	TCLK1_B28    ((unsigned int) PIO_PA28) 

#define PIO_PA29    ((unsigned int) 1 << 29) 
#define 	RI1_A29      ((unsigned int) PIO_PA29) 
#define 	TCLK2_B29    ((unsigned int) PIO_PA29) 

#define PIO_PA30    ((unsigned int) 1 << 30) 
#define 	IRQ1_A30     ((unsigned int) PIO_PA30) 
#define 	NPCS2_B30    ((unsigned int) PIO_PA30) 

#define PIO_PA31    ((unsigned int) 1 << 31) 
#define 	NPCS1_A31    ((unsigned int) PIO_PA31)
#define 	PCK2_B31     ((unsigned int) PIO_PA31)



#endif // of _PIOA_7S_DEF_H_INCLUDED_
