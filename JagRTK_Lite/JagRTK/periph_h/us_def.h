/********************************************************************
  File: us_def.h   (notacja JagNOT)
  Desc: USART

				Przedstawiony kod opisany jest w rozdziale 16.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*********************************************************************/
#ifndef _US_DEF_H_INCLUDED_
#define _US_DEF_H_INCLUDED_
#include "../periph_hw_h/bit_names.h"
#include "../periph_hw_h/reg_typedef.h"
#include "../periph_hw_h/pio.h"
#include "../periph_hw_h/pdc.h"

//------------------------------------------------------------------
typedef struct _S_USART {				// dostep, wart. po resecie, offset, 	nazwa Atmel
		REG	 CONTROL_R; 						// WO,			-,								+0x00,		CR
		REG	 MODE_R; 								// R/W,			-,								+0x04,		MR
		REG	 INT_ENABLE_R; 					// WO,			-,								+0x08,		IER
		REG	 INT_DISABLE_R; 				// WO,			-,								+0x0C,		IDR
		REG	 INT_MASK_R; 						// RO,			0x0,							+0x10,		IMR
		REG	 INT_STATUS_R; 					// RO,			-,								+0x14,		CSR
		REG	 RHR; 									// RO,			0x0,							+0x18,		RHR
		REG	 THR; 									// WO,			-,								+0x1C,		THR
		REG	 BAUD_RATE_R; 					// R/W,			0x0,							+0x20,		BRGR
		REG	 RECEIVER_TIMEOUT_R; 		// R/W,			0x0,							+0x24,		RTOR
		REG	 TRANSMITER_TIMEGUARD_R;// R/W,			0x0,							+0x28,		TTGR
		REG	 rezerw0[5]; 						 														
		REG	 FRACT_DIVIDER_R; 			// R/W,			0x174,						+0x40,		FIDI 
		REG	 NUMBER_OF_ERRS_R;			// RO,			-,								+0x44,		NER 
		REG	 XON_XOFF_R; 						// RESERVED
		REG	 IRDA_FILTR_R; 					// R/W,			0x0, 							+0x4C,		IF
		REG	 MANCHESTER_R;					// R/W,			0x0,							+0x50,		MAN
} S_USART, *PS_USART;

#define  US0   	((PS_USART) 		0xFFFC0000) 
#define  US1   	((PS_USART) 		0xFFFC4000) 

#define  PDC_US0 ((PS_PDC)     	0xFFFC0100) 
#define  PDC_US1 ((PS_PDC)     	0xFFFC4100) 

#define  US0_PERIPH_ID   6
#define  US1_PERIPH_ID	 7
#define  US0_PERIPH_ID_MASK   (0x1 << US0_PERIPH_ID)
#define  US1_PERIPH_ID_MASK   (0x1 << US1_PERIPH_ID)

// ----- rejestr CONTROL_R -------------------------------------------------------
//---- bit 0-7 sa identyczne jak w porcie DBGU
#define US_RESET_RECEIVER  	BIT_2 	
#define US_RESET_TRANSMITER	BIT_3 	
#define US_RX_ENABLE    		BIT_4  	
#define US_RX_DISABLE   		BIT_5  
#define US_TX_ENABLE    		BIT_6  	
#define US_TX_DISABLE   		BIT_7  
#define US_RESET_STATUS_ERR ((unsigned int) 0x1 <<  8) 

#define US_START_BREAK      ((unsigned int) 0x1 <<  9) 
#define US_STOP_BREAK       ((unsigned int) 0x1 << 10) 
#define US_START_TIMEOUT    ((unsigned int) 0x1 << 11) 
#define US_SEND_ADDRESS     ((unsigned int) 0x1 << 12) 
#define US_RESET_ITERATION  ((unsigned int) 0x1 << 13) 
#define US_RESET_NACK      	((unsigned int) 0x1 << 14) 
#define US_REARM_TIMEOUT    ((unsigned int) 0x1 << 15) 
#define US_DTR_ENABLE       ((unsigned int) 0x1 << 16) 
#define US_DTR_DISABLE      ((unsigned int) 0x1 << 17) 
#define US_RTS_ENABLE       ((unsigned int) 0x1 << 18) 
#define US_RTS_DISABLE      ((unsigned int) 0x1 << 19) 

// ----- rejestr MODE_R -------------------------------------------------------
#define US_MODE_FIELD       ((unsigned int) 0xF <<  0)
#define 	US_MODE_NORMAL          ((unsigned int) 0x0)
#define 	US_MODE_RS485           ((unsigned int) 0x1) 
#define 	US_MODE_HW_HSHK         ((unsigned int) 0x2) 
#define 	US_MODE_MODEM           ((unsigned int) 0x3) 
#define 	US_MODE_ISO7816_PROT_T0 ((unsigned int) 0x4) 
#define 	US_MODE_ISO7816_PROT_T1 ((unsigned int) 0x6) 
#define 	US_MODE_IRDA            ((unsigned int) 0x8)
#define US_CLKS_FIELD        ((unsigned int) 0x3 <<  4) 		
#define 	US_CLOCK_MCK            ((unsigned int) 0x0 <<  4)
#define 	US_CL0CK_FDIV1          ((unsigned int) 0x1 <<  4)
#define 	US_CL0CK_RESERVED_SLOW  ((unsigned int) 0x2 <<  4)
#define 	US_CL0CK_EXT            ((unsigned int) 0x3 <<  4)
#define US_CHRL_FIELD        ((unsigned int) 0x3 <<  6) 
#define 	US_5_BITS               ((unsigned int) 0x0 <<  6) 
#define 	US_6_BITS               ((unsigned int) 0x1 <<  6) 
#define 	US_7_BITS               ((unsigned int) 0x2 <<  6) 
#define 	US_8_BITS               ((unsigned int) 0x3 <<  6) 
#define US_SYNC_MODE         ((unsigned int) 0x1 <<  8) 		
#define   US_ASYNC_MODE      ((unsigned int) 0x0 <<  8) 		
#define US_PARITY_FIELD 		 ((unsigned int) 0x7 <<  9) 
#define 	US_PARITY_EVEN 	     		((unsigned int) 0x0 <<  9) 
#define 	US_PARITY_ODD        		((unsigned int) 0x1 <<  9) 
#define 	US_PARITY_SPACE      		((unsigned int) 0x2 <<  9) 	
#define 	US_PARITY_MARK       		((unsigned int) 0x3 <<  9) 	
#define 	US_PARITY_NONE       		BIT_11  
#define 	US_PARITY_MULTI_DROP 		((unsigned int) 0x6 <<  9) 	

#define US_NBSTOP_FIELD      ((unsigned int) 0x3 << 12) 
#define 	US_STOP_1_BIT           ((unsigned int) 0x0 << 12) 
#define 	US_STOP_15_BIT          ((unsigned int) 0x1 << 12) 
#define 	US_STOP_2_BIT           ((unsigned int) 0x2 << 12) 
#define US_MSB_FIRST         ((unsigned int) 0x1 << 16) 
#define   US_LSB_FIRST            ((unsigned int) 0x0 << 16) 
#define US_MODE9             ((unsigned int) 0x1 << 17) 
#define US_CKL_OUTPUT        ((unsigned int) 0x1 << 18) 
#define US_OVERSAMPLING8     ((unsigned int) 0x1 << 19) 
#define   US_OVERSAMPLING16     ((unsigned int) 0x0 << 19)
#define US_INHIBIT_NACK      ((unsigned int) 0x1 << 20) 
#define US_DISABLE_NACK      ((unsigned int) 0x1 << 21) 
#define US_MAX_ITERATION     ((unsigned int) 0x1 << 24) 
#define US_FILTER            ((unsigned int) 0x1 << 28) 

// ----- rejestry INT_ENABLE_R, INT_DISABLE_R, INT_MASK_R, INT_STATUS_R -------------------------------------------------------
#define US_RXRDY        ((unsigned int) 0x1 <<  0) 
#define US_TXRDY        ((unsigned int) 0x1 <<  1) 
#define US_TXEMPTY      ((unsigned int) 0x1 <<  9) 
#define US_ENDRX        ((unsigned int) 0x1 <<  3) 
#define US_ENDTX        ((unsigned int) 0x1 <<  4) 
#define US_TXBUFS_EMPTY ((unsigned int) 0x1 << 11) 
#define US_RXBUFS_FULL  ((unsigned int) 0x1 << 12) 

#define US_OVERRUN_ERR  ((unsigned int) 0x1 <<  5) 
#define US_FRAME_ERR    ((unsigned int) 0x1 <<  6) 
#define US_PARITY_ERR   ((unsigned int) 0x1 <<  7) 

#define US_RX_BREAK     ((unsigned int) 0x1 <<  2) 
#define US_RX_TIMEOUT   ((unsigned int) 0x1 <<  8) 
#define US_ITERATION    ((unsigned int) 0x1 << 10) 
#define US_NACK         ((unsigned int) 0x1 << 13) 

#define US_RI_IN_CHANGE  ((unsigned int) 0x1 << 16) 
#define US_DSR_IN_CHANGE ((unsigned int) 0x1 << 17) 
#define US_DCD_IN_CHANGE ((unsigned int) 0x1 << 18) 
#define US_CTS_IN_CHANGE ((unsigned int) 0x1 << 19) 
//-- dodatkowe bity tylko w INT_STATUS_R ----
#define US_RI_INPUT     ((unsigned int) 0x1 << 20) 
#define US_DSR_INPUT    ((unsigned int) 0x1 << 21) 
#define US_DCD_INPUT    ((unsigned int) 0x1 << 22) 
#define US_CTS_INPUT    ((unsigned int) 0x1 << 23) 



#endif // _US_DEF_H_INCLUDED_
