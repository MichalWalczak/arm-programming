/*********************************************************
  File: sysint_handler_dbgu_pit_rtt.c
  Desc: przerwanie systemowe, wspoldzielone

				Przedstawiony kod opisany jest w rozdziale 13.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
	 	 	 	0.1G - GNU / 2007.08.31
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**********************************************************/
#include "../periph_hw_h/aic.h"
#include "../periph_hw_h/pio.h"
#include "../periph_hw_h/pmc.h"
#include "../periph_hw_h/led_sw.h"
#include "../periph_hw_h/sys_intrpt_handler.h"
#include "../periph_hw_h/dbgu_debug.h"
#include "../periph_hw_h/pit.h"
#include "../periph_hw_h/rtt.h"
#include "proj_h.h"
//--------------------------------------------------------------------------------


int  SYS_irq_counter;						
//------------------------------------------------------------------------------
void SYS_INTRPT_irq_handler( void )  DEFINE_AS_IRQ_HANDLER
{	
 int status_r, status_and_mask;	
 	//INT_NEST_ENABLE;

		SYS_irq_counter++; 					
		
		status_r = DBGU->INT_STATUS_R;
		status_and_mask = status_r & DBGU->INT_MASK_R;
		if( status_and_mask ){  			
			DBGU_irq_kernel( status_and_mask, status_r );
		}


		if( PIT->MODE_R & PIT_INT_ENABLE ){ 
			status_r = PIT->STATUS_R; 			  
			if( status_r & PIT_STATUS ){ 			
					PIT_irq_kernel( status_r );
			}		
		}
		
	 	if( RTT->MODE_R & RTT_ALARM_INT_ENABLE ){  
			status_r = RTT->STATUS_R ; 		
			RTT_irq_kernel( status_r );
			//RTT_irq(status_r);
		}

 	//INT_NEST_DISABLE;
 	AIC_EOI;
}
