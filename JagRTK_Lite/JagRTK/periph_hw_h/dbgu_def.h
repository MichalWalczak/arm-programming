/********************************************************************
  File: dbgu_def.h   (notacja JagNOT)
  Desc: dbgu 

				Przedstawiony kod opisany jest w rozdziale 11.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*********************************************************************/
#ifndef _PORT_DBGU_DEF_H_INCLUDED_
#define _PORT_DBGU_DEF_H_INCLUDED_
#include "../periph_hw_h/bit_names.h"
#include "../periph_hw_h/reg_typedef.h"
#include "../periph_hw_h/pio_def.h"
#include "../periph_hw_h/pdc_def.h"
#include "../periph_hw_h/sys_intrpt_handler.h"
#include <stdio.h>




//------------------------------------------------------------------
typedef struct _S_DBGU {
	REG	 CONTROL_R; 				// , CR									
	REG	 MODE_R; 						// , MR 								
	REG	 INT_ENABLE_R; 			// , IER								
	REG	 INT_DISABLE_R; 		// , IDR
	REG	 INT_MASK_R; 				// , IMR								
	REG	 INT_STATUS_R; 			// , CRS								
	REG	 RECEIVER_HOLDING_R;// , RHR
	REG	 TRANSMIT_HOLDING_R;// , THR
	REG	 BAUD_RATE_GEN_R; 	// , BRGR
	REG	 Reserved0[7]; 			// 
	REG	 CHIP_ID_R; 				//
	REG	 CHIP_ID_EXT_R; 		//
	REG	 FORCE_NTRST_R; 		//
} S_DBGU, *PS_DBGU;				//

#define  DBGU   	((PS_DBGU) 		0xFFFFF200) 
#define  PDC_DBGU ((PS_PDC)     0xFFFFF300) 

// ----- rejestr CONTROL_R -------------------------------------------------------
#define DBGU_RESET_RX  				BIT_2 	
#define DBGU_RESET_TX 				BIT_3 	
#define DBGU_RX_ENABLE    		BIT_4  	
#define DBGU_RX_DISABLE   		BIT_5  
#define DBGU_TX_ENABLE    		BIT_6  	
#define DBGU_TX_DISABLE   		BIT_7  
#define DBGU_RESET_ERROR_BITS   BIT_8 

// --- rejestry: INT_ENABLE_R, INT_DISABLE_R, INT_MASK_R, INT_STATUS_R -----------
#define DBGU_RX_RDY 				BIT_0 	
#define DBGU_TX_RDY  				BIT_1 	
#define DBGU_ENDRX  				BIT_3 	
#define DBGU_ENDTX  				BIT_4 	
#define DBGU_OVERRUN_ERR 		((unsigned int) 0x1 <<  5) 
#define DBGU_FRAME_ERR   		((unsigned int) 0x1 <<  6) 
#define DBGU_PARIRY_ERR  		((unsigned int) 0x1 <<  7) 
#define DBGU_TX_EMPTY    		((unsigned int) 0x1 <<  9) 
#define DBGU_TX_BUFS_EMPTY  ((unsigned int) 0x1 << 11) 
#define DBGU_RX_BUFS_FULL   ((unsigned int) 0x1 << 12) 
#define DBGU_COMM_TX     		((unsigned int) 0x1 << 30) 
#define DBGU_COMM_RX     		((unsigned int) 0x1 << 31) 


// -------- MODE_R  -------- 
#define DBGU_PAR_field   		((unsigned int) 0x7 <<  9) 
#define 	DBGU_PARITY_EVEN 	    ((unsigned int) 0x0 <<  9) 
#define 	DBGU_PARITY_ODD        ((unsigned int) 0x1 <<  9) 
#define 	DBGU_PARITY_SPACE      ((unsigned int) 0x2 <<  9) 
#define 	DBGU_PARITY_MARK       ((unsigned int) 0x3 <<  9) 
#define 	DBGU_PARITY_NONE       BIT_11  


#define DBGU_MODE_field   ((unsigned int) 0x3 << 14) 
#define 	DBGU_MODE_NORMAL  				((unsigned int) 0x0 << 14) 
#define 	DBGU_MODE_AUTO_ECHO    		((unsigned int) 0x1 << 14) 
#define 	DBGU_MODE_LOCAL_LOOPBACK  ((unsigned int) 0x2 << 14) 
#define 	DBGU_MODE_REMOTE_LOOPBACK ((unsigned int) 0x3 << 14) 



#endif // _DBGU_DEF_H_INCLUDED_
