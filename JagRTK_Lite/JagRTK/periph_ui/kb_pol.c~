/***********************************************************************
  File: kb_pooling.c
  Desc: Sterownik (driver) klawiatury obslugiwanej w trybie odpytywania

				Przedstawiony kod opisany jest w rozdziale 6 i 7.

				Szczegolowy opis programu, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************************************************/
#include "../periph_hw_h/pmc.h"
#include "../periph_hw_h/pio.h"
#include "../periph_hw_h/led_sw.h"
#include "../periph_hw_h/delay.h"
#include "../periph_hw_h/board_pins.h"

#include "../periph_ui_h/kb_keycodes_def.h"
#include "../periph_ui_h/kb_pol.h"

//-------------------------------------------------------------------------
static struct{						// zmienne stanu
	int Key;								// kod klawisza	
	int LastState;					// zapamietany stan wyprowadzen
	int Mask;								// maska wyprowadzen
}KB_state;

//--------------------------------------------------------------------------------------
void KBPOL_Open( unsigned int kbmask )
{
	KB_state.Key = KEY_NO_PRESSED;
	KB_state.LastState = KEY_NO_PRESSED;
	KB_state.Mask = kbmask;
	SW_Open( kbmask );			// skonfiguruj wejscia
}
//--------------------------------------------------------------------------------------
void KB_CheckState( void )
{
 int stan;
 	stan = SW_IsPressed( KB_state.Mask );
	if( stan != 0 ){
	 	if( stan != KB_state.LastState ){			
			switch( stan ){
			case SW0_ENTER:	KB_state.Key = KEY_ENTER;	 break;
			case SW1_DOWN:	KB_state.Key = KEY_DOWN;	 break;
			case SW2_UP:		KB_state.Key = KEY_UP;		 break;
			case SW3_ESC:		KB_state.Key = KEY_ESC;		 break;
			default:				KB_state.Key = KEY_NO_PRESSED;	break;		
			}
		}		
	}
	KB_state.LastState = stan;					
}
//--------------------------------------------------------------------------------------
int KB_ReadKey( void )
{
 int ret = KB_state.Key;		
	KB_state.Key = KEY_NO_PRESSED;			
	return(ret);
}

//--------------------------------------------------------------------------------------
int KB_IsKey( void )
{
 if(KB_state.Key==KEY_NO_PRESSED) return(0);
 else return(1);
}
//--------------------------------------------------------------------------------------
int KB_WaitKey( void )
{
 	do{
		KB_CheckState();
	}while( !KB_IsKey() );
	return( KB_ReadKey() );
}
