#ifndef LED_H
#define LED_H

#include "library\AT91SAM7S64.h"
#include "library\lib_AT91SAM7S64.h"


void LED_open(unsigned int pin_mask)
{
	AT91F_PIO_CfgOutput(AT91C_BASE_PIOA, pin_mask);
}

//------------------------------------------------------
void LED_set(unsigned int pin_mask)
{
	AT91F_PIO_ClearOutput(AT91C_BASE_PIOA, pin_mask);
}

//------------------------------------------------------

void LED_clear(unsigned int pin_mask)
{
	AT91F_PIO_SetOutput(AT91C_BASE_PIOA, pin_mask);
}
//------------------------------------------------------

void LED_toggleOne(unsigned int pin_mask)
{
	if(AT91F_PIO_GetStatus(AT91C_BASE_PIOA) &&  pin_mask)
			AT91F_PIO_ClearOutput(AT91C_BASE_PIOA, pin_mask);
	else
			AT91F_PIO_SetOutput(AT91C_BASE_PIOA, pin_mask);
				
}

//------------------------------------------------------

unsigned int LED_getState(unsigned int pin_mask)
{

	return ~(AT91F_PIO_GetStatus(AT91C_BASE_PIOA) &&  pin_mask);
}

#endif //LED_H
