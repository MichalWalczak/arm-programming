/********************************************************************
  File: startup_C.h
  Desc: funkcje startup urz. peryferyjnych w jezyku C

				Przedstawiony kod opisany jest w rozdziale 5.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Metody projektowania systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)
   Ver: 0.1 / 2007.07.25
        0.1G GNU / 2007.08.31
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*********************************************************************/
#ifndef _STARTUP_C_H_INCLUDED_
#define _STARTUP_C_H_INCLUDED_
//#include "../../JagRTK/periph_hw_h/aic.h"

#define	JTAG_RAM			1			// @2MB
#define	JTAG_FLASH		2			// @0MB
#define SAMBA_FLASH		3			// @1MB

#define	JTAG_RAM_MEMORY_TYPE			1			// @2MB, w run-time zrobi remap na RAM @0x0
#define	JTAG_FLASH_MEMORY_TYPE		2			// @0MB, wymagane jest tylko usuniecie remap, ale uwaga: loader przed flashowaniem do @0MB
#define SAMBA_FLASH_MEMORY_TYPE		3			// @1MB, ....... nie testowane, zaden remap nie jest wymagany bo wektory sa pod 0x0 domyslnie 



//----------------------------------------------------------------------------
void Periph_Startup_C( void );    // DECLARE_AS_ARM_FUNCTION;
void WDT_Startup( void );
void MC_Startup( void );
int  MC_IsRAMRemapped( void );
void MC_REMAP( void );
void PMC_Startup_48MHz( void );
void RSTC_Startup( void );
void AIC_Startup(void);


#endif // of _STARTUP_C_H_INCLUDED_
