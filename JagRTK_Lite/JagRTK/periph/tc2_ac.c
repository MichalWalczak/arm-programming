/*****************************************************************************
  File: tc2_ac.c
  Desc: Sterownik (driver) timera TC2 do wyzwalania przetwornika ADC, 
        Kod maksymalnie uproszczony.

				Przedstawiony kod opisany jest w rozdziale 15.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
	 			0.1G - GNU / 2007.08.31
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*******************************************************************************/
#include "proj_config.h"
#include "../periph_hw_h/pmc.h"
#include "../periph_hw_h/pio.h"
#include "../periph_hw_h/board_pins.h"
#include "../periph_hw_h/aic.h"
#include "../periph_hw_h/led_sw.h"
#include "../periph_hw_h/dbgu_debug.h"
#include "../periph_h/tc_def.h"
#include "../periph_h/ac_tc2dma.h"


int TC2AC_state_licznik;
//--------------------------------------------------------------------------------
void TC2AC_irq_handler(void)  DEFINE_AS_IRQ_HANDLER
{ 
  int status_r = TC2->INT_STATUS_R;	

		TC2AC_state_licznik++;					
  	//LED_ToggleOne( PIO_PA2 ); 		 	// opcjonalna wizualizacja

  AIC->EOI_R=0;											
}
//----------------------------------------------------------------------------------
void TC2AC_Open( int val_Hz )
{
 int mode, action;
 int max_tcfreq, rc_temp;
		
		PMC_OpenPeriphClock( TC2_PERIPH_ID_MASK );	
  	TC2->CONTROL_R = TC_CLK_DISABLE;						
  	TC2->INT_DISABLE_R = 0xFFFFFFFF;						

		 mode = 
					(	TC_MCK1024													
					| TC_CLOCK_NOT_INVERTED
					| TC_CLOCK_GATED_BY_NONE

					| TC_NOT_STOP_ON_RC_COMPARE				
					| TC_NOT_DISABLE_COUNTER_CLOCK_ON_RC_COMPARE 

					| TC_EXTERNAL_EVT_EDGE_NONE 			
					| TC_EXTERNAL_EVT_SEL_XC0					
					| TC_EXTERNAL_EVT_TRIGGER_NOT_ENABLE_RESET_PV

					| TC_MODE_UP_WITH_AUTO_RESET_ON_RC_COMPARE	
					| TC_MODE_WAVE_PWM								
					);
		 action = 
					( TC_ON_RA_COMPARE_TIOA_SET		 		
					| TC_ON_RC_COMPARE_TIOA_CLEAR			
					| TC_ON_EXTERNAL_EVENT_TIOA_NONE	
					| TC_ON_SOFT_TRIGGER_TIOA_CLEAR		

					| TC_ON_RB_COMPARE_TIOB_NONE			
					| TC_ON_RC_COMPARE_TIOB_NONE
					| TC_ON_EXTERNAL_EVENT_TIOB_NONE
					| TC_ON_SOFT_TRIGGER_TIOB_NONE 		
					);					 
		TC2->MODE_R = mode | action; 	
		
		max_tcfreq = (fMCK_Hz / 1024);			
		rc_temp = (max_tcfreq / val_Hz );
		//if( rc_temp > 0x0FFFF ) rc_temp=0x0FFFF;			// ogranicz
		TC2->RC = rc_temp;
		TC2->RA = rc_temp>>1;								

  	TC2->CONTROL_R = TC_CLK_ENABLE | TC_SOFTWARE_TRIGGER;	
		DBGU_Trace("TC2AC Opened %i[Hz].\n\r", val_Hz );
}

