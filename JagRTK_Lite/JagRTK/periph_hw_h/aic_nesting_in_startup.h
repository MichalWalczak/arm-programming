/********************************************************************
	File: aic_nesting_in_startup.h
	Desc: makra dla startup'u GNU_JagRTK_Lite/070831
	  By: JAG
	 Ver: 0.1
*********************************************************************/
#ifndef _AIC_NESTING_IN_STARTUP_H_INCLUDED_
#define _AIC_NESTING_IN_STARTUP_H_INCLUDED_

#define  DECLARE_AS_IRQ_HANDLER    
#define  DEFINE_AS_IRQ_HANDLER     

#define  DECLARE_AS_FIQ_HANDLER    
#define  DEFINE_AS_FIQ_HANDLER     


#define  DECLARE_AS_ARM_FUNCTION   
#define  DEFINE_AS_ARM_FUNCTION    

#define  AIC_EOI  

#define  INT_NEST_ENABLE   
#define  INT_NEST_DISABLE  



#endif // of _AIC_NESTING_IN_STARTUP_H_INCLUDED_
