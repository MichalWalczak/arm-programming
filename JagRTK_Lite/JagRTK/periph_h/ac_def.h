/********************************************************************
  File: ac_def.h   (notacja JagNOT)
  Desc: ADC

				Przedstawiony kod opisany jest w rozdziale 15.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*********************************************************************/
#ifndef _ADC_DEF_H_INCLUDED_
#define _ADC_DEF_H_INCLUDED_
#include "../periph_hw_h/reg_typedef.h"
#include "../periph_hw_h/bit_names.h"




//-------------------------------------------------------------------
typedef struct S_ADC {// dostep, wartosc po resecie, offset, nazwa [AT]
	REG	 	CONTROL_R;					// WO,	-,					+0x00, 	CR
	REG	 	MODE_R;							// R/W,	0x0,				+0x04, 	MR
	REG	 	rezerw0[2]; 				// 
	REG	 	CHANNEL_ENABLE_R;		// WO,	-,					+0x10,	CHER
	REG	 	CHANNEL_DISABLE_R;	// WO,	-,					+0x14,	CHDR 
	REG	 	CHANNEL_STATUS_R;		// R0,	0x0,				+0x18,	CHSR 
	REG	 	INT_STATUS_R;				// RO,	0x000C0000, +0x1C,	SR 	
	REG	 	LAST_CONV_DATA_R;		// RO,	0x0,				+0x20,	LCDR 
	REG	 	INT_ENABLE_R;				// WO,	-,					+0x24,	IER 	
	REG	 	INT_DISABLE_R;			// WO,	-,				 	+0x28,	IDR 	
	REG	 	INT_MASK_R;					// RO,	0x0,			 	+0x2C,	IMR 	
	REG	 	CHANNEL_DATA_R[8];	// RO,	0x0,				+0x30,	CDR0-7
} S_ADC, *PS_ADC;


#define ADC   ((PS_ADC)	0xFFFD8000) 

#define PDC_ADC  ((PS_PDC) 0xFFFD8100)   	

#define 	ADC_PERIPH_ID 	4   			
#define 	ADC_PERIPH_ID_MASK  (0x1 << ADC_PERIPH_ID)  	



// -------- CONTROL_R --------------------------------
#define ADC_SOFT_RESET  	((unsigned int) 0x1 <<  0) 
#define ADC_START_CONV    ((unsigned int) 0x1 <<  1) 
// -------- MODE_R    --------------------------------
#define ADC_HW_TRG_ENABLE    ((unsigned int) 0x1 << 0) 
#define 	ADC_HW_TRG_DISABLE    ((unsigned int) 0x0 << 0) 
#define ADC_TRG_SEL_field    ((unsigned int) 0x7 << 1) 
#define 	ADC_TRG_TIOA0            ((unsigned int) 0x0 << 1) 
#define 	ADC_TRG_TIOA1            ((unsigned int) 0x1 << 1) 
#define 	ADC_TRG_TIOA2            ((unsigned int) 0x2 << 1) 
#define 	ADC_TRG_EXT_PIN          ((unsigned int) 0x6 << 1) 
#define ADC_RESOLUTION     		((unsigned int) 0x1 << 4) 
#define ADC_RESOLUTION_8BIT   ((unsigned int) 0x1 << 4) 
#define 	ADC_RESOLUTION_10BIT     ((unsigned int) 0x0 <<  4) 
#define ADC_SLEEP          		((unsigned int) 0x1 << 5) 
#define ADC_POWER_SLEEP_MODE  ((unsigned int) 0x1 << 5) 
#define 	ADC_POWER_NORMAL_MODE    ((unsigned int) 0x0 <<5) 

#define ADC_PRESCALER_bsf     8 
#define ADC_PRESCALER_field   ((unsigned int) 0x3F <<  8) 
#define ADC_STARTUP_TIME_CFG_bsf  16
#define ADC_STARTUP_TIME_field ((unsigned int) 0x1F << 16)
#define ADC_SH_TIME_CFG_bsf		24
#define ADC_SH_TIME_field     ((unsigned int) 0xF << 24) 
// -------- 	CHANNEL_ENABLE_R 
// -------- 	CHANNEL_DISABLE_R  
// -------- 	CHANNEL_STATUS_R 
#define ADC_CH0         ((unsigned int) 0x1 << 0) 
#define ADC_CH1         ((unsigned int) 0x1 << 1) 
#define ADC_CH2         ((unsigned int) 0x1 << 2) 
#define ADC_CH3         ((unsigned int) 0x1 << 3) 
#define ADC_CH4         ((unsigned int) 0x1 << 4) 
#define ADC_CH5         ((unsigned int) 0x1 << 5) 
#define ADC_CH6         ((unsigned int) 0x1 << 6) 
#define ADC_CH7         ((unsigned int) 0x1 << 7) 

// -------- INT_ENABLE_R
// -------- INT_DISABLE_R
// -------- INT_MASK_R
// -------- INT_STATUS_R
#define ADC_EOC0        ((unsigned int) 0x1 <<  0) 
#define ADC_EOC1        ((unsigned int) 0x1 <<  1) 
#define ADC_EOC2        ((unsigned int) 0x1 <<  2) 
#define ADC_EOC3        ((unsigned int) 0x1 <<  3) 
#define ADC_EOC4        ((unsigned int) 0x1 <<  4) 
#define ADC_EOC5        ((unsigned int) 0x1 <<  5) 
#define ADC_EOC6        ((unsigned int) 0x1 <<  6) 
#define ADC_EOC7        ((unsigned int) 0x1 <<  7) 
#define ADC_OVRE0       ((unsigned int) 0x1 <<  8) 
#define ADC_OVRE1       ((unsigned int) 0x1 <<  9) 
#define ADC_OVRE2       ((unsigned int) 0x1 << 10) 
#define ADC_OVRE3       ((unsigned int) 0x1 << 11) 
#define ADC_OVRE4       ((unsigned int) 0x1 << 12) 
#define ADC_OVRE5       ((unsigned int) 0x1 << 13) 
#define ADC_OVRE6       ((unsigned int) 0x1 << 14) 
#define ADC_OVRE7       ((unsigned int) 0x1 << 15) 
#define ADC_DRDY        ((unsigned int) 0x1 << 16) 
#define ADC_GENERAL_OVRE ((unsigned int) 0x1 << 17) 
#define ADC_ENDRX       ((unsigned int) 0x1 << 18) 
#define ADC_RX_BUFS_FULL ((unsigned int) 0x1 << 19)
// -------- LAST_CONV_R ---
#define ADC_LAST_DATA_field   ((unsigned int) 0x3FF <<  0) 
// -------- CONV_DATA_R[0-7] ---
#define ADC_CONV_DATA_field   ((unsigned int) 0x3FF <<  0) 




#endif //_ADC_DEF_H_INCLUDED_
