/**************************************************************
	File: proj_H.h
  Desc: Dolaczenie wszystkich	wymaganych dla tego projektu header'ow 

				Przedstawiony kod opisany jest w rozdziale 7.

				Szczegolowy opis programu, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
	 			0.1G - GNU / 2007.08.31
	 Web: www.sparrow-rt.com

   * Copyright (C) 2006-2007 by JAG Jacek Augustyn. All rights reserved.
   *
   * Redistribution and use in source and binary forms, with or without
   * modification, are permitted provided that the following conditions
   * are met:
   *
   * 1. Redistributions of source code must retain the above copyright
   *    notice, this list of conditions and the following disclaimer.
   * 2. Redistributions in binary form must reproduce the above copyright
   *    notice, this list of conditions and the following disclaimer in the
   *    documentation and/or other materials provided with the distribution.
   * 3. Neither the name of the copyright holders nor the names of
   *    contributors may be used to endorse or promote products derived
   *    from this software without specific prior written permission.
   *
   * THIS SOFTWARE IS PROVIDED BY JAG AND CONTRIBUTORS
   * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
   * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL EGNITE
   * SOFTWARE GMBH OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
   * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
   * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
   * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
   * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
   * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
   * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
   * SUCH DAMAGE.

***************************************************************/
#ifndef _PROJ_H_H_INCLUDED_
#define _PROJ_H_H_INCLUDED_

#include "JagRTK/periph_hw_h/reg_typedef.h"
#include "JagRTK/periph_hw_h/pio.h"
#include "JagRTK/periph_hw_h/board_pins.h"
#include "JagRTK/periph_hw_h/led_sw.h"
#include "JagRTK/periph_hw_h/pmc.h"
#include "JagRTK/periph_hw_h/delay.h"
#include "JagRTK/periph_hw_h/dbgu_debug.h"
#include "JagRTK/periph_ui_h/kb_keycodes_def.h"
#include "JagRTK/periph_ui_h/kb_pol.h"

#include "JagRTK/periph_ui_h/hd44780.h"

#include "JagRTK/periph_hw_h/aic.h"


#include <stdio.h>
#include <stddef.h>


void main_init( void );


#endif //_PROJ_H_H_INCLUDED_

