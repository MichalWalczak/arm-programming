/*******************************************************************
	File: sys_variable.c
  Desc: niektore zmienne systemowe
    By: JAG (Jacek Augustyn)  
   Ver: 0.1G / 2007.08.31
	 Web: www.sparrow-rt.com

   * Copyright (C) 2006-2007 by JAG Jacek Augustyn. All rights reserved.
   *
   * Redistribution and use in source and binary forms, with or without
   * modification, are permitted provided that the following conditions
   * are met:
   *
   * 1. Redistributions of source code must retain the above copyright
   *    notice, this list of conditions and the following disclaimer.
   * 2. Redistributions in binary form must reproduce the above copyright
   *    notice, this list of conditions and the following disclaimer in the
   *    documentation and/or other materials provided with the distribution.
   * 3. Neither the name of the copyright holders nor the names of
   *    contributors may be used to endorse or promote products derived
   *    from this software without specific prior written permission.
   *
   * THIS SOFTWARE IS PROVIDED BY JAG AND CONTRIBUTORS
   * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
   * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL EGNITE
   * SOFTWARE GMBH OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
   * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
   * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
   * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
   * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
   * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
   * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
   * SUCH DAMAGE.

************************************************************************/
#include "proj_config.h"
#include "JagRTK/startup_h/startup_C.h"

int fMCK_Hz=MCK;			// wartosc podczas inicjalizacji 

#if defined JTAG_RAM_RUNTIME
	int RunTimeMemoryType = JTAG_RAM_MEMORY_TYPE;
#elif defined SAMBA_FLASH_RUNTIME
	int RunTimeMemoryType = SAMBA_FLASH_MEMORY_TYPE; 
#elif defined JTAG_FLASH_RUNTIME
	int RunTimeMemoryType = JTAG_FLASH_MEMORY_TYPE; 
#else
	#error "Niezdefiniowany wariant projektu"
#endif

int MainIterations=0;
int MainDelay=250;
