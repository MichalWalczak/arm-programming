/*********************************************
  File:	hd44780_def.h
  Desc:	Bity i slowa sterujace HD 44780

				Przedstawiony kod opisany jest w rozdziale 6 i 7.

				Szczegolowy opis programu, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

****************************************************************************/
#ifndef _HD44780_DEF_H_INCLUDED_
#define _HD44780_DEF_H_INCLUDED_


#define LCD_KONFIG_PARAM_R  	0x20  
#define LCD_MATRYCA_10x5 				(0x1<<2)
#define LCD_MATRYCA_5x7  				(0x0<<2)
#define LCD_2LINIE     					(0x1<<3)
#define LCD_1LINIA     					(0x0<<3)
#define LCD_8BIT       					(0x1<<4)
#define LCD_4BIT       					(0x0<<4)

#define LCD_KONFIG_RUCHKIER_R 0x10	
#define LCD_RUCH_DO_PRAWEJ 			(0x1<<2)
#define LCD_RUCH_DO_LEWEJ  			(0x0<<2)
#define LCD_RUCH_OBRAZU   			(0x1<<3)
#define LCD_RUCH_KURSORA  			(0x0<<3)

#define LCD_KONFIG_KURSOR_R 	0x08	  
#define LCD_KURSOR_BLOKOWY      (0x1<<0)
#define LCD_KURSOR_PODKRESLENIE (0x0<<0)
#define LCD_KURSOR_OFF 					(0x1<<1)
#define LCD_KURSOR_ON						(0x0<<1)
#define LCD_DISP_ON 						(0x1<<2)
#define LCD_DISP_OFF 						(0x0<<2)

#define LCD_KONFIG_INKR_WST_R	0x04	
#define LCD_WSTAWIANIE    			(0x1<<0)
#define LCD_NADPISYWANIE  			(0x0<<0)
#define LCD_INKREMENTACJA 			(0x1<<1)
#define LCD_DEKREMENTACJA 			(0x0<<1)

#define LCD_CLRSCR_R					0x01	
#define LCD_HOME_R						0x02	

#define LCD_SET_DATA_PTR_R		0x80


#endif // _HD44780_DEF_H_INCLUDED_
