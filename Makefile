#***********************************************************************************
# File: Makefile
# Desc: makefile
#   By: JAG (Jacek Augustyn)  
#  Ver: 0.1G / 2007.08.31
#  Web: www.sparrow-rt.com
#
#   * Copyright (C) 2006-2007 by JAG Jacek Augustyn. All rights reserved.
#   *
#   * Redistribution and use in source and binary forms, with or without
#   * modification, are permitted provided that the following conditions
#   * are met:
#   *
#   * 1. Redistributions of source code must retain the above copyright
#   *    notice, this list of conditions and the following disclaimer.
#   * 2. Redistributions in binary form must reproduce the above copyright
#   *    notice, this list of conditions and the following disclaimer in the
#   *    documentation and/or other materials provided with the distribution.
#   * 3. Neither the name of the copyright holders nor the names of
#   *    contributors may be used to endorse or promote products derived
#   *    from this software without specific prior written permission.
#   *
#   * THIS SOFTWARE IS PROVIDED BY JAG AND CONTRIBUTORS
#   * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#   * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL EGNITE
#   * SOFTWARE GMBH OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#   * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#   * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
#   * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
#   * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#   * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
#   * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
#   * SUCH DAMAGE.
#***********************************************************************************
 
TARGET_SUBNAME = wynik
MAKEFILE_NAME = Makefile
JAGRTK_PATH   = JagRTK_Lite

RUN_MODE   	 = SAMBA_FLASH
#RUN_MODE      = JTAG_RAM
#RUN_MODE   	= JTAG_FLASH												# nieudostepniane w tej wersji

PROGRAMATOR   = OPEN_OCD_USB
#PROGRAMATOR  = OPEN_OCD_WIGGLER

IRQ_HDLR		  = VIC_STARTUP_NEST_CALLS_IRQ_HANDLERS
#IRQ_HDLR	    = VIC_DIRECT_CALLS_IRQ_HANDLERS_GNU	 # nieudostepniane w tej wersji

#THUMB        = -mthumb														 # nieudostepniane w tej wersji
THUMB_IW      = -mthumb-interwork
		  
OPTIMIZATION  = s


#---
CINCS         =
EXTRAINCDIRS  = 
DEBUG         = stabs

#---------------------------------------------------------------------------------
#---- pliki zrodlowe wchodzace w slad projektu -----------------------
C_SRC_32BITARM = main.c 																			
C_SRC_32BITARM += sys/syscalls.c  sys/sys_variable.c


C_SRC_32BITARM += \
                  $(JAGRTK_PATH)/JagRTK/startup/Periph_Startup_C_JagRTK_Lite_GNU.c 	\
                  \
				 $(JAGRTK_PATH)/JagRTK/periph_hw/delay.c 	\
                  $(JAGRTK_PATH)/JagRTK/periph_hw/pmc.c 		\
                  $(JAGRTK_PATH)/JagRTK/periph_hw/pio.c			\
                  $(JAGRTK_PATH)/JagRTK/periph_hw/led_sw.c  	\
				  $(JAGRTK_PATH)/JagRTK/periph_hw/pdc.c  		\
																\
                  $(JAGRTK_PATH)/JagRTK/periph_ui/kb_pol.c  	\
                  $(JAGRTK_PATH)/JagRTK/periph_ui/hd44780.c

C_SRC_32BITARM += $(SRC-LIB-MODULES)

#--- pliki zrodlowe asm
ASM_SRC_32BITARM = $(JAGRTK_PATH)/JagRTK/startup/startup_7s_JagRTK_Lite_GNU.S
#ASM_SRC_32BITARM = sys/startup_7s_JagRTK_Lite_GNU.S


#---------------------------------------------------------------------------------
RUN_MODE_DEF = $(RUN_MODE)_RUNTIME

C_DEFS		   = -D$(RUN_MODE_DEF) -D$(IRQ_HDLR) -I$(JAGRTK_PATH)
A_DEFS 			 = -D$(RUN_MODE_DEF) -D$(IRQ_HDLR) -I$(JAGRTK_PATH)

TARGET 			 = $(TARGET_SUBNAME)_$(RUN_MODE)

C_FLAGS      = -g$(DEBUG) -O$(OPTIMIZATION) $(C_DEFS) $(CINCS)

CMD_ELFSIZE  = arm-elf-size  -A -x objs/$(TARGET).elf
CMD_HEXSIZE  = arm-elf-size  --target=ihex objs/$(TARGET).hex

LST_C_ASM		  = NO

#ifeq ($(LST_C_ASM),YES)
#  C_FLAGS += -Wa,-adhlns=$(subst $(suffix $<),.lst,$<)
#endif	

include $(JAGRTK_PATH)/JagRTK/makefiles/MakeFlags.mk

#-----------------------------------------------------
C_OBJARM = $(C_SRC_32BITARM:.c=.o)
A_OBJARM = $(ASM_SRC_32BITARM:.S=.o)
LST      = $(ASM_SRC_32BITARM:.S=.lst) $(C_SRC_32BITARM:.c=.lst)
					 
#------------------------------------------------------------------------------------------------
build: dispbegin elf hex bin sizeelf sizehex finished dispend
all:   dispbegin gccversion build finished dispend

elf:   objs/$(TARGET).elf
hex:   objs/$(TARGET).hex
bin:   objs/$(TARGET).bin

include $(JAGRTK_PATH)/JagRTK/makefiles/MakeRules.mk

dispbegin: 
	@echo "-------- start make process -------"
finished:
	@echo "-------- finished ---------"
dispend: 
	@echo "-------- end make process ---------"

sizeelf:
	@if [ -f objs/$(TARGET).elf ]; then echo; $(CMD_ELFSIZE); fi
sizehex:
	@if [ -f objs/$(TARGET).elf ]; then $(CMD_HEXSIZE); fi

include $(JAGRTK_PATH)/JagRTK/makefiles/MakeLoad.mk


#-----------------------------------------------------------------------------------------------
clean: dispbegin clean_list finished dispend

REMOVE = rm -f
SHELL  = sh
clean_list :
	@echo
	@echo "usuwanie plikow posrednich (clean)"
	$(REMOVE) objs/$(TARGET).bin
	$(REMOVE) objs/$(TARGET).hex	
	$(REMOVE) objs/$(TARGET).obj
	$(REMOVE) objs/$(TARGET).elf
	$(REMOVE) objs/$(TARGET).map
	$(REMOVE) objs/$(TARGET).obj
	$(REMOVE) objs/$(TARGET).sym
	$(REMOVE) objs/$(TARGET).lnk
	$(REMOVE) objs/$(TARGET).lss
	$(REMOVE) $(C_OBJ)
	$(REMOVE) $(CPPOBJ)
	$(REMOVE) $(A_OBJ)
	$(REMOVE) $(C_OBJARM)
	$(REMOVE) $(CPPOBJARM)
	$(REMOVE) $(A_OBJARM)
	$(REMOVE) $(LST)
	$(REMOVE) $(SRC:.c=.s)
	$(REMOVE) $(SRC:.c=.d)
	$(REMOVE) $(C_SRC_32BITARM:.c=.s)
	$(REMOVE) $(C_SRC_32BITARM:.c=.d)
	$(REMOVE) $(CPPSRC:.cpp=.s) 
	$(REMOVE) $(CPPSRC:.cpp=.d)
	$(REMOVE) $(CPPSRCARM:.cpp=.s) 
	$(REMOVE) $(CPPSRCARM:.cpp=.d)
	$(REMOVE) .dep/*

# Include the dependency files.
-include $(shell mkdir .dep 2>/dev/null) $(wildcard .dep/*)
	
# Listing of phony targets.
.PHONY : all dispbegin finish dispend sizeelf sizehex gccversion \
         build elf hex bin lss sym clean clean_list 

