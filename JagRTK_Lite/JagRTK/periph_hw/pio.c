/********************************************************************************
  File: pio.c
  Desc: Sterownik (driver) pio/gpio

				Przedstawiony kod opisany jest w rozdziale 6 i 7.

				Szczegolowy opis programu, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*********************************************************************************/
#include "../periph_hw_h/reg_typedef.h"
#include "../periph_hw_h/pmc.h"
#include "../periph_hw_h/pio.h"
#include "../periph_hw_h/board_pins.h"


//-------------------------------------------------------------------------
inline void GPIO_OpenInputNoPullUp( unsigned int pin_mask)
{
	PIO->PIO_ENABLE_R = pin_mask;									
	PIO->PULLUP_DISABLE_R = pin_mask;						
	PIO->OUTPUT_DISABLE_R = pin_mask;	
}
//-------------------------------------------------------------------------
inline void GPIO_OpenInputPullUp( unsigned int pin_mask)
{
	PIO->PIO_ENABLE_R = pin_mask;									
	PIO->OUTPUT_DISABLE_R = pin_mask;	
	PIO->PULLUP_ENABLE_R = pin_mask;							
}
//-------------------------------------------------------------------------
inline unsigned int GPIO_ReadInput( int pin_mask ) 
{
	 return( PIO->PIN_DATA_STATUS_R & pin_mask );	
}
//-------------------------------------------------------------------------
inline unsigned int GPIO_ReadPins( void )
{
	 return( PIO->PIN_DATA_STATUS_R );						
}


//-------------------------------------------------------------------------
inline void GPIO_OpenOutput( unsigned int pin_mask )
{
	PIO->PIO_ENABLE_R = pin_mask; 								
	PIO->OUTPUT_ENABLE_R = pin_mask; 	
	PIO->PULLUP_DISABLE_R = pin_mask;						
}
//-------------------------------------------------------------------------
inline void GPIO_SetOutput( unsigned int pin_mask )
{
 	PIO->SET_OUTPUT_DATA_R = pin_mask;
}
//-------------------------------------------------------------------------
inline void GPIO_ClearOutput( unsigned int pin_mask )
{
 	PIO->CLEAR_OUTPUT_DATA_R = pin_mask;
}
//-------------------------------------------------------------------------
inline unsigned int GPIO_StatusOutput( unsigned int pin_mask )
{
 	return( PIO->OUTPUT_DATA_STATUS_R & pin_mask);
}
//-------------------------------------------------------------------------
inline void GPIO_ToggleOneOutput( unsigned int onepin_mask )
{
 	if( PIO->OUTPUT_DATA_STATUS_R & onepin_mask ){	
		PIO->CLEAR_OUTPUT_DATA_R = onepin_mask;				
	}else{
		PIO->SET_OUTPUT_DATA_R = onepin_mask;					
	}
}
//-------------------------------------------------------------------------
inline void GPIO_Configure_DirectWrite( unsigned int pin_mask)
{
	PIO->OUTPUT_WRITE_MASK_ENABLE_R = pin_mask; 
}
//-------------------------------------------------------------------------
inline void GPIO_DirectWriteOutput( unsigned int pins )
{
	PIO->OUTPUT_DATA_STATUS_R = pins; 
}


//-------------------------------------------------------------------------
inline void PIO_OpenPeriph_A( unsigned int PeriphA_mask )
{
	PIO->PERIPH_A_SELECT_R = PeriphA_mask;	
	PIO->PIO_DISABLE_R = PeriphA_mask; 	
	PIO->PULLUP_DISABLE_R = PeriphA_mask;		
}
//----------------------------------------------------------------------
inline void PIO_OpenPeriph_B( unsigned int PeriphB_mask )
{
	PIO->PERIPH_B_SELECT_R = PeriphB_mask; 	
	PIO->PIO_DISABLE_R = PeriphB_mask; 			
	PIO->PULLUP_DISABLE_R = PeriphB_mask;		
}
//-------------------------------------------------------------------------
inline void PIO_Configure_AsPeriph( unsigned int PeriphA, unsigned int PeriphB )
{
	PIO->PERIPH_A_SELECT_R		 = PeriphA;
	PIO->PERIPH_B_SELECT_R 		 = PeriphB;
	PIO->PIO_DISABLE_R = (PeriphA | PeriphB ); 
}
//----------------------------------------------------------------------
inline int PIO_IsGPIO( unsigned int one_pin )
{	
	return( PIO->PIO_STATUS_R & one_pin );	
}
//----------------------------------------------------------------------
inline int PIO_IsPeriph( unsigned int one_pin )
{	
	return( (~(PIO->PIO_STATUS_R) & one_pin) );	
}



//-------------------------------------------------------------------------
inline void PIO_InterruptEnable( unsigned int pins_mask )
{
		PIO->INT_ENABLE_R = pins_mask;
}
//-------------------------------------------------------------------------
inline void PIO_InterruptDisable( unsigned int pins_mask )
{
		PIO->INT_DISABLE_R = pins_mask;
}
//-------------------------------------------------------------------------
inline unsigned int PIO_ReadInterruptRequests( void )
{
		return( PIO->INT_STATUS_R );
}



