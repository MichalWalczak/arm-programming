/********************************************************************
  File: us0_dma_int.c
  Desc: Sterownik (driver) U0DMA portu USART
        Uzywa DMA oraz przerwan

				Przedstawiony kod opisany jest w rozdziale 16.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
	 			0.1G - GNU / 2007.08.31
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*********************************************************************/
#include "proj_config.h"
#include "../periph_hw_h/bit_names.h"
#include "../periph_hw_h/reg_typedef.h"
#include "../periph_hw_h/pio.h"
#include "../periph_hw_h/board_pins.h"
#include "../periph_hw_h/pmc.h"
#include "../periph_hw_h/aic.h"
#include "../periph_hw_h/led_sw.h"
#include "../periph_hw_h/dbgu_debug.h"
#include "../periph_hw_h/pdc.h"

#include "../periph_h/us_def.h"
#include "../periph_h/us0_dma_int.h"
#include <stdio.h>
#include <string.h>

void U0DMA_irq_handler( void ) DECLARE_AS_IRQ_HANDLER;
struct {
	volatile char RxErrFlag;
	volatile char RxTimeout;
} U0DMA_state;

//--------------------------------------------------------------------------
void U0DMA_Open(unsigned long baudrate, unsigned short timeout_in_bits )
{
	PMC_OpenPeriphClock( US0_PERIPH_ID_MASK );
	US0->INT_DISABLE_R = 0xFFFFFFFF;
  US0->CONTROL_R = US_RESET_RECEIVER | US_RESET_TRANSMITER
								 | US_RX_DISABLE | US_TX_DISABLE;
	PDC_DisableAndResetWrite( PDC_US0 );
	PDC_DisableAndResetRead( PDC_US0 );
	
	US0->MODE_R = US_CLOCK_MCK 
							| US_MODE_NORMAL | US_ASYNC_MODE | US_OVERSAMPLING16
							| US_8_BITS | US_PARITY_NONE | US_STOP_1_BIT | US_LSB_FIRST; 
  US0->BAUD_RATE_R = US_CalcPrescaler( baudrate );
  US0->TRANSMITER_TIMEGUARD_R = 0;
	US0->RECEIVER_TIMEOUT_R = timeout_in_bits;
	
	U0DMA_state.RxErrFlag=0;				
	U0DMA_state.RxTimeout=0;				

	AIC_Configure_IRQ( US0_PERIPH_ID, (AIC_EDGE_TRIGGERED | AIC_PRIORITY_3),
										 U0DMA_irq_handler );

  US0->CONTROL_R = US_TX_ENABLE | US_RX_ENABLE;
	PDC_EnableWrite( PDC_US0 );
	PDC_EnableRead( PDC_US0 );	

	#if (PROCESSOR_TYPE == PROCESSOR_SAM7S)
		PIO_OpenPeriph_A( RXD0_A5|TXD0_A6 );  
	#elif (PROCESSOR_TYPE == PROCESSOR_SAM7X)	
		PIO_OpenPeriph_A( RXD0_AA0|TXD0_AA0 );  
	#else
		#error "Nieznany typ procesora"
	#endif

	DBGU_Trace("Port USART0 (U0DMA) Opened, %i[bd].\n\r", (int)baudrate );
}

//--------------------------------------------------------------------------
void U0DMA_irq_handler( void ) DEFINE_AS_IRQ_HANDLER
{
 int status_r, status_and_mask;
 int tmp;
	status_r = US0->INT_STATUS_R;
	status_and_mask = status_r & US0->INT_MASK_R;
	DBGU_Trace(" \n\rU0INT ISR=0x%X ", status_r );

	if( status_and_mask & US_ENDRX ){
		DBGU_Trace("RX: ", NULL );
		US0->INT_DISABLE_R = US_ENDRX;  		
		tmp = status_r & (US_PARITY_ERR | US_OVERRUN_ERR | US_FRAME_ERR | US_RX_BREAK);
		if( tmp ){  										
			DBGU_Trace("RXERR: ", NULL );			
			U0DMA_state.RxErrFlag = tmp;  	
			US0->CONTROL_R = US_RESET_STATUS_ERR;	
		}
		//...														// kod uzytkowy stosownie do potrzeb aplikacji
	}
	if( status_and_mask & US_RX_TIMEOUT ){
	 	DBGU_Trace("RXTIMEOUT: RX_CNT_R=%i ",PDC_US0->RX_CNT_R);
		U0DMA_state.RxTimeout=1;					
		US0->INT_DISABLE_R = US_RX_TIMEOUT;
		//...														// kod uzytkowy stosownie do potrzeb aplikacji
	}
	if( status_and_mask & US_ENDTX ){
		DBGU_Trace("TX: ", NULL );
		US0->INT_DISABLE_R = US_ENDTX;  
		//...														// kod uzytkowy stosownie do potrzeb aplikacji
	}
	AIC->EOI_R = 0;										
}

//--------------------------------------------------------------------------
void U0DMA_AsyncRead( char *p, int ile )
{
	U0DMA_FlushRead();	
	U0DMA_state.RxErrFlag=0;					
	U0DMA_state.RxTimeout=0;					
	PDC_SetRx( p, ile, PDC_US0 );
	US0->CONTROL_R = US_START_TIMEOUT;		
	//US0->CONTROL_R = US_START_TIMEOUT | US_REARM_TIMEOUT;		
	US0->INT_ENABLE_R = US_ENDRX | US_RX_TIMEOUT;
}
//--------------------------------------------------------------------------
void U0DMA_AsyncWrite( char *p, int ile )
{
	U0DMA_FlushWrite();
	PDC_SetTx( p, ile, PDC_US0 );
	US0->INT_ENABLE_R = US_ENDTX;
}
//--------------------------------------------------------------------------
int U0DMA_IsReadCompleted( void )
{
	if( U0DMA_state.RxTimeout ) return(-1);	
	else if( PDC_US0->RX_CNT_R ) return(0); 
	else return(1);													
}
//--------------------------------------------------------------------------
int U0DMA_IsWriteCompleted( void )
{
	return( !PDC_US0->TX_CNT_R );	
}


//--------------------------------------------------------------------------
int U0DMA_FlushRead( void )
{
 int ret;
 	do{
		ret=U0DMA_IsReadCompleted();
		if(ret!=0) return(ret);
	}while(1);
}
//--------------------------------------------------------------------------
void U0DMA_FlushWrite( void )
{
	while( !U0DMA_IsWriteCompleted() ){};
}

//----------------------------------------------------------------------------------
int US_CalcPrescaler( unsigned int baud_rate )	
{
	unsigned int presc = ((fMCK_Hz*10)/(baud_rate * 16));	
	if ((presc % 10) >= 5)	presc = (presc / 10) + 1;
	else									presc /= 10;
	return( presc );			
}


