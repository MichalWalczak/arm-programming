/********************************************************************
  File: rtt_def.h   (notacja JagNOT)
  Desc: timer RTT

				Przedstawiony kod opisany jest w rozdziale 13.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*********************************************************************/
#ifndef _RTT_DEF_H_INCLUDED_
#define _RTT_DEF_H_INCLUDED_
#include "../periph_hw_h/reg_typedef.h"

//-----------------------------------------------------------------------------
typedef struct _RTT {	// dostep, wartosc po resecie, adres wzgledny, nazwa Atmel
		REG	 MODE_R; 			// R/W,			0x00008000,					+0x0,					RTMR
		REG	 ALARM_R; 		// R/W,			0xFFFFFFFF,					+0x4,					RTAR 
		REG	 VALUE_R; 		// RO,			0x0,								+0x8,					RTVR 
		REG	 STATUS_R; 		// RO,			0x0,								+0xC,					RTSR
} _RTT, *PS_RTT;

#define RTT   ((PS_RTT) 0xFFFFFD20) 

// -------- MODE_R ---------- 
#define RTT_PRESCALER  			 			((unsigned int) 0xFFFF << 0) 
#define RTT_ALARM_INT_ENABLE      ((unsigned int) 0x1 << 16) 
#define RTT_ALARM_INT_DISABLE 		   0x0 <<16
#define RTT_INCREMENT_INT_ENABLE  ((unsigned int) 0x1 << 17) 
#define RTT_INCREMENT_INT_DISABLE	   0x0 <<17
#define RTT_RESTART   				 		((unsigned int) 0x1 << 18) 
// -------- STATUS_R -------- 
#define RTT_ALARM_STATUS      		((unsigned int) 0x1 <<  0) 
#define RTT_INCREMENT_STATUS  		((unsigned int) 0x1 <<  1) 




#endif // of _RTT_DEF_H_INCLUDED_
