/********************************************************************
  File: aic.c
  Desc: Sterownik (driver) kontrolera przerwan AIC

				Przedstawiony kod opisany jest w rozdziale 9.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************************************************/
#include "../periph_hw_h/reg_typedef.h"
#include "../periph_hw_h/aic_def.h"
#include "../periph_hw_h/aic.h"
#include "../periph_hw_h/dbgu_debug.h"

//--------------------------------------------------------------------
inline void AIC_Enable( unsigned int irq_mask )
{
   AIC->INT_ENABLE_R = irq_mask;
}
//--------------------------------------------------------------------
inline void AIC_Disable( unsigned int irq_mask )
{
	AIC->INT_DISABLE_R = irq_mask;
}

//--------------------------------------------------------------------
inline void AIC_Enable2( unsigned int irq_mask )
{
    AIC->INT_ENABLE_R = irq_mask;
}
//--------------------------------------------------------------------
inline void AIC_Disable2( unsigned int irq_mask )
{
    AIC->INT_DISABLE_R = irq_mask;
}
//--------------------------------------------------------------------
inline void AIC_DisableAll( void )
{
    AIC->INT_DISABLE_R = (0xFFFFFFFF);
}



//--------------------------------------------------------------------
inline unsigned int AIC_Configure_IRQ (
					unsigned int periph_id,     					
					unsigned int priorytet_i_wyzwalanie,
					void (*adres_proc_obslugi) (void) ) 			
{
	unsigned int adres_poprzedniej_proc, mask;
  	mask = (0x1 << periph_id);

    adres_poprzedniej_proc = AIC->SOURCE_VECTOR_R[periph_id];     	
    AIC->INT_DISABLE_R = mask ;											
    AIC->SOURCE_VECTOR_R[periph_id] = (unsigned int) adres_proc_obslugi ; 	
    AIC->SOURCE_MODE_R[periph_id] = priorytet_i_wyzwalanie;				
    AIC->INT_CLEAR_CMD_R = mask;										
		AIC->INT_ENABLE_R = mask;  
	return adres_poprzedniej_proc;
}

//--------------------------------------------------------------------
inline unsigned int AIC_Configure_FIQ (
						unsigned int periph_id,     				
						unsigned int typ_wyzwalania,				
						void (*adres_proc_obslugi)(void) ) 	
{
	unsigned int adres_poprzedniej_proc, mask;
  	mask = (0x1 << periph_id) ;

    adres_poprzedniej_proc = AIC->SOURCE_VECTOR_R[ 0 ]; 	
    AIC->INT_DISABLE_R 	= mask ;							
		AIC->SOURCE_VECTOR_R[ 0 ] = (unsigned int)adres_proc_obslugi; 
		AIC->SOURCE_MODE_R[ 0 ] = typ_wyzwalania;	
    AIC->INT_CLEAR_CMD_R 		= mask;								
		AIC->FAST_ENABLE_R 	= mask;								
		AIC->INT_ENABLE_R 	= mask;  							
	return adres_poprzedniej_proc;
}

//----------------------------------------------------------------------------
int AIC_spurious_cnt;
void default_spurious_handler(void) //__irq
{ 	
	AIC_spurious_cnt++;			
	AIC->EOI_R = 0;		
}
//----------------------------------------------------------------------------
void AIC_ConfigureDefaultSpuriousHandler( void )
{
	AIC->SPURIOUS_VECTOR_R = (unsigned int)default_spurious_handler;
}
//----------------------------------------------------------------------------
void AIC_ConfigureSpurious( void (*adres_proc_obslugi)(void) )
{
	AIC->SPURIOUS_VECTOR_R = (unsigned int)adres_proc_obslugi;
}


