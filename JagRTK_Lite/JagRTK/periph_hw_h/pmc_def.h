/********************************************************************
  File: pmc_def.h   (notacja JagNOT)
  Desc: Definicja struktury rejestrow i stalych symbolicznych urz. PMC 

				Przedstawiony kod opisany jest w rozdziale 4 i 5.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*********************************************************************/
#ifndef _PMC_DEF_H_INCLUDED_
#define _PMC_DEF_H_INCLUDED_

#include "../periph_hw_h/reg_typedef.h"
				  
//-----------------------------------------------------------------------------------------
typedef struct S_PMC {				
	REG	 SYS_CLOCK_ENABLE_R; 		// WO,		-,										+0x00,	SCER
	REG	 SYS_CLOCK_DISABLE_R; 	// WO,		-,										+0x04,	SCDR
	REG	 SYS_CLOCK_STATUS_R; 		// RO, 		0x01,									+0x08,	SCSR
	REG	 rezerw0[1]; 					
	REG	 PERIPH_CLOCK_ENABLE_R; // WO,		-,										+0x10,	PCER
	REG	 PERIPH_CLOCK_DISABLE_R;// WO,		-,										+0x14,	PCDR
	REG	 PERIPH_CLOCK_STATUS_R; // RO,		0x0,									+0x18,	PCSR
	REG	 Reserved1[1]; 				 
	REG	 MAIN_OSC_R; 						// R/W,		0x0,									+0x20,	MOR
	REG	 MAIN_CLK_FREQUENCY_R; 	// RO,		0x0,									+0x24,	MCFR
	REG	 rezerw2[1]; 					 
	REG	 PLL_R; 								// R/W,		0x3F00,								+0x2C,	PLLR
	REG	 MCK_R; 								// R/W,		0x0,									+0x30,	MCKR
	REG	 rezerw3[3]; 					
	REG	 PCK_R[3]; 							// R/W,		0x0,									+0x40,	PCK0..3
	REG  rezerw4[5];							
	REG	 INT_ENABLE_R; 					// WO,		-,										+0x60,	IER
	REG	 INT_DISABLE_R; 				// WO,		-,										+0x64, 	IDR
	REG	 INT_STATUS_R; 					// RO,		0x08,									+0x68,	ISR
	REG	 INT_MASK_R; 						// RO,		0x0,									+0x6C,	IMR
} S_PMC, *PS_PMC;

#define PMC  ((PS_PMC)    0xFFFFFC00)


// -------- SYS_CLOCK_ENABLE_R / _DISABLE_R / _STATUS_R -----------------------
#define bPMC_PCK         ((unsigned int) 0x1 <<  0) 
#define bPMC_UDP         ((unsigned int) 0x1 <<  7) 
#define bPMC_PCK0        ((unsigned int) 0x1 <<  8) 
#define bPMC_PCK1        ((unsigned int) 0x1 <<  9) 
#define bPMC_PCK2        ((unsigned int) 0x1 << 10) 
//#define bPMC_PCK3        ((unsigned int) 0x1 << 11) 

// -------- MCK_R ------------------------------------------------------------- 
#define PMC_SELCLK_field    ((unsigned int) 0x3 <<  0) 
#define 	PMC_SEL_SLOWCLK   ((unsigned int) 0x0) 
#define 	PMC_SEL_MAINCLK   ((unsigned int) 0x1) 
#define 	PMC_SEL_PLLCLK    ((unsigned int) 0x3) 
#define PMC_PRESC_field     ((unsigned int) 0x7 <<  2)
#define 	PMC_PRESC_1       ((unsigned int) 0x0 <<  2) 
#define 	PMC_PRESC_2     	((unsigned int) 0x1 <<  2) 
#define 	PMC_PRESC_4     	((unsigned int) 0x2 <<  2) 
#define 	PMC_PRESC_8     	((unsigned int) 0x3 <<  2) 
#define 	PMC_PRESC_16    	((unsigned int) 0x4 <<  2) 
#define 	PMC_PRESC_32    	((unsigned int) 0x5 <<  2) 
#define 	PMC_PRESC_64    	((unsigned int) 0x6 <<  2) 
// -------- PCK_R, identyczne bity z MCK_R

// -------- INT_ENABLE_R / INT_DISABLE_R / INT_STATUS_R / INT_MASK_R --------------------------
#define PMC_MAIN_OSC_STATUS ((unsigned int) 0x1 <<  0)
#define PMC_LOCK        		((unsigned int) 0x1 <<  2)
#define PMC_MCK_READY      	((unsigned int) 0x1 <<  3)
#define PMC_PCK0_READY     	((unsigned int) 0x1 <<  8)
#define PMC_PCK1_READY     	((unsigned int) 0x1 <<  9)
#define PMC_PCK2_READY     	((unsigned int) 0x1 << 10)
#define PMC_PCK3_READY      ((unsigned int) 0x1 << 11)



//--------------------------------------------------------------------------------------------
typedef struct _S_CKGR {
	REG	 MOR; 	
	REG	 MCFR; 	
	REG	 rezerw0[1]; 	
	REG	 PLLR; 	
} S_CKGR, *PS_CKGR;
#define CKGR ((PS_CKGR) 0xFFFFFC20) 

// ------ MAIN_OSC_R -------- 
#define PMC_MAIN_OSC_ENABLE 	((unsigned int) 0x1 <<  0) 
#define PMC_OSC_BYPASS  			((unsigned int) 0x1 <<  1) 
#define PMC_OSC_COUNT_bsf   	8
#define PMC_OSC_COUNT_field 	((unsigned int) 0xFF <<  PMC_OSC_COUNT_bsf) 
// ------ MAIN_CLK_FREQUENCY_R  -------- 
#define PMC_MAIN_FREQUENCY_field 	((unsigned int) 0xFFFF <<  0) 
#define PMC_MAIN_OSC_READY    ((unsigned int) 0x1 << 16) 
// ------ PLL_R  -----------
#define PMC_DIV_bsf       		0
#define PMC_DIV_field       	((unsigned int) 0xFF <<  PLL_DIV_bsf) 
#define 	PMC_DIV_0           	((unsigned int) 0x0) 
#define 	PMC_DIV_BYPASS      	((unsigned int) 0x1) 
#define PMC_PLL_COUNT_bsf   	8
#define PMC_PLL_COUNT_field 	((unsigned int) 0x3F << PLL_PLL_COUNT_bsf) 
#define 	PMC_OUT_PLL_80_160    ((unsigned int) 0x0 << 14) 
#define 	PMC_OUT_PLL_150_180   ((unsigned int) 0x2 << 14) 
#define PMC_MUL_bsf        		16
#define PMC_MUL_field       	((unsigned int) 0x7FF << PLL_MUL_bsf) 
#define PMC_USBDIV_field    	((unsigned int) 0x3 << 28) 
#define 	PMC_USB_DIV_1       	((unsigned int) 0x0 << 28) 
#define 	PMC_USB_DIV_2        	((unsigned int) 0x1 << 28) 
#define 	PMC_USB_DIV_4        	((unsigned int) 0x2 << 28) 


#endif // _PMC_DEF_H_INCLUDED_
