/***************************************************************************
  File: jtag_disableaic.s
  Desc: wstawka	z poziomu jezyka Asembler

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow dla procesorow z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Metody projektowania systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"
				Przedstawiony kod opisany jest w rozdziale 3 i 5.

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.05.01
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

********************************************************************************/
JTAG_DisableAIC:		
		//-----------------------------------------
		_AIC_INT_DISABLE_R 	EQU 	0xFFFFF124

		ldr r0,=_AIC_INT_DISABLE_R
		ldr r1,=0xFFFFFFFF
		str r1,[r0]					

		//-----------------------------------------
		_RSTC_CONTROL_R 		EQU	 0xFFFFFD00
		_RSTC_PERIPH_RESET  EQU  0x4
		   
		ldr r0,=_RSTC_CONTROL_R
		ldr r1,=(0xA5000000 | _RSTC_PERIPH_RESET)
		str r1,[r0]					
		nop
		nop	
		nop	
		_w0_wait_execute:
				nop
		 b _w0_wait_execute	
//-------------------------------------------------------------------------------------
