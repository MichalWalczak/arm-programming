/********************************************************************
  File: pit_def.h   (notacja JagNOT)
  Desc: PIT 

				Przedstawiony kod opisany jest w rozdziale 12.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*********************************************************************/
#ifndef _TIMER_PIT_DEF_H_INCLUDED_
#define _TIMER_PIT_DEF_H_INCLUDED_
#include "../periph_hw_h/reg_typedef.h"
#include "../periph_hw_h/bit_names.h"


//-------------------------------------------------------------------------
typedef struct _PITC { 		
	REG	 MODE_R; 			// R/W,			0x000FFFFF,					+0x00,						PIMR
	REG	 STATUS_R; 		// RO,			0x0,								+0x04,						PISR
	REG	 VALUE_R; 		// RO,			0x0,								+0x08,						PIVR
	REG	 IMAGE_R; 		// R0,			0x0,								+0x0C,						PIIR
} _PIT, *PS_PIT;

#define  PIT   ((PS_PIT) 0xFFFFFD30) 	


//--- rejestr MODE_R -----------------------------------------------------
#define PIT_VALUE_field		0x000FFFFF	
#define PIT_ENABLE				BIT_24
#define PIT_INT_ENABLE		BIT_25
//--- rejestr STATUS_R ----------------------------------------------------
#define PIT_STATUS				BIT_0

//--- rejestr VALUE_R -----------------------------------------------------
//--- rejestr IMAGE_R -----------------
#define PIT_CURRENT_VALUE_field 0x000FFFFF
#define PIT_COUNTER_field				0xFFF00000	
#define PIT_COUNTER_bsf					20					


#endif // _TIMER_PIT_DEF_H_INCLUDED_
