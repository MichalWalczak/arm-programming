/*****************************************************************************
  File: pwm.c
  Desc: Sterownik (driver) timera PWM

				Przedstawiony kod opisany jest w rozdziale 14.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*******************************************************************************/
#include "proj_config.h"
#include "../periph_hw_h/pmc.h"
#include "../periph_hw_h/pio.h"
#include "../periph_hw_h/board_pins.h"
#include "../periph_hw_h/aic.h"
#include "../periph_hw_h/led_sw.h"
#include "../periph_hw_h/delay.h"
#include "../periph_hw_h/dbgu_debug.h"
#include "../periph_h/pwm.h"


//----------------------------------------------------------------------
typedef struct {
 	unsigned int pin_mask;
	unsigned int periph_a_or_b;
} _cfg_wyprowadzen;
static _cfg_wyprowadzen  PWM_state[4];

//----------------------------------------------------------------------
void PWM_Open( int channel, int prescaler, int resolution, int polarity,
			   unsigned int pin_mask, char periph_a_or_b )
{
 int chan_mask = (0x1 << channel);
	PMC_OpenPeriphClock( PWM_PERIPH_ID_MASK );
	PWM->INT_DISABLE_R = chan_mask;
	PWM->DISABLE_R = chan_mask;				
	while( PWM->STATUS_R & chan_mask ){};		
	PWM->CHANNEL[channel].MODE_R =  prescaler
									| polarity
									| PWM_LEFT_ALINGMENT
									| PWM_UPDATE_DUTY;
	PWM->CHANNEL[channel].PERIOD_R = resolution;
	PWM->CHANNEL[channel].DUTY_R = 2;  
	//PWM->CHANNEL[channel].UPDATE_R = 2;
	if( polarity == PWM_POLARITY_HIGH )	
				GPIO_ClearOutput( pin_mask ); 
	else	GPIO_SetOutput( pin_mask );		
	GPIO_OpenOutput( pin_mask );				

	PWM_state[channel].pin_mask = pin_mask;									
	PWM_state[channel].periph_a_or_b = periph_a_or_b;				

	PWM->ENABLE_R = chan_mask;				
	DBGU_Trace("Starting PWM channel=%i.\n\r", channel );
}


//----------------------------------------------------------------------
void PWM_Set( int channel, int duty )
{ 
	
	
	if( duty > 1 ){	
	 	if( PIO_IsGPIO( PWM_state[channel].pin_mask ) ){ 
			if( PWM_state[channel].periph_a_or_b == PIN_ASSIGNED_TO_PERIPH_A )				
						PIO_OpenPeriph_A( PWM_state[channel].pin_mask );
			else	PIO_OpenPeriph_B( PWM_state[channel].pin_mask );	
		}
		if( duty > PWM->CHANNEL[channel].PERIOD_R ){ 
			duty = PWM->CHANNEL[channel].PERIOD_R;
		}
		
		while((PWM->CHANNEL[channel].COUNTER_R == 0) || (PWM->CHANNEL[channel].COUNTER_R == 1));
		PWM->CHANNEL[channel].UPDATE_R = duty;	
	
	}else{ 
		if( !PIO_IsGPIO( PWM_state[channel].pin_mask ) ){  	
			GPIO_OpenOutput( PWM_state[channel].pin_mask );		
		}
		
		while((PWM->CHANNEL[channel].COUNTER_R == 0) || (PWM->CHANNEL[channel].COUNTER_R == 1));
		PWM->CHANNEL[channel].UPDATE_R = 2;		
	}	
}
//------------------------------------------------------------------------------
void PWM_Close( int channel )
{
	if( !PIO_IsGPIO( PWM_state[channel].pin_mask ) ){  
		GPIO_OpenOutput( PWM_state[channel].pin_mask );	
	}
 	PWM->DISABLE_R = 0x1 <<channel;				
	while( PWM->STATUS_R & (0x1<<channel) ){};		
}
