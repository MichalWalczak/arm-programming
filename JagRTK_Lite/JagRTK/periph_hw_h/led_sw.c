/****************************************************************
  File: led_sw.c
  Desc: Sterownik (driver) LED oraz sterownik (driver) SW (mikrowylacznikow)
				dla typowego podlaczenia: aktywny niskim stanem

				Przedstawiony kod opisany jest w rozdziale 6 i 7.

				Szczegolowy opis programu, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

******************************************************************/
#include "pio.h"
#include "led_sw.h"

//-------------------------------------------------------------
//-------------------------------------------------------------
int SW_IsPressed( unsigned int keyb_mask )
{
		return( ~(GPIO_ReadInput(keyb_mask)) & keyb_mask );	
}
//-------------------------------------------------------------
void SW_Open( unsigned int keyb_mask )
{
		GPIO_OpenInputPullUp( keyb_mask ); 	
}


//-------------------------------------------------------------
void LED_Set( unsigned int mask )
{
 	GPIO_ClearOutput( mask );		
}
//-------------------------------------------------------------
void LED_Clear( unsigned int mask )
{
 	GPIO_SetOutput( mask );  		
}
//-------------------------------------------------------------
void LED_ToggleOne( unsigned int one_led )
{
 	GPIO_ToggleOneOutput( one_led );
}
//-------------------------------------------------------------
void LED_SetState( unsigned int mask, int stan )
{
 	if(stan==0){
		LED_Clear( mask );
	}else{
		LED_Set( mask );
	}
}
//-------------------------------------------------------------
unsigned int LED_GetState( unsigned int mask )
{
	return( ~(PIO->OUTPUT_DATA_STATUS_R & mask) );	
}

//-------------------------------------------------------------
void LED_Open( unsigned int mask )
{
	GPIO_OpenOutput( mask );
}

//-------------------------------------------------------------
void LEDWIZ_Set( unsigned int mask, int WIZ )
{
	if(WIZ){
 		GPIO_ClearOutput( mask );	
	}
}
//-------------------------------------------------------------
void LEDWIZ_Clear( unsigned int mask, int WIZ )
{
	if(WIZ){
 		GPIO_SetOutput( mask );  
	}
}
//-------------------------------------------------------------
void LEDWIZ_ToggleOne( unsigned int onepin, int WIZ )
{
	if(WIZ){
 		GPIO_ToggleOneOutput( onepin );
	}
}


