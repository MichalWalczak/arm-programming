/********************************************************************
	File: proj_config.h
	Desc: Najbardziej istotne konfiguracje projektu

				Przedstawiony kod opisany jest w rozdziale 7.
        
				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)
   Ver: 0.1 / 2007.07.25
	 			0.1G - GNU / 2007.08.31
	 Web: www.sparrow-rt.com

   * Copyright (C) 2006-2007 by JAG Jacek Augustyn. All rights reserved.
   *
   * Redistribution and use in source and binary forms, with or without
   * modification, are permitted provided that the following conditions
   * are met:
   *
   * 1. Redistributions of source code must retain the above copyright
   *    notice, this list of conditions and the following disclaimer.
   * 2. Redistributions in binary form must reproduce the above copyright
   *    notice, this list of conditions and the following disclaimer in the
   *    documentation and/or other materials provided with the distribution.
   * 3. Neither the name of the copyright holders nor the names of
   *    contributors may be used to endorse or promote products derived
   *    from this software without specific prior written permission.
   *
   * THIS SOFTWARE IS PROVIDED BY JAG AND CONTRIBUTORS
   * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
   * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL EGNITE
   * SOFTWARE GMBH OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
   * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
   * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
   * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
   * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
   * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
   * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
   * SUCH DAMAGE.

*********************************************************************/

#ifndef _PROJ_CONFIG_H_INCLUDED_
#define _PROJ_CONFIG_H_INCLUDED_
#include "JagRTK/periph_hw_h/processor_kit_def.h"

//-------------------------------------------------------------------
//   glowne stale symboliczne
//-------------------------------------------------------------------
#define MCK    	47923200   
#define EXT_OSC 18432000   
#define MCKKHz  (MCK/1000) 

#define KIT_TYPE	SAM7S_KIT_SPARROW_1			



extern int fMCK_Hz;
extern int RunTimeMemoryType;
extern int MainIterations;
extern int MainDelay;		


//-------------------------------------------------------------------------
// dodatkowe definicje i sprawdzanie warunkow
//-------------------------------------------------------------------------
#if (KIT_TYPE == SAM7S_KIT_SPARROW_1)
	#define PROCESSOR_TYPE  	PROCESSOR_SAM7S
#elif (KIT_TYPE == SAM7S_KIT_PROPOX)
	#define PROCESSOR_TYPE  	PROCESSOR_SAM7S
#elif  (KIT_TYPE == SAM7X_KIT_ATMEL)
	#define PROCESSOR_TYPE  	PROCESSOR_SAM7X
#elif	(KIT_TYPE == SAM7X_KIT_OLIMEX)
	#define PROCESSOR_TYPE  	PROCESSOR_SAM7X
#elif
  #error "Nieznany typ kita"
#endif

#if KIT_TYPE != SAM7S_KIT_SPARROW_1
	#error "ten projekt jest dla kita SAM7S_KIT_SPARROW_1"
#endif
	

#endif // _PROJ_CONFIG_H_INCLUDED_
