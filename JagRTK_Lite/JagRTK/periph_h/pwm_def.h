/********************************************************************
  File: pwm_def.h   (notacja JagNOT)
  Desc: Sterownik (driver) timera PWM

				Przedstawiony kod opisany jest w rozdziale 14.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*********************************************************************/
#ifndef _PWM_DEF_H_INCLUDED_
#define _PWM_DEF_H_INCLUDED_
#include "../periph_hw_h/reg_typedef.h"

//------------------------------------------------------------------
typedef struct S_PWM_CHANNEL {	// dostep, wart. po resecie, offset, nazwa[AT]
	REG	 MODE_R; 								// R/W,			0x0,							+0x00	,CMR
	REG	 DUTY_R; 								// R/W,			0x0,							+0x04	,CDTYR
	REG	 PERIOD_R; 							// R/W,			0x0,							+0x08	,CPRDR
	REG	 COUNTER_R; 						// RO,			0x0,							+0x0C	,CCNTR
	REG	 UPDATE_R; 							// WO,			-,								+0x10	,CUPDR
	REG	 rezerw[3]; 						//
} S_PWMC_CH, *PS_PWMC_CH;

// -------- CHANNEL[n].MODE_R 
#define PWM_CHAN_PRESC_field      ((unsigned int) 0xF <<  0) 
#define 	PWM_CHAN_PRESC_MCK_1		0x0	
#define 	PWM_CHAN_PRESC_MCK_2		0x1	
#define 	PWM_CHAN_PRESC_MCK_4		0x2	
#define 	PWM_CHAN_PRESC_MCK_8		0x3	
#define 	PWM_CHAN_PRESC_MCK_16		0x4	
#define 	PWM_CHAN_PRESC_MCK_32		0x5	
#define 	PWM_CHAN_PRESC_MCK_64		0x6	
#define 	PWM_CHAN_PRESC_MCK_128	0x7	
#define 	PWM_CHAN_PRESC_MCK_256	0x8	
#define 	PWM_CHAN_PRESC_MCK_512	0x9	
#define 	PWM_CHAN_PRESC_MCK_1024	0xA	
#define 	PWM_CHAN_PRESC_CLKA			0xB	
#define 	PWM_CHAN_PRESC_CLKB			0xC	
#define PWM_CENTER_ALINGMENT	0x1 << 8
#define PWM_LEFT_ALINGMENT		0x0 << 8
#define PWM_POLARITY_HIGH 		0x1 << 9
#define PWM_POLARITY_LOW  		0x0 << 9
#define PWM_UPDATE_PERIOD			0x1 << 10
#define PWM_UPDATE_DUTY				0x0 << 10



//----------------------------------------------------------------------
typedef struct  S_PWM {	// dostep, wart. po resecie, offset, nazwa[AT]
	REG	 MODE_R; 					// R/W,			0x0,						+0x00, 		MR
	REG	 ENABLE_R; 				// WO,			-,							+0x04,		ENA
	REG	 DISABLE_R; 			// WO,			-,							+0x08,		DIS
	REG	 STATUS_R; 				// RO,			0x0,						+0x0C,		SR
	REG	 INT_ENABLE_R; 		// WO,			-,							+0x10,		IER
	REG	 INT_DISABLE_R; 	// WO,			-,							+0x14,		IDR
	REG	 INT_MASK_R; 			// RO,			0x0,						+0x18,		IMR
	REG	 INT_STATUS_R; 		// RO,			0x0,						+0x1C,		ISR
	REG	 Reserved0[55]; 	// 
	REG	 VERSION_R; 			// 	,			,	VR
	REG	 Reserved1[64]; 	// 
	S_PWMC_CH	CHANNEL[4]; // struktury kanalow PWM,   +0x200
} S_PWM, *PS_PWM;

#define PWM   ((PS_PWM) 0xFFFCC000) 						 
#define PWM_PERIPH_ID   		((unsigned int) 10)  
#define PWM_PERIPH_ID_MASK  (0x1<<PWM_PERIPH_ID) 



// ----- rej. MODE_R
#define PWM_DIV_A_bsf     	0	 
#define PWM_DIV_A_field    ((unsigned int) 0xFF <<  0) 
#define PWM_PRESC_A_field  ((unsigned int) 0xF <<  8) 
#define 	PRESC_A_MCK_1			0x0	<< 8
#define 	PRESC_A_MCK_2			0x1	<< 8
#define 	PRESC_A_MCK_4			0x2	<< 8
#define 	PRESC_A_MCK_8			0x3	<< 8
#define 	PRESC_A_MCK_16		0x4	<< 8
#define 	PRESC_A_MCK_32		0x5	<< 8
#define 	PRESC_A_MCK_64		0x6	<< 8
#define 	PRESC_A_MCK_128		0x7	<< 8
#define 	PRESC_A_MCK_256		0x8	<< 8
#define 	PRESC_A_MCK_512		0x9	<< 8
#define 	PRESC_A_MCK_1024	0xA	<< 8
#define PWM_DIV_B_bsf     	16
#define PWM_DIV_B_field     ((unsigned int) 0xFF << 16) 
#define PWM_PRESC_B_field   ((unsigned int) 0xF << 24) 
#define 	PRESC_B_MCK_1			0x0	<< 24
#define 	PRESC_B_MCK_2			0x1	<< 24
#define 	PRESC_B_MCK_4			0x2	<< 24
#define 	PRESC_B_MCK_8			0x3	<< 24
#define 	PRESC_B_MCK_16		0x4 << 24
#define 	PRESC_B_MCK_32		0x5	<< 24
#define 	PRESC_B_MCK_64		0x6	<< 24
#define 	PRESC_B_MCK_128		0x7	<< 24
#define 	PRESC_B_MCK_256		0x8	<< 24
#define 	PRESC_B_MCK_512		0x9	<< 24
#define 	PRESC_B_MCK_1024	0xA	<< 24


// ---  rej. ENABLE_R, DISABLE_R, STATUS_R
// ---  rej. INT_ENABLE_R, INT_DISABLE_R, INT_MASK_R, INT_STATUS_R
#define PWM_CHAN_ID0      ((unsigned int) 0x1 <<  0) 
#define PWM_CHAN_ID1      ((unsigned int) 0x1 <<  1) 
#define PWM_CHAN_ID2      ((unsigned int) 0x1 <<  2) 
#define PWM_CHAN_ID3      ((unsigned int) 0x1 <<  3) 


#endif // of_PWM_DEF_H_INCLUDED_
