/*********************************************
  File:	hd44780.h
  Desc:	Sterownik (driver) wyswietlacza LCD typu hd44780

				Przedstawiony kod opisany jest w rozdziale 6 i 7.

				Szczegolowy opis programu, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)  
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**********************************************************************************/
#ifndef _HD44780_H_INCLUDED_
#define _HD44780_H_INCLUDED_
#include "../periph_ui_h/hd44780_def.h"
#include "proj_config.h"

//---------------------------------------------------------------------
#define LCD_DATA  0x01 		
#define LCD_CMD   0x00 		




//---------------------------------------------------------------------
void LCD_WriteCmd(unsigned char cmd_lcd );
void LCD_WriteByte(unsigned char data_lcd );
void LCD_WriteString(char *pt);
void LCD_WriteStringUpToN( int n, char *p );
void LCD_WriteStringRC(char row, char col, char *p);
void LCD_WriteDirectN(char *pt, unsigned char ile );
void LCD_GotoRC(char row, char col);
void LCD_ClrScr(void);
void LCD_Home(void);

void LCD_SendNibble(unsigned int data_lcd, unsigned int cmd, char ktory);
void LCD_Delay1us( void );
void LCD_Delay40us(void);
void LCD_Delay2ms (void);
void LCD_DelayN2ms( unsigned int ile );

void LCD_Open(void);


void LCD_Hex4(int hex);
void LCD_Hex8(int hex);
void LCD_Hex16(int hex);
void LCD_GotoXY(char x, char y);
void LCD_TextXY(char x, char y, char *pt);

void LCD_WriteInt(int value, int lm, int null_zero);


extern char LCD_TraceBuf[];
//------------------------------------------------------------------------
#define LCD_Trace(row,col,txt,zmn)  									  \
	if( 1 ){ 																\
		sprintf( LCD_TraceBuf, txt, zmn );  								\
		LCD_GotoRC( row, col );															\
		LCD_WriteString( LCD_TraceBuf ); 										\
	}	
//-----------------------------------------------------------------------
/*void LCD_WriteFloat(float l,int lm_i,int lm_f)
{

		int dzielnik = 1;	
	int i = (int) l;
	int f; 
	int lm_f1 = lm_f;
			
	while(lm_f1--)	
		dzielnik *= 10;		

	f = (int)((l - (float)i ) * dzielnik + 0.5 );

	if (f < 0)
		f = (int)-f + 1.5;
	
	LCD_WriteInt(i,lm_i, 1) ;	
	LCD_WriteByte('.');		
	LCD_WriteInt((int)f,lm_f, 0) ;	
}*/
//--------------------------------------------------------------------------------------


#endif // _HD44780_H_INCLUDED_
