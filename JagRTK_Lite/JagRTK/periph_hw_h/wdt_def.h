/********************************************************************
  File: wdt_def.h   (notacja JagNOT)
  Desc: defincja struktury rejestrow i stalych symbolicznych watchdoga WDT

				Przedstawiony kod opisany jest w rozdziale 5.

				Szczegolowy opis dzialania, komentarze, objasnienia, 
				omowienie dzialania rdzenia ARM7TDMI, jego trybow pracy, 
				urzadzen peryferyjnych procesorow rodziny SAM7S, 
				metod projektowania systemow wbudowanych dla procesorow 
				z rdzeniem ARM7TDMI, 
				driverow urzadzen peryferyjnych, etc.
				znajduja sie w ksiazce:
				 "Projektowanie systemow wbudowanych 
				  na przykladzie rodziny SAM7S z rdzeniem ARM7TDMI"

    By: JAG (Jacek Augustyn)
   Ver: 0.1 / 2007.07.25
   Web: www.sparrow-rt.com

   * Copyright (c) 2006-2007, JagRT Jacek Augustyn
   * 
   * All rights reserved.
   * 
   * Redistribution and use in source and binary forms, with or without modification, 
   * are permitted provided that the following conditions are met:
   * 
   * 1. Redistributions of source code must retain the above copyright notice, 
   *    this list of conditions and the following disclaimer. 
   * 
   * 2. Redistributions in binary form must reproduce the above copyright notice, 
   *    this list of conditions and the following disclaimer in the documentation 
   *    and/or other materials provided with the distribution. 
   * 
   * 3. Neither the name of the JagRT Jacek Augustyn nor the names of its contributors 
   *    may be used to endorse or promote products derived from this software 
   *    without specific prior written permission. 
   * 
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*********************************************************************/
#ifndef _WDT_DEF_H_INCLUDED_
#define _WDT_DEF_H_INCLUDED_
#include "../periph_hw_h/reg_typedef.h"


//-----------------------------------------------------------------------------
//   WATCHDOG blok
//-----------------------------------------------------------------------------
typedef struct _WDTC {	
	REG	 CONTROL_R; 			// WO,					-, 				+0x0,		WDCR 
	REG	 MODE_R; 					// WOnce/RO, 0x3FFF2FFF,	+0x4,		WDMR 
	REG	 STATUS_R; 				// RO, 				0x0,				+0x8,		WDSR
} _WDTC, *PS_WDTC;

#define WDT  ((PS_WDTC) 0xFFFFFD40) 	

// --- CONTROL_R -----------------
#define WDT_RESTART   					((unsigned int) 0x1 <<  0) 
// -------- MODE_R  -------------
#define WDT_WD_VAL_bsf   				0
#define WDT_WD_VAL_field   			((unsigned int) 0xFFF <<WDT_WD_VAL_bsf) 
#define WDT_FAULT_INT_EN 				((unsigned int) 0x1 << 12) 
#define WDT_RESET_EN 						((unsigned int) 0x1 << 13) 
#define WDT_ON_FAULT_RESET_PROC ((unsigned int) 0x1 << 14) 
#define WDT_DISABLE   					((unsigned int) 0x1 << 15) 
#define WDT_WD_DELTA_bsf        16
#define WDT_WD_DELTA_field      ((unsigned int) 0xFFF << WDT_WD_DELTA_bsf)
#define WDT_DEBUG_HALT 					((unsigned int) 0x1 << 28) 
#define WDT_IDLE_HALT 					((unsigned int) 0x1 << 29) 
// -------- STATUS_R -------------
#define WDT_UNDERFLOW  ((unsigned int) 0x1 <<  0) 
#define WDT_ERROR      ((unsigned int) 0x1 <<  1) 



#endif // of _WDG_DEF_H_INCLUDED_
